﻿using LearningBase.Dto.Common;

namespace LearningBase.ServiceContract
{
    public interface ISecurityProvider
    {
        #region Properties

        bool IsAuthenticated { get; }

        int CurrentUserId { get; }

        string CurrentUserName { get; }

        #endregion

        #region (public) Methods

        bool Login(string userName, string password);

        void Logout();

        bool IsUserExist(string userName);

        bool IsUserInRole(string userName, string roleName);

        string[] GetRoles(string userName);

        void AddUserToRole(string userName, string roleName);

        void RemoveUserFromRole(string userName, string[] roleNames);

        void CreateUser(UserProfileDto userProfile, string password);

        int GetUserId(string userName);

        void DeleteUser(string userName);

        bool ChangePassword(string userName, string oldPassword, string newPassword);

        void ResetPassword(string userName, string newPassword);

        #endregion
    }
}