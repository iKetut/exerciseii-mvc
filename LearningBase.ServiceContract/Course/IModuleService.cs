﻿using LearningBase.Dto.Course;
using LearningBase.ServiceContract.Core.Response;
using LearningBase.ServiceContract.Course.Request;
using LearningBase.ServiceContract.Course.Response;

namespace LearningBase.ServiceContract.Course
{
    public interface IModuleService
    {
        #region Public Methods

        SaveModuleResponse Create(SaveModuleRequest request);

        BasicResponse EditName(int moduleId, int courseId, string moduleName);

        GenericGetDtoCollectionResponse<ModuleDto> Search(int courseId);

        BasicResponse Delete(int moduleId);

        BasicResponse UpdateIndex(int moduleId, int sortingIndex);

        #endregion
    }
}