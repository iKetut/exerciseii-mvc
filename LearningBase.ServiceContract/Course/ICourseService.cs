﻿using LearningBase.Dto.Course;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;
using LearningBase.ServiceContract.Course.Request;
using LearningBase.ServiceContract.Course.Response;

namespace LearningBase.ServiceContract.Course
{
    public interface ICourseService
    {
        #region Public Methods

        GenericPagedSearchResponse<CourseDto> PagedSearch(PagedSearchRequest request);

        SaveCourseResponse Create(SaveCourseRequest request);

        GenericReadDtoResponse<CourseDto> Read(int courseId);

        SaveCourseResponse Edit(SaveCourseRequest request);

        BasicResponse Delete(int courseId);

        GenericGetDtoCollectionResponse<CourseDto> GetAll();

        #endregion
    }
}