﻿using LearningBase.Dto.Course;

namespace LearningBase.ServiceContract.Course.Request
{
    public class SaveModuleRequest
    {
        #region Properties

        public ModuleDto ModuleDto { get; set; }

        #endregion
    }
}