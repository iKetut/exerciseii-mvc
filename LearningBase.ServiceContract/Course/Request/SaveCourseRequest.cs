﻿using LearningBase.Dto.Course;

namespace LearningBase.ServiceContract.Course.Request
{
    public class SaveCourseRequest
    {
        #region Properties

        public CourseDto CourseDto { get; set; }

        #endregion
    }
}