﻿using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.ServiceContract.Course.Response
{
    public class SaveModuleResponse : BasicResponse
    {
        #region Properties

        public int DtoId { get; set; }

        #endregion
    }
}