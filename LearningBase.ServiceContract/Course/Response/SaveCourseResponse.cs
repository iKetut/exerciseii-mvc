﻿using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.ServiceContract.Course.Response
{
    public class SaveCourseResponse : BasicResponse
    {
        #region Properties

        public int DtoId { get; set; }

        #endregion
    }
}