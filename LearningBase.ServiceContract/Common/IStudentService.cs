﻿using LearningBase.Dto.Common;
using LearningBase.ServiceContract.Common.Request;
using LearningBase.ServiceContract.Common.Response;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.ServiceContract.Common
{
    public interface IStudentService
    {
        #region Public Methods

        GenericPagedSearchResponse<StudentDto> PagedSearch(PagedSearchRequest request);

        SaveStudentResponse Create(CreateStudentRequest request);

        GenericReadDtoResponse<StudentDto> Read(int studentId);

        SaveStudentResponse Edit(EditStudentRequest request);

        BasicResponse Delete(int studentId);

        #endregion
    }
}