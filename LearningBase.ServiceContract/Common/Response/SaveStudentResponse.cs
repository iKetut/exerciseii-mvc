﻿using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.ServiceContract.Common.Response
{
    public class SaveStudentResponse: BasicResponse
    {
        #region Properties

        public int DtoId { get; set; }

        #endregion
    }
}