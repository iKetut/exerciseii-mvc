﻿using LearningBase.Dto.Common;

namespace LearningBase.ServiceContract.Common.Request
{
    public class EditStudentRequest
    {
        #region Properties

        public StudentDto StudentDto { get; set; }

        #endregion
    }
}