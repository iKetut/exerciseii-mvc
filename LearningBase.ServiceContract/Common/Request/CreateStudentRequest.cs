﻿using LearningBase.Dto.Common;

namespace LearningBase.ServiceContract.Common.Request
{
    public class CreateStudentRequest
    {
        #region Properties

        public StudentDto StudentDto { get; set; }

        public string PrimaryUserPassword { get; set; }

        #endregion
    }
}