﻿using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.ServiceContract.Core
{
    public interface IAccountService
    {
        #region Public Methods

        BooleanResponse Login(LoginRequest request);

        BasicResponse Logout();

        BooleanResponse IsAdministrator(string userName);

        BooleanResponse IsStudent(string userName);

        GetCurrentLoginInformationResponse GetCurrentLoginInformation();

        #endregion
    }
}