﻿namespace LearningBase.ServiceContract.Core.Request
{
    public class ChangePasswordRequest
    {
        #region Properties

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        #endregion
    }
}