﻿namespace LearningBase.ServiceContract.Core.Request
{
    public class LoginRequest
    {
        #region Properties

        public string UserName { get; set; }

        public string Password { get; set; }

        public string IpAddress { get; set; }

        #endregion
    }
}