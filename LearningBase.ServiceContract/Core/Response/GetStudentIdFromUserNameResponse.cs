﻿namespace LearningBase.ServiceContract.Core.Response
{
    public class GetStudentIdFromUserNameResponse: BasicResponse
    {
        #region Properties

        public int StudentId { get; set; }

        #endregion
    }
}