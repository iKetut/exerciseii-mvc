﻿using System.Collections.Generic;

namespace LearningBase.ServiceContract.Core.Response
{
    public class GenericGetDtoCollectionResponse<TDto> : BasicResponse
    {
        #region Fields

        private List<TDto> _dtoList;

        #endregion

        #region Properties

        public ICollection<TDto> DtoCollection
        {
            get { return this._dtoList ?? (this._dtoList = new List<TDto>()); }
        }

        #endregion
    }
}