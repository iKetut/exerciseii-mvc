﻿using System;

namespace LearningBase.ServiceContract.Core.Response
{
    public class DateTimeResponse : BasicResponse
    {
        #region Properties

        public DateTime Result { get; set; }

        #endregion
    }
}
