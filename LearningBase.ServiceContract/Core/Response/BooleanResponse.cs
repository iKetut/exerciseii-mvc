﻿namespace LearningBase.ServiceContract.Core.Response
{
    public class BooleanResponse : BasicResponse
    {
        #region Properties

        public bool Result { get; set; }

        #endregion
    }
}