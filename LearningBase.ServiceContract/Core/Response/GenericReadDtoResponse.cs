﻿namespace LearningBase.ServiceContract.Core.Response
{
    public class GenericReadDtoResponse<TDto> : BasicResponse
    {
        #region Properties

        public TDto Dto { get; set; }

        #endregion
    }
}