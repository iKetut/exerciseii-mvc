﻿namespace LearningBase.ServiceContract.Core.Response
{
    public class StringResponse : BasicResponse
    {
        #region Properties

        public string Result { get; set; }

        #endregion
    }
}
