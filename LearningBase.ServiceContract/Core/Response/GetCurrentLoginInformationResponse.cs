﻿using System.Collections.Generic;

namespace LearningBase.ServiceContract.Core.Response
{
    public class GetCurrentLoginInformationResponse : BasicResponse
    {
        #region Fields

        private List<string> _roles;

        #endregion

        #region Properties

        public bool IsAuthenticated { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public int EntityId { get; set; }

        public ICollection<string> Roles
        {
            get { return this._roles ?? (this._roles = new List<string>()); }
        }

        #endregion
    }
}