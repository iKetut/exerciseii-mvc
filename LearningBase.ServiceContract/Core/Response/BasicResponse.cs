﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LearningBase.Core;

namespace LearningBase.ServiceContract.Core.Response
{
    public class BasicResponse
    {
        #region Fields

        private Collection<Message> _messages;

        #endregion

        #region Properties

        public Collection<Message> Messages
        {
            get { return this._messages ?? (this._messages = new Collection<Message>()); }
        }

        #endregion

        #region (public) Methods

        public bool IsError()
        {
            return this.Messages.Count(item => item.Type == CoreEnum.MessageType.Error) > 0;
        }

        public bool IsContainInfo()
        {
            return this.Messages.Count(item => item.Type == CoreEnum.MessageType.Info) > 0;
        }

        public string[] GetMessageTextArray()
        {
            return this.Messages.Select(item => item.MessageText).ToArray();
        }

        public string[] GetMessageErrorTextArray()
        {
            return this.Messages.Where(item => item.Type == CoreEnum.MessageType.Error)
                .Select(item => item.MessageText)
                .ToArray();
        }

        public string[] GetMessageInfoTextArray()
        {
            return this.Messages.Where(item => item.Type == CoreEnum.MessageType.Info)
                .Select(item => item.MessageText)
                .ToArray();
        }

        public string GetErrorMessage()
        {
            var messageBuilder = new StringBuilder();
            foreach (var message in this.Messages)
            {
                messageBuilder.AppendLine(message.MessageText);
            }

            return messageBuilder.ToString().Trim();
        }

        public void AddErrorMessage(string errorMessage)
        {
            this.Messages.Add(new Message
            {
                MessageText = errorMessage,
                Type = CoreEnum.MessageType.Error
            });
        }

        public void AddInfoMessage(string infoMessage)
        {
            this.Messages.Add(new Message
            {
                MessageText = infoMessage,
                Type = CoreEnum.MessageType.Info
            });
        }

        #endregion
    }
}