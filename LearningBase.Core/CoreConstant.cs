﻿namespace LearningBase.Core
{
    public static class CoreConstant
    {
        #region Constants

        public const int DefaultOrganizationId = 1;
        public const int DefaultMaximumLogin = 5;
        public const int DefaultMaximumActivityScore = 100;

        public const string BracketRegexPrefix = @"\{([^}]*)\}";

        #endregion

        #region Nested type: AppSettingKey

        public static class AppSettingKey
        {
            #region Constants

            public const string DefaultRecordingFolder = "DefaultRecordingFolder";
            public const string DefaultCdnRecordingUrl = "cdnRecordingUrl";
            public const string DefaultSendGridApiKey = "sendGridApiKey";
            public const string DefaultStarPerformanceThresholdKey = "starPerformanceThreshold";
            public const string DefaultUpdateFlagKey = "databaseUpdateFlag";

            #endregion
        }

        #endregion

       

        #region Nested type: AssessmentTypeCode

        public static class AssessmentTypeCode
        {
            #region Constants

            public const string GeneralAssessment = "ASSESSMENT";
            public const string WritingAssessment = "WRITING_ASSESSMENT";
            public const string SpeakingAssessment = "SPEAKING_ASSESSMENT";

            #endregion
        }

        #endregion

        #region Nested type: AutomaticWritingType

        public static class AutomaticWritingType
        {
            #region Constants

            public const string PositiveText = "P";
            public const string NegativeText = "N";
            public const string QuestionText = "Q";

            #endregion
        }

        #endregion

        #region Nested type: E2PronounceConstant

        public static class E2PronounceConstant
        {
            #region Constants

            public const string SecretKey = "f5c6bd4f-d502-49ca-b981-92a7772ee2d5";

            #endregion
        }

        #endregion

        #region Nested type: EmailAddressKey

        public static class EmailAddressKey
        {
            #region Constants

            public const string DefaultTeacherEmailAddress = "teacherEmailAddress";

            #endregion
        }

        #endregion

        #region Nested type: ExamType

        public static class ExamType
        {
            #region Constants

            public const string Paid = "Paid";
            public const string Free = "Free";

            #endregion
        }

        #endregion

        #region Nested type: ExamTypeCode

        public static class ExamTypeCode
        {
            #region Constants

            public const string Pte = "PTE";
            public const string Ielts = "IELTS";
            public const string Toefl = "TOEFL";
            public const string Oet = "OET";
            public const string All = "ALL";

            #endregion
        }

        #endregion

        #region Nested type: ExportStudentDateConstant

        public class ExportStudentDateConstant
        {
            #region Constants

            public const int Today = 1;

            public const int OneWeek = 7;

            public const int TwoWeeks = 14;

            public const int CustomDate = -1;

            #endregion
        }

        #endregion

        #region Nested type: FirstLanguage

        #region Nested type : FirstLanguageCode

        public static class FirstLanguage
        {
            #region Constants

            public const string Arabic = "ARA";
            public const string Bengali = "BEN";
            public const string Cantonese = "CAN";
            public const string Chinese = "CHN";
            public const string Croatian = "CRO";
            public const string Czech = "CZE";
            public const string Danish = "DAN";
            public const string Dutch = "DUT";
            public const string English = "ENG";
            public const string Spanish = "ESP";
            public const string Finnish = "FIN";
            public const string French = "FRE";
            public const string German = "GER";
            public const string Greek = "GRE";
            public const string Hindi = "HIN";
            public const string Hungarian = "HUN";
            public const string Indonesian = "IDN";
            public const string Italian = "ITA";
            public const string Javanese = "JAV";
            public const string Japanese = "JPN";
            public const string Khmer = "KHM";
            public const string Korean = "KOR";
            public const string Kurdish = "KUR";
            public const string Mandarin = "MAN";
            public const string Marathi = "MAR";
            public const string Malay = "MYS";
            public const string Norwegian = "NOR";
            public const string Polish = "POL";
            public const string Portuguese = "POR";
            public const string Punjabi = "PUN";
            public const string Russian = "RUS";
            public const string Somali = "SOM";
            public const string Swahili = "SWA";
            public const string Swedish = "SWE";
            public const string Tagalog = "TGL";
            public const string Tamil = "TAM";
            public const string Telugu = "TEL";
            public const string Thai = "THA";
            public const string Turkish = "TUR";
            public const string Ukranian = "UKR";
            public const string Urdu = "URD";
            public const string Vietnamese = "VIE";
            public const string Yue = "YUE";

            #endregion
        }

        #endregion

        #endregion

        #region Nested type: ForgotPasswordTokenFormat

        public static class ForgotPasswordTokenFormat
        {
            #region Constants

            public const string TokenFormat = "{0}#{1}";

            #endregion
        }

        #endregion

        #region Nested type: HowFindUsCodeConstant

        public static class HowFindUsCodeConstant
        {
            #region Constants

            public const string GooggleSearch = "GOOGLE_SEARCH";
            public const string Youtube = "YOUTUBE";
            public const string Flyer = "FLYER";
            public const string MigrationAgent = "MIGRATION_AGENT";
            public const string Facebook = "FACEBOOK";
            public const string Twitter = "TWITTER";
            public const string E2LanguageBlog = "E2LANGUAGE_BLOG";
            public const string Radio = "RADIO";
            public const string Youku = "YOUKU";
            public const string WeChat = "WECHAT";
            public const string Weibo = "WEIBO";
            public const string SinaBlog = "SINABLOG";
            public const string ReferralFromFriend = "REFRRL_FROM_FRIEND";
            public const string Baidu = "BAIDU";
            public const string Other = "OTHER";

            #endregion
        }

        #endregion

        #region Nested type: LessonType

        public static class LessonType
        {
            #region Constants

            public const string Grammar = "G";
            public const string Listening = "L";
            public const string Writing = "W";
            public const string AutomaticWriting = "AW";
            public const string Reading = "R";
            public const string Speaking = "S";

            #endregion
        }

        #endregion

        #region Nested type: MessageFromType

        public static class MessageFromType
        {
            #region Constants

            // TODO: doddy / gusti - [E2LV2-55] what are the purpose of this constant? StudentFromType is not used. What is the differences between TeacherFromType and TeacherTopicFromType?
            public const string TeacherFromType = "teacher";
            public const string StudentFromType = "student";
            public const string TeacherTopicFromType = "Teacher Topic";

            #endregion
        }

        #endregion

        #region Nested type: PackageCode

        public static class PackageCode
        {
            #region Constants

            public const string Format = "{0}_{1}";
            public const string PteFree = "PTE_Free";
            public const string PtePaid = "PTE_Paid";
            public const string OetFree = "OET_Free";
            public const string OetPaid = "OET_Paid";
            public const string ToeflFree = "TOEFL_Free";
            public const string ToeflPaid = "TOEFL_Paid";
            public const string IeltsFree = "IELTS_Free";
            public const string IeltsPaid = "IELTS_Paid";

            #endregion
        }

        #endregion

        #region Nested type: PaymentType

        public static class PaymentType
        {
            #region Constants

            public const string PayPal = "PayPal";

            public const string CreditCard = "CreditCard";

            #endregion
        }

        #endregion

        #region Nested type: PreparationActivityTypeCode

        public static class PreparationActivityTypeCode
        {
            #region Constants

            public const string AzureVideo = "AZURE_VIDEO";
            public const string YoutubeVideo = "YOUTUBE_VIDEO";
            public const string DownloadPdf = "DOWNLOAD_PDF";
            public const string WriteEssayAssessment = "WRITE_ESSAY_WRITING_ASSESSMENT";
            public const string WriteEssay = "WRITE_ESSAY";
            public const string DownloadE2Pronounce = "DOWNLOAD_E2PRONOUNCE";
            public const string SummarizeWritingAssessment = "SUMMARIZE_WRITING_ASSESSMENT";
            public const string SummarizeWriting = "WRITING_SUMMARIZE";
            public const string ReadAloudPractice = "READ_ALOUD_PRACTICE";
            public const string ReadAloudAssessment = "READ_ALOUD_SPEAKING_ASSESSMENT";
            public const string AnswerShortQuestion = "SHORT_QUESTION";
            public const string RepeatSentence = "REPEAT_SENTENCE";
            public const string ReTellLectureAssessment = "RETELL_LECTURE_SPEAKING_ASSESSMENT";
            public const string ReTellLecture = "RETELL_LECTURE";
            public const string DescribeImageAssessment = "DESCRIBE_IMAGE_SPEAKING_ASSESSMENT";
            public const string Pronunciation = "PRONUNCIATION";
            public const string MultipleChoiceSingleAnswer = "MULTIPLECHOICE_SINGLEANSWER";
            public const string FillInTheBlank = "FILLIN_THEBLANK";
            public const string GrammarLesson = "GRAMMAR_LESSON";
            public const string OrderParagraph = "ORDER_PARAGRAPH";
            public const string HighlightIncorrect = "HIGHLIGHT_INCORRECT";
            public const string HighlightCorrectSummary = "HIGHLIGHT_CORRECTSUMMARY";
            public const string DescribeImagePractice = "DESCRIBE_IMAGE_PRACTICE";
            public const string MultipleChoiceMultipleAnswer = "MULTIPLECHOICE_MULTIPLEANSWER";
            public const string WriteFromDictation = "WRITEFROM_DICTATION";
            public const string ReadingWritingFillInTheBlank = "READINGWRITING_FILLINTHEBLANK";
            public const string VocabularyBuilder = "VOCABULARY_BUILDER";
            public const string TargetLink = "TARGET_LINK";
            public const string FillInTheBox = "FILLIN_THEBOX";

            #endregion
        }

        #endregion

        #region Nested type: PreparationLength

        public static class PreparationLength
        {
            #region Constants

            public const int OneWeek = 1;
            public const int TwoWeeks = 2;
            public const int ThreeWeeks = 3;
            public const int FourWeeks = 4;
            public const int SixWeeks = 6;
            public const int TwelveWeeks = 12;

            #endregion
        }

        #endregion

        #region Nested type: PreparationModuleTypeCode

        public static class PreparationModuleTypeCode
        {
            #region Constants

            public const string EssayWriting = "Essays";
            public const string Grammar = "Grammar";
            public const string Listening = "Listening";
            public const string Pronunciation = "Pronunciation";
            public const string Reading = "Reading";
            public const string Speaking = "Speaking";
            public const string Vocabulary = "Vocabulary";
            public const string Writing = "Writing";
            public const string Webinars = "Mock Tests & Webinars";

            #endregion
        }

        #endregion

        #region Nested type: ProfilePicureFileFormat

        public static class ProfilePicureFileFormat
        {
            #region Constants

            public const string ProfilePicture = "{0}_student_{1}.jpg";

            #endregion
        }

        #endregion

        #region Nested type: RecordingFileFormat

        public static class RecordingFileFormat
        {
            #region Constants

            public const string Mp3 = "recording{0}-{1}.mp3";
            public const string Wav = "recording{0}-{1}.wav";

            #endregion
        }

        #endregion

        #region Nested type: ReportCardAccessTimeFormat

        public static class ReportCardAccessTimeFormat
        {
            #region Constants

            public const string WithDay = "{0} {1}:{2}";
            public const string WithoutDay = "{0}:{1}";

            #endregion
        }

        #endregion

        #region Nested type: Role

        public static class Role
        {
            #region Constants

            public const string Administrator = "Administrator";
            public const string Student = "Student";

            #endregion
        }

        #endregion

        #region Nested type: Setting

        public static class Setting
        {
            #region Constants

            public const string DefaultLocalTimeZoneId = "AUS Eastern Standard Time";
            public const string DefaultShortDateFormat = "dd-MMM-yyyy";
            public const string DefaultLongDateFormat = "dd-MMM-yyyy hh:mm:ss tt";
            public const string DefaultShortDatePickerFormat = "dd-M-yyyy";
            public const string DefaultSendGridDateFormat = "{0:MM}/{0:dd}/{0:yyyy}";

            #endregion
        }

        #endregion

        #region Nested type: SortOrder

        public static class SortOrder
        {
            #region Constants

            public const string Ascending = "asc";
            public const string Descending = "desc";

            #endregion
        }

        #endregion

        #region Nested type: SpeakingAssessmentOptionType

        public static class SpeakingAssessmentOptionType
        {
            #region Constants

            // TODO: gusti - E2LV249 - please use code style for the content so when used in view, we know that this is a code (not labels). You can use "CONTENT", "ORAL_FLUENCY", etc.
            public const string Content = "Content";
            public const string OralFluency = "Oral Fluency";
            public const string Pronunciation = "Pronunciation";

            #endregion
        }

        #endregion

        #region Nested type: System

        public static class System
        {
            #region Constants

            public const string Version = "Version";
            public const string Maintenance = "MaintenanceMode";

            #endregion
        }

        #endregion

        #region Nested type: VideoId

        public static class VideoId
        {
            #region Fields

            public static string HowToUseVideoId = "8m40lsc6ho";

            public static string StudentPlanVideoId = "8m40lsc6ho";

            #endregion
        }

        #endregion

        #region Nested type: WistiaCodeConstant

        public static class WistiaCodeConstant
        {
            #region Constants

            public const string JavascriptPattern = "//fast.wistia.com/embed/medias/{0}.jsonp";
            public const string SectionClassPattern = "wistia_embed wistia_async_{0} video-poster videoFoam=true";
            public const string JavascriptCoreSource = "//fast.wistia.com/assets/external/E-v1.js";

            #endregion
        }

        #endregion
    }
}