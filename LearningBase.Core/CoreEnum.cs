﻿namespace LearningBase.Core
{
    public static class CoreEnum
    {
        #region MessageType enum

        public enum MessageType
        {
            Error,
            Info,
            Warning
        }

        #endregion
    }
}