﻿using System;

namespace LearningBase.Core
{
    public static class DateTimeManager
    {
        #region (public) Methods

        public static DateTime GetCurrentLocalDate(string timeZoneId)
        {
            return GetCurrentLocalDateTime(timeZoneId).Date;
        }

        public static DateTime GetCurrentLocalDateTime(string timeZoneId)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

            var currentDateTimeUtc = DateTime.UtcNow;
            var currentLocalDate = currentDateTimeUtc.Add(timeZoneInfo.BaseUtcOffset);

            return currentLocalDate;
        }

        public static DateTime UtcToLocalDateTime(string timeZoneId, DateTime utcDateTime)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            var currentLocalDate = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, timeZoneInfo);
            return currentLocalDate;
        }

        public static DateTime LocalToUtcDateTime(string timeZoneId, DateTime dateTime)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            var utcDateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
            return utcDateTime;
        }

        #endregion
    }
}