﻿using System.Collections.Generic;
using System.Linq;
using LearningBase.DatabaseModel;
using LearningBase.Dto.Utility;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Utility;

namespace LearningBase.Repository.Utility
{
    public class UserLoginActivityRepository : EntityBaseRepository<utl_UserLoginActivity, UserLoginActivityDto>,
        IUserLoginActivityRepository
    {
        #region Constructors

        public UserLoginActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion

        #region IUserLoginActivityRepository Members

        public IEnumerable<UserLoginActivityDto> SearchByUserName(string userName)
        {
            return this.Search(this.DbSet.Where(item => item.UserName == userName));
        }

        #endregion

        #region Protected Methods

        protected override void DtoToEntity(UserLoginActivityDto dto, utl_UserLoginActivity entity)
        {
            this.EntityMapper.Map(dto, entity);
        }

        protected override void EntityToDto(utl_UserLoginActivity entity, UserLoginActivityDto dto)
        {
            this.EntityMapper.Map(entity, dto);
        }

        #endregion
    }
}