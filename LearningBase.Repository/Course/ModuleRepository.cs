﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using LearningBase.DatabaseModel;
using LearningBase.Dto.Course;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Course;

namespace LearningBase.Repository.Course
{
    public class ModuleRepository : EntityBaseRepository<crs_Module, ModuleDto>,
        IModuleRepository
    {
        #region Constants

        private const string UpdateModuleIndexCommand = "EXEC usp_crs_UpdateModuleIndex @moduleId, @newIndex";

        #endregion

        #region Constructors

        public ModuleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion

        #region IModuleRepository Members

        public bool IsModuleExist(int id, int courseId, string name)
        {
            return this.DbSet.Any(item => item.CourseId == courseId && item.Name == name && item.Id != id);
        }

        public int GetNextSortingIndex(int courseId)
        {
            var currentSortingIndex =
                this.DbSet.Where(item => item != null && item.CourseId == courseId)
                    .DefaultIfEmpty()
                    .Max(item => item == null ? 0 : item.SortingIndex);
            return currentSortingIndex + 1;
        }

        public IEnumerable<ModuleDto> Search(int courseId)
        {
            return this.Search(this.DbSet.Where(item => item.CourseId == courseId));
        }

        public void UpdateIndex(int moduleId, int newIndex)
        {
            this.UnitOfWork.ExecuteSqlCommand(UpdateModuleIndexCommand,
                new object[]
                {
                    new SqlParameter("moduleId", moduleId),
                    new SqlParameter("newIndex", newIndex)
                });
        }

        #endregion

        #region Protected Methods

        protected override void DtoToEntity(ModuleDto dto, crs_Module entity)
        {
            this.EntityMapper.Map(dto, entity);
        }

        protected override void EntityToDto(crs_Module entity, ModuleDto dto)
        {
            this.EntityMapper.Map(entity, dto);
        }

        #endregion
    }
}