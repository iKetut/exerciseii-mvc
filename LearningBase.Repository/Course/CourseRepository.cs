﻿using System.Linq;
using LearningBase.DatabaseModel;
using LearningBase.Dto.Course;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Course;

namespace LearningBase.Repository.Course
{
    public class CourseRepository : EntityBaseRepository<crs_Course, CourseDto>,
        ICourseRepository
    {
        #region Constructors

        public CourseRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion

        #region ICourseRepository Members

        public bool IsCodeExist(int id, string code)
        {
            return this.DbSet.Any(item => item.Code == code && item.Id != id);
        }

        #endregion

        #region Protected Methods

        protected override void DtoToEntity(CourseDto dto, crs_Course entity)
        {
            this.EntityMapper.Map(dto, entity);
        }

        protected override void EntityToDto(crs_Course entity, CourseDto dto)
        {
            this.EntityMapper.Map(entity, dto);
        }

        #endregion
    }
}