﻿using System.Data.SqlClient;
using System.Linq;
using LearningBase.DatabaseModel;
using LearningBase.Dto.Common;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Common;

namespace LearningBase.Repository.Common
{
    public class StudentRepository : EntityBaseRepository<com_Student, StudentDto>,
        IStudentRepository
    {
        #region Constants

        private const string DeleteStudentCommand = "EXEC usp_com_DeleteStudent @studentId";

        #endregion

        #region Constructors

        public StudentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion

        #region IStudentRepository Members

        public bool IsStudentEmailExist(int studentId, string emailAdress)
        {
            return this.DbSet.Any(item => item.UserProfile.EmailAddress.Equals(emailAdress) && item.Id != studentId);
        }

        public void Delete(int studentId)
        {
            this.UnitOfWork
                .ExecuteSqlCommand(DeleteStudentCommand,
                    new object[]
                    {
                        new SqlParameter("studentId", studentId)
                    });
        }

        public StudentDto ReadByUserId(int userId)
        {
            return this.Read(this.DbSet.Where(item => item.UserId == userId));
        }

        #endregion

        #region Protected Methods

        protected override void DtoToEntity(StudentDto dto, com_Student entity)
        {
            this.EntityMapper.Map(dto, entity);
        }

        protected override void EntityToDto(com_Student entity, StudentDto dto)
        {
            this.EntityMapper.Map(entity, dto);

            dto.UserName = entity.UserProfile.UserName;
            dto.FirstName = entity.UserProfile.FirstName;
            dto.LastName = entity.UserProfile.LastName;
            dto.EmailAddress = entity.UserProfile.EmailAddress;
            dto.CourseName = entity.crs_Course.Name;
        }

        #endregion
    }
}