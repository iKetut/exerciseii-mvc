﻿using System;
using System.Data.SqlClient;
using System.Linq;
using LearningBase.Dto.Common;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Common;

namespace LearningBase.Repository.Common
{
    public class UserProfileRepository : CustomBaseRepository, IUserProfileRepository
    {
        #region Constructors

        public UserProfileRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion

        #region IUserProfileRepository Members

        public UserProfileDto Read(int userId)
        {
            return this.UnitOfWork.SqlQuery<UserProfileDto>("SELECT * FROM UserProfile WHERE UserId = @userId",
                new object[]
                {
                    new SqlParameter("userId", userId)
                }).FirstOrDefault();
        }

        public UserProfileDto Read(string userName)
        {
            try
            {
                return this.UnitOfWork.SqlQuery<UserProfileDto>("SELECT * FROM UserProfile WHERE UserName = @userName",
                    new object[]
                    {
                        new SqlParameter("userName", userName)
                    }).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public void Update(UserProfileDto userProfile)
        {
            this.UnitOfWork.ExecuteSqlCommand(
                "UPDATE UserProfile SET FirstName = @firstname, LastName = @lastname, EmailAddress = @email, LastModifiedBy= @lastModifiedBy, LastModifiedDateTime= @lastModifiedDateTime WHERE UserId = @userId",
                new object[]
                {
                    new SqlParameter("userId", userProfile.Id),
                    new SqlParameter("firstname", userProfile.FirstName),
                    new SqlParameter("lastname", userProfile.LastName),
                    new SqlParameter("email", userProfile.EmailAddress),
                    new SqlParameter("lastModifiedBy", userProfile.LastModifiedBy),
                    new SqlParameter("lastModifiedDateTime", userProfile.LastModifiedDateTime)
                });
        }

        #endregion
    }
}
