﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Transactions;
using LearningBase.DatabaseModel;
using LearningBase.RepositoryContract;

namespace LearningBase.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Constants

        private const int DefaultCommandTimeout = 60;

        #endregion

        #region Fields

        private readonly LearningBaseDatabaseContainer _databaseEntity;
        private TransactionScope _transaction;

        #endregion

        #region Constructors

        public UnitOfWork()
        {
            this._databaseEntity = new LearningBaseDatabaseContainer();
        }

        #endregion

        #region IUnitOfWork Members

        public int ExecuteSqlCommand(string command, object[] parameters)
        {
            return this._databaseEntity.Database.ExecuteSqlCommand(command, parameters);
        }

        public IEnumerable<T> SqlQuery<T>(string sqlQuery, object[] parameters, int commandTimeout = 60)
        {
            ((IObjectContextAdapter) this._databaseEntity).ObjectContext.CommandTimeout = commandTimeout;
            var result = this._databaseEntity.Database.SqlQuery<T>(sqlQuery, parameters);
            ((IObjectContextAdapter) this._databaseEntity).ObjectContext.CommandTimeout = DefaultCommandTimeout;

            return result;
        }

        public void Dispose()
        {
            this._databaseEntity.Dispose();
        }

        public void StartTransaction()
        {
            this._transaction = new TransactionScope();
        }

        public void Commit()
        {
            this._databaseEntity.SaveChanges();
            this._transaction.Complete();
        }

        public int SaveChanges()
        {
            try
            {
                return this._databaseEntity.SaveChanges();
            }
            catch (DbEntityValidationException exception)
            {
                // TODO: This is just for debugging purpose.
                var x = string.Empty;
                throw;
            }
        }

        public IDbSet<T> GetDbSet<T>() where T : class
        {
            return this._databaseEntity.Set<T>();
        }

        public EntityState GetEntityState<T>(T entity) where T : class
        {
            return this._databaseEntity.Entry(entity).State;
        }

        public void SetModified<T>(T entity) where T : class
        {
            this._databaseEntity.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public void ApplyEntityValues<T>(T currentEntity, T updatedEntity) where T : class
        {
            var attachedEntry = this._databaseEntity.Entry(currentEntity);
            attachedEntry.CurrentValues.SetValues(updatedEntity);
        }

        
        #endregion
    }
}