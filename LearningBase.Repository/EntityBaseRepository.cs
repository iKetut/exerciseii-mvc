﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using LearningBase.Core;
using LearningBase.RepositoryContract;

namespace LearningBase.Repository
{
    public abstract class EntityBaseRepository<TEntity, TDto> : IEntityBaseRepository<TDto>
        where TEntity : class, new()
        where TDto : class, new()
    {
        #region Constructors

        protected EntityBaseRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
            {
                throw new ArgumentNullException(nameof(unitOfWork));
            }

            this.UnitOfWork = unitOfWork;
            this.DbSet = this.UnitOfWork.GetDbSet<TEntity>();

            var config = new MapperConfiguration(cfg => cfg.CreateMap<TEntity, TDto>().ReverseMap());

            this.EntityMapper = config.CreateMapper();
        }

        #endregion

        #region Properties

        public IMapper EntityMapper { get; }

        protected IDbSet<TEntity> DbSet { get; }

        #endregion

        #region IEntityBaseRepository<TDto> Members

        public IUnitOfWork UnitOfWork { get; }

        #endregion

        #region (public) Methods

        public TDto Read(object primaryKey)
        {
            var entity = this.DbSet.Find(primaryKey);
            if (entity == null)
            {
                return null;
            }

            var dto = new TDto();
            this.EntityToDto(entity, dto);
            return dto;
        }

        public PagedSearchResult<TDto> PagedSearch(PagedSearchParameter parameter)
        {
            var queryable = string.IsNullOrEmpty(parameter.Keyword)
                ? this.DbSet.AsQueryable()
                : this.GetKeywordPagedSearchQueryable(parameter.Keyword);

            return this.GetPagedSearchEnumerable(parameter, queryable);
        }

        public int Count()
        {
            return this.DbSet.Count();
        }

        public bool Exists(object primaryKey)
        {
            return this.DbSet.Find(primaryKey) != null;
        }

        public virtual int Insert(TDto dto)
        {
            var entity = new TEntity();
            this.DtoToEntity(dto, entity);

            dynamic obj = this.DbSet.Add(entity);
            this.UnitOfWork.SaveChanges();

            return obj.Id;
        }

        public virtual void Update(TDto dto)
        {
            var entity = new TEntity();
            this.DtoToEntity(dto, entity);

            var entityState = this.UnitOfWork.GetEntityState(entity);
            if (entityState == EntityState.Detached)
            {
                var key = this.GetPrimaryKey(entity);
                var currentEntity = this.DbSet.Find(key);
                if (currentEntity != null)
                {
                    this.UnitOfWork.ApplyEntityValues(currentEntity, entity);
                }
                else
                {
                    this.DbSet.Attach(entity);
                    this.UnitOfWork.SetModified(entity);
                }
            }

            this.UnitOfWork.SaveChanges();
        }

        public void Delete(object primaryKey)
        {
            var entity = this.DbSet.Find(primaryKey);
            if (entity == null)
            {
                throw new InvalidOperationException("Record to be deleted does not exists");
            }

            this.DbSet.Remove(entity);
            this.UnitOfWork.SaveChanges();
        }

        public IEnumerable<TDto> GetAll()
        {
            return this.Search(this.DbSet);
        }

        #endregion

        #region (protected) Methods

        protected TDto Read(IQueryable<TEntity> queryable)
        {
            var entity = queryable.SingleOrDefault();
            if (entity == null)
            {
                return null;
            }

            var dto = new TDto();
            this.EntityToDto(entity, dto);
            return dto;
        }

        protected IEnumerable<TDto> Search(IEnumerable<TEntity> queryable)
        {
            var dtoList = new List<TDto>();

            var entityList = queryable.ToList();
            foreach (var entity in entityList)
            {
                var dto = new TDto();
                this.EntityToDto(entity, dto);
                dtoList.Add(dto);
            }

            return dtoList.AsEnumerable();
        }

        protected virtual IQueryable<TEntity> GetKeywordPagedSearchQueryable(string keyword)
        {
            return this.DbSet.AsQueryable();
        }

        protected abstract void DtoToEntity(TDto dto, TEntity entity);

        protected abstract void EntityToDto(TEntity entity, TDto dto);

        protected PagedSearchResult<TDto> GetPagedSearchEnumerable(PagedSearchParameter parameter,
            IQueryable<TEntity> queryable)
        {
            var result = new PagedSearchResult<TDto>();

            queryable = string.IsNullOrEmpty(parameter.OrderByFieldName)
                ? this.GetOrderedQueryableEntity(queryable, "Id", CoreConstant.SortOrder.Ascending)
                : this.GetOrderedQueryableEntity(queryable, parameter.OrderByFieldName, parameter.SortOrder);

            result.Count = queryable.Count();

            var entityList = parameter.PageSize == -1
                ? queryable.ToList()
                : queryable.Skip(parameter.PageIndex*parameter.PageSize).Take(parameter.PageSize).ToList();

            foreach (var entity in entityList)
            {
                var dto = new TDto();
                this.EntityToDto(entity, dto);
                result.Result.Add(dto);
            }

            return result;
        }

        protected PagedSearchResult<TDto> GetPagedSearchEnumerable(PagedSearchParameter parameter,
            IQueryable<TDto> queryable)
        {
            var result = new PagedSearchResult<TDto>();

            queryable = string.IsNullOrEmpty(parameter.OrderByFieldName)
                ? this.GetOrderedQueryableEntity(queryable, "Id", CoreConstant.SortOrder.Ascending)
                : this.GetOrderedQueryableEntity(queryable, parameter.OrderByFieldName, parameter.SortOrder);

            result.Count = queryable.Count();

            var entityList = parameter.PageSize == -1
                ? queryable.ToList()
                : queryable.Skip(parameter.PageIndex*parameter.PageSize).Take(parameter.PageSize).ToList();

            foreach (var entity in entityList)
            {
                result.Result.Add(entity);
            }

            return result;
        }

        protected PagedSearchResult<TDto> GetCustomPagedSearchEnumerable(PagedSearchParameter parameter,
            IQueryable<TDto> queryable)
        {
            var result = new PagedSearchResult<TDto>();

            queryable = string.IsNullOrEmpty(parameter.OrderByFieldName)
                ? this.GetOrderedQueryableEntity(queryable, "Id", CoreConstant.SortOrder.Ascending)
                : this.GetOrderedQueryableEntity(queryable, parameter.OrderByFieldName, parameter.SortOrder);

            result.Count = queryable.Count();

            var entityList = parameter.PageSize == -1
                ? queryable.ToList()
                : queryable.Skip(parameter.PageIndex*parameter.PageSize).Take(parameter.PageSize).ToList();

            foreach (var entity in entityList)
            {
                result.Result.Add(entity);
            }

            return result;
        }

        #endregion

        #region (private) Methods

        private IOrderedQueryable<TEntity> GetOrderedQueryableEntity(IQueryable<TEntity> queryable,
            string orderByFieldName,
            string sortOrder)
        {
            var orderByMethodName = "OrderBy";
            if (sortOrder.Equals(CoreConstant.SortOrder.Descending))
            {
                orderByMethodName = "OrderByDescending";
            }

            var typeParams = new[] {Expression.Parameter(typeof(TEntity), "")};

            var pi = typeof(TEntity).GetProperty(orderByFieldName);

            return (IOrderedQueryable<TEntity>) queryable.Provider.CreateQuery(
                Expression.Call(
                    typeof(Queryable),
                    orderByMethodName,
                    new[] {typeof(TEntity), pi.PropertyType},
                    queryable.Expression,
                    Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
                );
        }

        private IOrderedQueryable<TDto> GetOrderedQueryableEntity(IQueryable<TDto> queryable,
            string orderByFieldName,
            string sortOrder)
        {
            var orderByMethodName = "OrderBy";
            if (sortOrder.Equals(CoreConstant.SortOrder.Descending))
            {
                orderByMethodName = "OrderByDescending";
            }

            var typeParams = new[] {Expression.Parameter(typeof(TDto), "")};

            var pi = typeof(TDto).GetProperty(orderByFieldName);

            return (IOrderedQueryable<TDto>) queryable.Provider.CreateQuery(
                Expression.Call(
                    typeof(Queryable),
                    orderByMethodName,
                    new[] {typeof(TDto), pi.PropertyType},
                    queryable.Expression,
                    Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
                );
        }

        private object GetPrimaryKey<T>(T entity)
        {
            var property = entity.GetType().GetProperties().FirstOrDefault(prop => prop.Name == "Id");
            if (property != null)
            {
                return property.GetValue(entity, null);
            }

            throw new InvalidOperationException("Invalid entity.");
        }

        #endregion
    }
}