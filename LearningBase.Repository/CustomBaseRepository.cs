﻿using System;
using LearningBase.RepositoryContract;

namespace LearningBase.Repository
{
    public class CustomBaseRepository : ICustomBaseRepository
    {
        #region Constructors

        protected CustomBaseRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
            {
                throw new ArgumentNullException("unitOfWork");
            }

            this.UnitOfWork = unitOfWork;
        }

        #endregion

        #region Properties

        public IUnitOfWork UnitOfWork { get; }

        #endregion
    }
}