﻿using System.Collections.Generic;
using LearningBase.Dto.Utility;

namespace LearningBase.RepositoryContract.Utility
{
    public interface IUserLoginActivityRepository : IEntityBaseRepository<UserLoginActivityDto>
    {
        #region Public Methods

        IEnumerable<UserLoginActivityDto> SearchByUserName(string userName);

        #endregion
    }
}