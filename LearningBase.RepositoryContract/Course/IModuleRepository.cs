﻿using System.Collections.Generic;
using LearningBase.Dto.Course;

namespace LearningBase.RepositoryContract.Course
{
    public interface IModuleRepository : IEntityBaseRepository<ModuleDto>
    {
        #region Public Methods

        bool IsModuleExist(int id, int courseId, string name);

        int GetNextSortingIndex(int courseId);

        IEnumerable<ModuleDto> Search(int courseId);

        void UpdateIndex(int moduleId, int newIndex);

        #endregion
    }
}