﻿using LearningBase.Dto.Course;

namespace LearningBase.RepositoryContract.Course
{
    public interface ICourseRepository : IEntityBaseRepository<CourseDto>
    {
        #region Public Methods

        bool IsCodeExist(int id, string code);

        #endregion
    }
}