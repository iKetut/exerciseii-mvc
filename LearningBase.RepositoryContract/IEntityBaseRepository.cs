﻿using System.Collections.Generic;

namespace LearningBase.RepositoryContract
{
    public interface IEntityBaseRepository<TDto>
    {
        #region Properties

        IUnitOfWork UnitOfWork { get; }

        #endregion

        #region (public) Methods

        TDto Read(object primaryKey);

        IEnumerable<TDto> GetAll();

        PagedSearchResult<TDto> PagedSearch(PagedSearchParameter parameter);

        int Count();

        bool Exists(object primaryKey);

        int Insert(TDto dto);

        void Update(TDto dto);

        void Delete(object primaryKey);

        #endregion
    }
}