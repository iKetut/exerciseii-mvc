﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;

namespace LearningBase.RepositoryContract
{
    public interface IUnitOfWork : IDisposable
    {
        #region Public Methods

        int ExecuteSqlCommand(string command, object[] parameters);

        IEnumerable<T> SqlQuery<T>(string sqlQuery, object[] parameters, int commandTimeout = 60);

        void StartTransaction();

        void Commit();

        int SaveChanges();

        IDbSet<T> GetDbSet<T>() where T : class;

        System.Data.Entity.EntityState GetEntityState<T>(T entity) where T : class;

        void SetModified<T>(T entity) where T : class;

        void ApplyEntityValues<T>(T currentEntity, T updatedEntity) where T : class;

        #endregion
    }
}