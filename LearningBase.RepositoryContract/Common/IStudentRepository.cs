﻿using LearningBase.Dto.Common;

namespace LearningBase.RepositoryContract.Common
{
    public interface IStudentRepository : IEntityBaseRepository<StudentDto>
    {
        #region Public Methods

        bool IsStudentEmailExist(int studentId, string emailAdress);

        void Delete(int studentId);

        StudentDto ReadByUserId(int userId);

        #endregion
    }
}