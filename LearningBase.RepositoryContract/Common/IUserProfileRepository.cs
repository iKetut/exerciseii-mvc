﻿using LearningBase.Dto.Common;

namespace LearningBase.RepositoryContract.Common
{
    public interface IUserProfileRepository : ICustomBaseRepository
    {
        #region Public Methods

        UserProfileDto Read(int userId);

        UserProfileDto Read(string userName);

        void Update(UserProfileDto userProfile);

        #endregion
    }
}