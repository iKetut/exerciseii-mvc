﻿using LearningBase.Core;
using LearningBase.Dto.Course;
using LearningBase.RepositoryContract.Course;
using LearningBase.ServiceContract;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;
using LearningBase.ServiceContract.Course;
using LearningBase.ServiceContract.Course.Request;
using LearningBase.ServiceContract.Course.Response;

namespace LearningBase.Service.Course
{
    public class CourseService : ServiceBase, ICourseService
    {
        #region Fields

        private readonly ICourseRepository _courseRepository;

        #endregion

        #region Constructors

        public CourseService(ISecurityProvider securityProvider, ICourseRepository courseRepository)
            : base(securityProvider)
        {
            this._courseRepository = courseRepository;
        }

        #endregion

        #region ICourseService Members

        public GenericPagedSearchResponse<CourseDto> PagedSearch(PagedSearchRequest request)
        {
            return this.PagedSearch(this._courseRepository, request);
        }

        public SaveCourseResponse Create(SaveCourseRequest request)
        {
            var response = new SaveCourseResponse();

            if (!this.IsValidCourse(request.CourseDto, response))
            {
                return response;
            }

            this.PopulateAuditFieldsOnCreate(request.CourseDto);
            response.DtoId = this._courseRepository.Insert(request.CourseDto);

            return response;
        }

        public GenericReadDtoResponse<CourseDto> Read(int courseId)
        {
            var response = new GenericReadDtoResponse<CourseDto>
            {
                Dto = this._courseRepository.Read(courseId)
            };

            return response;
        }

        public SaveCourseResponse Edit(SaveCourseRequest request)
        {
            var response = new SaveCourseResponse();

            if (!this.IsValidCourse(request.CourseDto, response))
            {
                return response;
            }

            var dto = this._courseRepository.Read(request.CourseDto.Id);
            if (dto != null)
            {
                dto.Code = request.CourseDto.Code;
                dto.Name = request.CourseDto.Name;
                dto.Description = request.CourseDto.Description;

                this.PopulateAuditFieldsOnUpdate(dto);
                this._courseRepository.Update(dto);

                response.DtoId = request.CourseDto.Id;
            }

            return response;
        }

        public BasicResponse Delete(int courseId)
        {
            this._courseRepository.Delete(courseId);

            return new BasicResponse();
        }

        public GenericGetDtoCollectionResponse<CourseDto> GetAll()
        {
            return this.GetAll(this._courseRepository);
        }

        #endregion

        #region Private Methods

        private bool IsValidCourse(CourseDto dto, BasicResponse response)
        {
            if (this._courseRepository.IsCodeExist(dto.Id, dto.Code))
            {
                response.AddErrorMessage(string.Format(CoreResource.Course_CodeShouldBeUnique, dto.Code));
            }

            return !response.IsError();
        }

        #endregion
    }
}