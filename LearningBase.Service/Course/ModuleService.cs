﻿using System;
using System.Collections.Generic;
using LearningBase.Core;
using LearningBase.Dto.Course;
using LearningBase.RepositoryContract.Course;
using LearningBase.ServiceContract;
using LearningBase.ServiceContract.Core.Response;
using LearningBase.ServiceContract.Course;
using LearningBase.ServiceContract.Course.Request;
using LearningBase.ServiceContract.Course.Response;

namespace LearningBase.Service.Course
{
    public class ModuleService : ServiceBase, IModuleService
    {
        #region Fields

        private readonly IModuleRepository _moduleRepository;

        #endregion

        #region Constructors

        public ModuleService(ISecurityProvider securityProvider, IModuleRepository moduleRepository)
            : base(securityProvider)
        {
            this._moduleRepository = moduleRepository;
        }

        #endregion

        #region IModuleService Members

        public SaveModuleResponse Create(SaveModuleRequest request)
        {
            var response = new SaveModuleResponse();

            request.ModuleDto.SortingIndex = this._moduleRepository.GetNextSortingIndex(request.ModuleDto.CourseId);
            this.PopulateAuditFieldsOnCreate(request.ModuleDto);
            response.DtoId = this._moduleRepository.Insert(request.ModuleDto);

            return response;
        }

        public BasicResponse EditName(int moduleId, int courseId, string moduleName)
        {
            var response = new BasicResponse();

            var moduleDto = this._moduleRepository.Read(moduleId);
            moduleDto.CourseId = courseId;
            moduleDto.Name = moduleName;

            if (this.IsValidModule(moduleDto, response))
            {
                this.PopulateAuditFieldsOnUpdate(moduleDto);
                this._moduleRepository.Update(moduleDto);
            }

            return response;
        }

        public GenericGetDtoCollectionResponse<ModuleDto> Search(int courseId)
        {
            Func<IEnumerable<ModuleDto>> searchFunction = () => this._moduleRepository.Search(courseId);
            return this.GetAll(searchFunction);
        }

        public BasicResponse Delete(int moduleId)
        {
            this._moduleRepository.Delete(moduleId);
            return new BasicResponse();
        }

        public BasicResponse UpdateIndex(int moduleId, int sortingIndex)
        {
            this._moduleRepository.UpdateIndex(moduleId, sortingIndex);
            return new BasicResponse();
        }

        #endregion

        #region Private Methods

        private bool IsValidModule(ModuleDto dto, BasicResponse response)
        {
            if (this._moduleRepository.IsModuleExist(dto.Id, dto.CourseId, dto.Name))
            {
                response.AddErrorMessage(string.Format(CoreResource.Module_NameShouldBeUnique, dto.Name));
            }

            return !response.IsError();
        }

        #endregion
    }
}