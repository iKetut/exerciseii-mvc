﻿using LearningBase.Core;
using LearningBase.Dto.Common;
using LearningBase.RepositoryContract.Common;
using LearningBase.ServiceContract;
using LearningBase.ServiceContract.Common;
using LearningBase.ServiceContract.Common.Request;
using LearningBase.ServiceContract.Common.Response;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.Service.Common
{
    public class StudentService : ServiceBase, IStudentService
    {
        #region Fields

        private readonly IStudentRepository _studentRepository;
        private readonly IUserProfileRepository _userProfileRepository;

        #endregion

        #region Constructors

        public StudentService(ISecurityProvider securityProvider,
            IStudentRepository studentRepository,
            IUserProfileRepository userProfileRepository)
            : base(securityProvider)
        {
            this._studentRepository = studentRepository;
            this._userProfileRepository = userProfileRepository;
        }

        #endregion

        #region IStudentService Members

        public GenericPagedSearchResponse<StudentDto> PagedSearch(PagedSearchRequest request)
        {
            return this.PagedSearch(this._studentRepository, request);
        }

        public SaveStudentResponse Create(CreateStudentRequest request)
        {
            var response = new SaveStudentResponse();

            if (!this.IsValidCreateRequest(request.StudentDto, response))
            {
                return response;
            }

            var userProfile = new UserProfileDto
            {
                FirstName = request.StudentDto.FirstName,
                LastName = request.StudentDto.LastName,
                EmailAddress = request.StudentDto.EmailAddress,
                UserName = request.StudentDto.UserName
            };

            this.PopulateAuditFieldsOnCreate(userProfile);

            this.SecurityProvider.CreateUser(userProfile, request.PrimaryUserPassword);
            var userId = this.SecurityProvider.GetUserId(userProfile.UserName);

            this.SecurityProvider.AddUserToRole(userProfile.UserName, CoreConstant.Role.Student);

            var studentDto = new StudentDto
            {
                UserId = userId,
                CourseId = request.StudentDto.CourseId
            };

            this.PopulateAuditFieldsOnCreate(studentDto);
            response.DtoId = this._studentRepository.Insert(studentDto);

            return response;
        }

        public GenericReadDtoResponse<StudentDto> Read(int studentId)
        {
            var response = new GenericReadDtoResponse<StudentDto>
            {
                Dto = this._studentRepository.Read(studentId)
            };

            return response;
        }

        public SaveStudentResponse Edit(EditStudentRequest request)
        {
            var response = new SaveStudentResponse();
            if (!this.IsValidEditRequest(request.StudentDto, response))
            {
                return response;
            }

            var student = this._studentRepository.Read(request.StudentDto.Id);
            if (student != null)
            {
                student.CourseId = request.StudentDto.CourseId;
                this.PopulateAuditFieldsOnUpdate(student);
                this._studentRepository.Update(student);
            }

            var userProfile = this._userProfileRepository.Read(request.StudentDto.UserId);

            userProfile.Id = request.StudentDto.UserId;
            userProfile.FirstName = request.StudentDto.FirstName;
            userProfile.LastName = request.StudentDto.LastName;
            userProfile.EmailAddress = request.StudentDto.EmailAddress;

            this.PopulateAuditFieldsOnUpdate(userProfile);
            this._userProfileRepository.Update(userProfile);

            response.DtoId = request.StudentDto.Id;

            return response;
        }

        public BasicResponse Delete(int studentId)
        {
            this._studentRepository.Delete(studentId);

            return new BasicResponse();
        }

        #endregion

        #region Private Methods

        private bool IsValidCreateRequest(StudentDto studentDto, BasicResponse response)
        {
            return this.IsValidUserName(studentDto, response) && this.IsValidEmailAddress(studentDto, response);
        }

        private bool IsValidEditRequest(StudentDto studentDto, BasicResponse response)
        {
            return this.IsValidEmailAddress(studentDto, response);
        }

        private bool IsValidUserName(StudentDto studentDto, BasicResponse response)
        {
            // Valid if there is not other user name exist in database.
            if (this.SecurityProvider.IsUserExist(studentDto.UserName))
            {
                response.AddErrorMessage(string.Format(CoreResource.Student_UserNameShouldBeUnique,
                    studentDto.UserName));
            }

            return !response.IsError();
        }

        private bool IsValidEmailAddress(StudentDto studentDto, BasicResponse response)
        {
            // Valid if there there is no other user using this email address
            if (this._studentRepository.IsStudentEmailExist(studentDto.Id, studentDto.EmailAddress))
            {
                response.AddErrorMessage(string.Format(CoreResource.Student_EmailShouldBeUnique,
                    studentDto.EmailAddress));
            }

            return !response.IsError();
        }

        #endregion
    }
}