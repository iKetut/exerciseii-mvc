﻿using System;
using System.Web.Security;
using LearningBase.Dto.Common;
using LearningBase.ServiceContract;
using WebMatrix.WebData;

namespace LearningBase.Service
{
    public class SecurityProvider : ISecurityProvider
    {
        #region ISecurityProvider Members

        public bool IsAuthenticated
        {
            get { return WebSecurity.IsAuthenticated; }
        }

        public int CurrentUserId
        {
            get { return WebSecurity.CurrentUserId; }
        }

        public string CurrentUserName
        {
            get { return WebSecurity.CurrentUserName; }
        }


        public bool Login(string userName, string password)
        {
            try
            {
                return WebSecurity.Login(userName, password);
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public void Logout()
        {
            WebSecurity.Logout();
        }

        public bool IsUserExist(string userName)
        {
            return WebSecurity.UserExists(userName);
        }

        public bool IsUserInRole(string userName, string roleName)
        {
            return Roles.IsUserInRole(userName, roleName);
        }

        public string[] GetRoles(string userName)
        {
            return Roles.GetRolesForUser(userName);
        }

        public void AddUserToRole(string userName, string roleName)
        {
            Roles.AddUserToRole(userName, roleName);
        }

        public void RemoveUserFromRole(string userName, string[] roleNames)
        {
            Roles.RemoveUserFromRoles(userName, roleNames);
        }

        public void CreateUser(UserProfileDto userProfile, string password)
        {
            WebSecurity.CreateUserAndAccount(userProfile.UserName,
                password,
                new
                {
                    userProfile.FirstName,
                    userProfile.LastName,
                    userProfile.EmailAddress,
                    userProfile.MaximumLogin,
                    userProfile.CreatedBy,
                    userProfile.CreatedDateTime,
                    userProfile.LastModifiedBy,
                    userProfile.LastModifiedDateTime
                });
        }

        public int GetUserId(string userName)
        {
            return WebSecurity.GetUserId(userName);
        }

        public void DeleteUser(string userName)
        {
            Membership.Provider.DeleteUser(userName, true);
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            var user = Membership.GetUser(userName);
            return user != null && user.ChangePassword(oldPassword, newPassword);
        }

        public void ResetPassword(string userName, string newPassword)
        {
            var token = WebSecurity.GeneratePasswordResetToken(userName);
            WebSecurity.ResetPassword(token, newPassword);
        }

        #endregion
    }
}