﻿using System;
using System.Linq;
using LearningBase.Core;
using LearningBase.Dto.Utility;
using LearningBase.RepositoryContract.Common;
using LearningBase.RepositoryContract.Utility;
using LearningBase.ServiceContract;
using LearningBase.ServiceContract.Core;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.Service.Core
{
    public class AccountService : ServiceBase, IAccountService
    {
        #region Fields

        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IUserLoginActivityRepository _userLoginActivityRepository;
        private readonly IStudentRepository _studentRepository;

        #endregion

        #region Constructors

        public AccountService(ISecurityProvider securityProvider,
            IUserLoginActivityRepository userLoginActivityRepository,
            IUserProfileRepository userProfileRepository,
            IStudentRepository studentRepository)
            : base(securityProvider)
        {
            this._userLoginActivityRepository = userLoginActivityRepository;
            this._userProfileRepository = userProfileRepository;
            this._studentRepository = studentRepository;
        }

        #endregion

        #region IAccountService Members

        public BooleanResponse Login(LoginRequest request)
        {
            var response = new BooleanResponse
            {
                Result = this.SecurityProvider.Login(request.UserName, request.Password)
            };

            if (!response.Result)
            {
                response.AddErrorMessage(CoreResource.Account_UserNamePasswordIncorrect);
            }
            else if (!this.IsValidLoginActivity(request))
            {
                response.Result = false;
                response.AddErrorMessage(CoreResource.Account_UserLoginActivityNotValid);
            }

            return response;
        }

        public BasicResponse Logout()
        {
            this.SecurityProvider.Logout();
            return new BasicResponse();
        }

        public BooleanResponse IsAdministrator(string userName)
        {
            var response = new BooleanResponse
            {
                Result = this.SecurityProvider.IsUserInRole(userName, CoreConstant.Role.Administrator)
            };

            return response;
        }

        public BooleanResponse IsStudent(string userName)
        {
            var response = new BooleanResponse
            {
                Result = this.SecurityProvider.IsUserInRole(userName, CoreConstant.Role.Student)
            };

            return response;
        }

        public GetCurrentLoginInformationResponse GetCurrentLoginInformation()
        {
            var response = new GetCurrentLoginInformationResponse
            {
                IsAuthenticated = this.SecurityProvider.IsAuthenticated
            };

            if (this.SecurityProvider.IsAuthenticated)
            {
                response.UserId = this.SecurityProvider.CurrentUserId;
                response.UserName = this.SecurityProvider.CurrentUserName;

                foreach (var role in this.SecurityProvider.GetRoles(this.SecurityProvider.CurrentUserName))
                {
                    response.Roles.Add(role);
                }

                if (this.SecurityProvider.IsUserInRole(this.SecurityProvider.CurrentUserName, CoreConstant.Role.Student))
                {
                    var student = this._studentRepository.ReadByUserId(this.SecurityProvider.CurrentUserId);
                    response.EntityId = student.Id;
                }
            }

            return response;
        }

        #endregion

        #region Private Methods

        private bool IsValidLoginActivity(LoginRequest request)
        {
            var userProfile = this._userProfileRepository.Read(request.UserName);
            var maximumLogin = userProfile.MaximumLogin;

            if (maximumLogin == null)
            {
                return true;
            }

            var userActivities = this._userLoginActivityRepository.SearchByUserName(request.UserName);
            var userLoginActivityDtos = userActivities as UserLoginActivityDto[] ?? userActivities.ToArray();

            if (userLoginActivityDtos.Any())
            {
                // if new ip and reach maximum login quota
                if (userLoginActivityDtos.All(item => item.IpAddress != request.IpAddress) &&
                    userLoginActivityDtos.Length >= maximumLogin.Value)
                {
                    return false;
                }

                // if new ip, register new ip
                if (userLoginActivityDtos.All(item => item.IpAddress != request.IpAddress))
                {
                    this.CreateUserLoginActivity(request);
                }
                else
                {
                    // update last login on the same Ip Address
                    var currentLoginActivity = userLoginActivityDtos.Single(item => item.IpAddress == request.IpAddress);

                    currentLoginActivity.LastLoginDateTime = DateTime.Now;
                    this.PopulateAuditFieldsOnUpdate(currentLoginActivity);
                    this._userLoginActivityRepository.Update(currentLoginActivity);
                }
            }
            else
            {
                this.CreateUserLoginActivity(request);
            }

            return true;
        }

        private void CreateUserLoginActivity(LoginRequest request)
        {
            var userLoginActivityDto = new UserLoginActivityDto
            {
                UserName = request.UserName,
                IpAddress = request.IpAddress,
                FirstLoginDateTime = DateTime.UtcNow,
                LastLoginDateTime = DateTime.UtcNow
            };

            this.PopulateAuditFieldsOnCreate(userLoginActivityDto);
            this._userLoginActivityRepository.Insert(userLoginActivityDto);
        }

        #endregion
    }
}