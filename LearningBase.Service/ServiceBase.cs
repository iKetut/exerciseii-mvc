﻿using System;
using System.Collections.Generic;
using LearningBase.Dto;
using LearningBase.RepositoryContract;
using LearningBase.ServiceContract;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;

namespace LearningBase.Service
{
    public abstract class ServiceBase
    {
        #region Constructors

        protected ServiceBase(ISecurityProvider securityProvider)
        {
            this.SecurityProvider = securityProvider;
        }

        #endregion

        #region Properties

        protected ISecurityProvider SecurityProvider { get; }

        #endregion

        #region (protected) Methods

        protected GenericGetDtoCollectionResponse<TDto> GetAll<TDto>(IEntityBaseRepository<TDto> repository)
        {
            return this.GetAll(repository.GetAll);
        }

        protected GenericGetDtoCollectionResponse<TDto> GetAll<TDto>(Func<IEnumerable<TDto>> getAllFunction)
        {
            var response = new GenericGetDtoCollectionResponse<TDto>();

            var dtoCollection = getAllFunction();
            foreach (var dto in dtoCollection)
            {
                response.DtoCollection.Add(dto);
            }

            return response;
        }

        protected GenericReadDtoResponse<TDto> Read<TDto>(IEntityBaseRepository<TDto> repository, int entityId)
        {
            var response = new GenericReadDtoResponse<TDto>
            {
                Dto = repository.Read(entityId)
            };

            return response;
        }

        protected GenericPagedSearchResponse<TDto> PagedSearch<TDto>(IEntityBaseRepository<TDto> repository,
            PagedSearchRequest request)
        {
            var response = new GenericPagedSearchResponse<TDto>();

            var result = repository.PagedSearch(new PagedSearchParameter
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                OrderByFieldName = request.OrderByFieldName,
                SortOrder = request.SortOrder,
                Keyword = request.Keyword
            });

            foreach (var dto in result.Result)
            {
                response.DtoCollection.Add(dto);
            }

            response.TotalCount = result.Count;

            return response;
        }

        protected void PopulateAuditFieldsOnCreate(AuditableDto dto)
        {
            var currentUtcTime = DateTime.UtcNow;
            dto.CreatedBy = this.SecurityProvider.CurrentUserName;
            dto.CreatedDateTime = currentUtcTime;
            dto.LastModifiedBy = this.SecurityProvider.CurrentUserName;
            dto.LastModifiedDateTime = currentUtcTime;
        }

        protected void PopulateAuditFieldsOnUpdate(AuditableDto dto)
        {
            var currentUtcTime = DateTime.UtcNow;
            dto.LastModifiedBy = this.SecurityProvider.CurrentUserName;
            dto.LastModifiedDateTime = currentUtcTime;
        }

        #endregion
    }
}