﻿using System;

namespace LearningBase.Dto
{
    public class AuditableDto : EntityBaseDto
    {
        #region Properties

        public string CreatedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

        #endregion
    }
}