﻿using System;

namespace LearningBase.Dto.Utility
{
    public class UserLoginActivityDto : AuditableDto
    {
        #region Properties

        public string UserName { get; set; }

        public string IpAddress { get; set; }

        public DateTime FirstLoginDateTime { get; set; }

        public DateTime LastLoginDateTime { get; set; }

        #endregion
    }
}