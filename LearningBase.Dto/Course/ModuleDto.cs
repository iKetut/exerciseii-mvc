﻿namespace LearningBase.Dto.Course
{
    public class ModuleDto : AuditableDto
    {
        #region Properties

        public int CourseId { get; set; }

        public string Name { get; set; }

        public int SortingIndex { get; set; }

        #endregion
    }
}