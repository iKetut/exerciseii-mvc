﻿namespace LearningBase.Dto.Course
{
    public class CourseDto : AuditableDto
    {
        #region Properties

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        #endregion
    }
}