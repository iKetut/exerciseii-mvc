﻿namespace LearningBase.Dto.Common
{
    public class StudentDto : AuditableDto
    {
        #region Properties

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StudentName
        {
            get { return this.FirstName + " " + this.LastName; }
        }

        public string EmailAddress { get; set; }

        public string TimeZoneId { get; set; }

        public int CourseId { get; set; }

        public string CourseName { get; set; }

        #endregion
    }
}