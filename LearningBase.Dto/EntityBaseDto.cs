﻿namespace LearningBase.Dto
{
    public class EntityBaseDto
    {
        #region Properties

        public int Id { get; set; }

        #endregion
    }
}