//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LearningBase.DatabaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class com_Student
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string TimeZoneId { get; set; }
        public int CourseId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string LastModifiedBy { get; set; }
        public System.DateTime LastModifiedDateTime { get; set; }
    
        public virtual crs_Course crs_Course { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
