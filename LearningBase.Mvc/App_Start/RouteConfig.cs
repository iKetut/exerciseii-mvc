﻿using System.Web.Mvc;
using System.Web.Routing;

namespace LearningBase.Mvc
{
    public class RouteConfig
    {
        #region Public Methods

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                new[] {"LearningBase.Mvc.Controllers"}
                );
        }

        #endregion
    }
}