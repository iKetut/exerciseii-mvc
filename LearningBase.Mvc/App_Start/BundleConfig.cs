﻿using System.Web.Optimization;

namespace LearningBase.Mvc
{
    public class BundleConfig
    {
        #region Public Methods

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/sbadmin-js").Include(
                "~/Themes/sb-admin-2/bower_components/jquery/dist/jquery.js",
                "~/Themes/sb-admin-2/bower_components/bootstrap/dist/js/bootstrap.js",
                "~/Themes/sb-admin-2/bower_components/metisMenu/dist/metisMenu.js",
                "~/Themes/sb-admin-2/bower_components/datatables/media/js/jquery.dataTables.js",
                "~/Themes/sb-admin-2/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js",
                "~/Themes/sb-admin-2/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                "~/Themes/sb-admin-2/dist/js/sb-admin-2.js",
                "~/scripts/jquery.bootgrid.js",
                "~/scripts/bootstrap-dialog.js",
                "~/scripts/jquery.validate.js",
                "~/scripts/notify.js",
                "~/scripts/cropper.js",
                "~/scripts/cropper.min.js",
                "~/scripts/jquery-ui-1.12.1.js",
                "~/scripts/e2l-dialog.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin-js")
                .Include("~/scripts/application/application.js")
                .Include("~/scripts/application/helper.js")
                .IncludeDirectory("~/scripts/application/component", "*.js", true)
                .IncludeDirectory("~/scripts/application/utilities", "*.js", true)
                .IncludeDirectory("~/scripts/application/area/Administrator", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/outerpage-js")
                .Include("~/scripts/application/application.js")
                .IncludeDirectory("~/scripts/application/outerpage", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/e2l-form-js").Include("~/scripts/e2l-form.js"));
            bundles.Add(new ScriptBundle("~/bundles/gridster").Include("~/scripts/gridster/jquery.gridster.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include("~/scripts/jquery-ui-1.10.4.js"));

            bundles.Add(new StyleBundle("~/bundles/sbadmin-css").Include(
                "~/Themes/sb-admin-2/bower_components/bootstrap/dist/css/bootstrap.css",
                "~/Themes/sb-admin-2/bower_components/metisMenu/dist/metisMenu.css",
                "~/Themes/sb-admin-2/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css",
                "~/Themes/sb-admin-2/dist/css/sb-admin-2.css",
                "~/Themes/sb-admin-2/bower_components/font-awesome/css/font-awesome.css",
                "~/Themes/sb-admin-2/bower_components/bootstrap-datepicker/css/datepicker.min.css",
                "~/Themes/v1/core-notify.css",
                "~/Content/jquery.bootgrid.css",
                "~/Content/bootstrap-dialog.css",
                "~/Content/cropper.css",
                "~/Content/themes/base/jquery-ui.css",
                "~/Content/themes/base/sortable.css",
                "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/bundles/gridster-css").Include("~/scripts/gridster/jquery.gridster.css"));
            bundles.Add(new StyleBundle("~/bundles/gridster-css").Include("~/scripts/gridster/jquery.gridster.css"));
            bundles.Add(new StyleBundle("~/bundles/jquery-ui-css").Include("~/Content/jquery-ui.css"));
        }

        #endregion
    }
}