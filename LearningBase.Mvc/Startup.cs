﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LearningBase.Mvc.Startup))]
namespace LearningBase.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
