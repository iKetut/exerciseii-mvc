﻿using LearningBase.ServiceContract.Core;

namespace LearningBase.Mvc.Infrastructure
{
    public interface ISystemConfiguration
    {
        #region Properties

        int UserId { get; set; }

        string UserName { get; set; }

        int EntityId { get; set; }

        #endregion

        #region Public Methods

        void Refresh(IAccountService accountService);

        #endregion
    }
}