﻿using System.Configuration;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public static class VideoManager
    {
        #region Public Methods

        public static string GetCdnVideoUri(string videoCode)
        {
            var cdnVideoRootUri = ConfigurationManager.AppSettings["cdnVideoUrl"];
            return cdnVideoRootUri + videoCode + ".mp4";
        }

        #endregion
    }
}