﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using LearningBase.Core;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public static class HtmlHelperExtension
    {
        //#region Constants

        //private const string YoutubeEmbedUrlFormat = "https://www.youtube.com/embed/{0}";

        //private const string ModuleDivIdPattern = "module-{0}";
        //private const string AccordionDivIdPattern = "accordion-{0}";
        //private const string SubModuleDivIdPattern = "subModule-{0}";
        //private const string SubModulePrimaryDivIdPattern = "subModulePrimary-{0}";
        //private const string CardPanelDivIdPattern = "panel-{0}";
        //private const string CheckedModuleIconIdPattern = "checkedModuleIcon-{0}";
        //private const string CheckedGroupIconIdPattern = "checkedGroupIcon-{0}";
        //private const string GroupActivityIdPattern = "groupActivity-{0}";

        //private const string ActiveTabHeaderClass = "active";
        //private const string ActiveTabPanelClass = "tab-pane fade in active tab-padding";
        //private const string DefaultTabPanelClass = "tab-pane fade tab-padding";

        //private const string CardPanelSuccessClass = "panel-success panel-task";
        //private const string DefaultCardPanelClass = "panel-task";
        //private const string BracketRegexPrefix = @"\{([^}]*)\}";

        //#endregion

        //#region Public Methods

        //public static MvcHtmlString GenerateOrderParagraphInstruction(this HtmlHelper htmlHelper, string instruction)
        //{
        //    var instructionMatches = Regex.Matches(instruction, BracketRegexPrefix);
        //    if (instructionMatches.Count == 0)
        //    {
        //        return MvcHtmlString.Create(instruction);
        //    }

        //    var result = instruction;
        //    foreach (var instructionMatch in instructionMatches)
        //    {
        //        var stringSections = instructionMatch.ToString()
        //            .TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);
        //        if (stringSections[0].Contains("Video"))
        //        {
        //            // generate wistia video key
        //            var wistiaVideoKey = stringSections[1];
        //            if (string.IsNullOrEmpty(wistiaVideoKey))
        //            {
        //                var errorHeadingTag = new TagBuilder("h3");
        //                errorHeadingTag.InnerHtml += "Current video could not be found.";
        //                result = result.Replace(instructionMatch.ToString(), errorHeadingTag.ToString());
        //                continue;
        //            }

        //            var scriptWistiaEmbedTag = new TagBuilder("script");
        //            scriptWistiaEmbedTag.Attributes.Add("async", null);
        //            scriptWistiaEmbedTag.Attributes.Add("src",
        //                string.Format(CoreConstant.WistiaCodeConstant.JavascriptPattern, wistiaVideoKey));

        //            var scriptWistiaCoreTag = new TagBuilder("script");
        //            scriptWistiaCoreTag.Attributes.Add("async", null);
        //            scriptWistiaCoreTag.Attributes.Add("src", CoreConstant.WistiaCodeConstant.JavascriptCoreSource);

        //            var wistiaVideoEmbbedDiv = new TagBuilder("div");
        //            wistiaVideoEmbbedDiv.Attributes.Add("style", "height:100%;width:100%");
        //            wistiaVideoEmbbedDiv.Attributes.Add("class",
        //                string.Format(CoreConstant.WistiaCodeConstant.SectionClassPattern, wistiaVideoKey));

        //            var wistiaWrapperDiv = new TagBuilder("div");
        //            wistiaWrapperDiv.Attributes.Add("style", "height:100%;left:0;position:absolute;top:0;width:100%;");
        //            wistiaWrapperDiv.Attributes.Add("class", "wistia_responsive_wrapper");
        //            wistiaWrapperDiv.InnerHtml += wistiaVideoEmbbedDiv.ToString();

        //            var wistiaPaddingDiv = new TagBuilder("div");
        //            wistiaPaddingDiv.Attributes.Add("style", "padding:56.25% 0 0 0;position:relative;");
        //            wistiaPaddingDiv.Attributes.Add("class", "wistia_responsive_padding");
        //            wistiaPaddingDiv.InnerHtml += wistiaWrapperDiv.ToString();

        //            var videoDiv = new TagBuilder("div");
        //            videoDiv.InnerHtml += scriptWistiaEmbedTag.ToString();
        //            videoDiv.InnerHtml += scriptWistiaCoreTag.ToString();
        //            videoDiv.InnerHtml += wistiaPaddingDiv.ToString();

        //            result = result.Replace(instructionMatch.ToString(), videoDiv.ToString());
        //            continue;
        //        }


        //        result = result.Replace(instructionMatch.ToString(), string.Empty);
        //    }

        //    var outerDiv = new TagBuilder("div");
        //    outerDiv.Attributes.Add("class", "zero-left-right");
        //    outerDiv.InnerHtml += result;

        //    return MvcHtmlString.Create(outerDiv.ToString());
        //}

        //#endregion

        //#region (public) Methods

        //public static MvcHtmlString BtTextEditorWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
        //    Expression<Func<TModel, TProperty>> expression,
        //    UiControlEnum.ControlWidthOption widthOption,
        //    bool isReadOnly)
        //{
        //    var name = ExpressionHelper.GetExpressionText(expression);
        //    var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    return BtTextEditorWithLabel(htmlHelper,
        //        name,
        //        "text",
        //        metadata.DisplayName,
        //        metadata.Model?.ToString() ?? string.Empty,
        //        metadata.IsRequired,
        //        isReadOnly,
        //        widthOption: widthOption);
        //}

        // ReSharper disable once UnusedParameter.Global
        public static MvcHtmlString BtTextEditorWithLabel(this HtmlHelper htmlHelper,
            string name,
            string type,
            string displayName,
            string value,
            bool isRequired,
            bool isReadOnly,
            bool isMultiLine = false,
            UiControlEnum.ControlWidthOption widthOption = UiControlEnum.ControlWidthOption.Medium,
            string cssClass = "",
            bool isWithDesription = false,
            bool isWithDoubleDescription = false,
            string descriptionText = "",
            string secondDescriptionText = "",
            int maxlength = 0)
        {
            // Create the outer div
            var formGroupDivTag = GetFormGroupDivTag();

            // And then the label
            var labelTag = GetLabelTag(name, displayName, isRequired);
            formGroupDivTag.InnerHtml += labelTag;

            // Then the main control itself
            var mainControlTag = isMultiLine ? new TagBuilder("textarea") : new TagBuilder("input");

            mainControlTag.Attributes.Add("id", name);
            mainControlTag.Attributes.Add("name", name);

            if (!string.IsNullOrEmpty(cssClass))
            {
                mainControlTag.Attributes.Add("class", cssClass);
            }

            if (isMultiLine)
            {
                mainControlTag.Attributes.Add("rows", "5");
            }

            if (isMultiLine)
            {
                mainControlTag.InnerHtml += value;
            }
            else
            {
                mainControlTag.Attributes.Add("value", value);
                mainControlTag.Attributes.Add("data-slider-value", value);
            }

            mainControlTag.AddCssClass("form-control");

            if (isRequired)
            {
                mainControlTag.Attributes.Add("required", "required");
                mainControlTag.Attributes.Add("placeholder", "This field is required.");
            }

            if (!isMultiLine)
            {
                mainControlTag.Attributes.Add("type", type);
            }

            if (isReadOnly)
            {
                mainControlTag.Attributes.Add("readonly", "readonly");
            }

            if (maxlength > 0)
            {
                mainControlTag.Attributes.Add("maxlength", maxlength.ToString(CultureInfo.InvariantCulture));
            }

            // Inner div to hold the main control
            var mainControlDivTag = GetMainControlDivTag(widthOption);
            mainControlDivTag.InnerHtml += mainControlTag;
            formGroupDivTag.InnerHtml += mainControlDivTag;

            if (isWithDesription)
            {
                var descriptionLabelTag = new TagBuilder("label");
                descriptionLabelTag.AddCssClass("col-sm-2 control-label description-label");
                descriptionLabelTag.Attributes.Add("for", name + "Description");
                descriptionLabelTag.Attributes.Add("Id", name + "Description");

                if (!string.IsNullOrEmpty(descriptionText))
                {
                    descriptionLabelTag.InnerHtml = descriptionText;
                }

                formGroupDivTag.InnerHtml += descriptionLabelTag;
            }

            if (isWithDoubleDescription)
            {
                var descriptionLabelTag = new TagBuilder("label");
                descriptionLabelTag.AddCssClass("col-sm-1 control-label description-label");
                descriptionLabelTag.Attributes.Add("for", name + "FirstDescription");
                descriptionLabelTag.Attributes.Add("Id", name + "FirstDescription");

                if (!string.IsNullOrEmpty(descriptionText))
                {
                    descriptionLabelTag.InnerHtml = descriptionText;
                }

                formGroupDivTag.InnerHtml += descriptionLabelTag;

                var secondDescriptionLabelTag = new TagBuilder("label");
                secondDescriptionLabelTag.AddCssClass("col-sm-2 control-label description-label");
                secondDescriptionLabelTag.Attributes.Add("for", name + "SecondDescription");
                secondDescriptionLabelTag.Attributes.Add("Id", name + "SecondDescription");

                if (!string.IsNullOrEmpty(secondDescriptionText))
                {
                    secondDescriptionLabelTag.InnerHtml = secondDescriptionText;
                }

                formGroupDivTag.InnerHtml += secondDescriptionLabelTag;
            }

            return MvcHtmlString.Create(formGroupDivTag.ToString());
        }

        public static MvcHtmlString BtTextEditorWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return BtTextEditorWithLabel(htmlHelper,
                name,
                "text",
                metadata.DisplayName,
                metadata.Model?.ToString() ?? string.Empty,
                metadata.IsRequired,
                metadata.IsReadOnly);
        }

        public static MvcHtmlString BtMultiLineTextEditorWithLabel<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return BtTextEditorWithLabel(htmlHelper,
                name,
                "text",
                metadata.DisplayName,
                metadata.Model as string,
                metadata.IsRequired,
                metadata.IsReadOnly,
                true);
        }

        public static MvcHtmlString BtPasswordEditorWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return BtTextEditorWithLabel(htmlHelper,
                name,
                "password",
                metadata.DisplayName,
                metadata.Model as string,
                metadata.IsRequired,
                metadata.IsReadOnly);
        }

        public static MvcHtmlString BtEmailEditorWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return BtTextEditorWithLabel(htmlHelper,
                name,
                "email",
                metadata.DisplayName,
                metadata.Model as string,
                metadata.IsRequired,
                metadata.IsReadOnly);
        }

        //private static MvcHtmlString BtDropDownWithLabelAndButton(string name,
        //    string displayName,
        //    SelectList items,
        //    object value,
        //    bool isRequired,
        //    bool isReadOnly,
        //    MaintenanceButtonRow maintenanceButtonRow,
        //    string placeholderText = "")
        //{
        //    // Create the outer div
        //    var formGroupDivTag = GetFormGroupDivTag();

        //    // And then the label
        //    var labelTag = GetLabelTag(name, displayName, isRequired);
        //    formGroupDivTag.InnerHtml += labelTag;

        //    // The select tag
        //    var selectTag = new TagBuilder("select");
        //    selectTag.Attributes.Add("id", name);
        //    selectTag.Attributes.Add("name", name);
        //    selectTag.AddCssClass("form-control");

        //    if (isRequired)
        //    {
        //        selectTag.Attributes.Add("required", "required");
        //    }

        //    if (isReadOnly)
        //    {
        //        selectTag.Attributes.Add("disabled", "disabled");
        //    }

        //    if (!string.IsNullOrEmpty(placeholderText))
        //    {
        //        selectTag.Attributes.Add("placeholder", placeholderText);
        //    }

        //    // Create the default option tag
        //    var defaultOptionTag = new TagBuilder("option");
        //    defaultOptionTag.Attributes.Add("value", "");
        //    defaultOptionTag.InnerHtml += placeholderText;
        //    selectTag.InnerHtml += defaultOptionTag;

        //    // The option tags
        //    foreach (var item in items)
        //    {
        //        var optionTag = new TagBuilder("option");
        //        optionTag.Attributes.Add("value", item.Value);

        //        if (value != null)
        //        {
        //            if (item.Value == value.ToString())
        //            {
        //                optionTag.Attributes.Add("selected", "selected");
        //            }
        //        }

        //        optionTag.InnerHtml += item.Text;
        //        selectTag.InnerHtml += optionTag;
        //    }

        //    // Inner div to hold the main control
        //    var mainControlDivTag = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Short);
        //    mainControlDivTag.InnerHtml += selectTag;
        //    formGroupDivTag.InnerHtml += mainControlDivTag;

        //    var buttonControlDivTag = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Short);

        //    // Create button control group
        //    foreach (var genericButton in maintenanceButtonRow.Items)
        //    {
        //        var buttonTag = new TagBuilder("button");
        //        buttonTag.Attributes.Add("type", genericButton.Type);
        //        buttonTag.AddCssClass(genericButton.IsPrimary ? "btn btn-primary" : "btn btn-default");
        //        buttonTag.Attributes.Add("id", genericButton.Id);
        //        buttonTag.InnerHtml += genericButton.Label;

        //        buttonControlDivTag.InnerHtml += buttonTag;
        //    }

        //    // Inner div to hold the main control
        //    formGroupDivTag.InnerHtml += buttonControlDivTag;

        //    return MvcHtmlString.Create(formGroupDivTag.ToString());
        //}

        private static MvcHtmlString BtDropDownWithLabel(string name,
            string displayName,
            SelectList items,
            object value,
            bool isRequired,
            bool isReadOnly,
            string placeholderText = "",
            bool isShowDivIdSection = false)
        {
            // Create the outer div
            var formGroupDivTag = GetFormGroupDivTag();

            if (isShowDivIdSection)
            {
                formGroupDivTag.Attributes.Add("id", name + "Section");
            }

            // And then the label
            var labelTag = GetLabelTag(name, displayName, isRequired);
            formGroupDivTag.InnerHtml += labelTag;

            // The select tag
            var selectTag = new TagBuilder("select");
            selectTag.Attributes.Add("id", name);
            selectTag.Attributes.Add("name", name);
            selectTag.Attributes.Add("class", "form-control");

            if (isRequired)
            {
                selectTag.Attributes.Add("required", "required");
            }

            if (isReadOnly)
            {
                selectTag.Attributes.Add("disabled", "disabled");
            }

            if (!string.IsNullOrEmpty(placeholderText))
            {
                selectTag.Attributes.Add("placeholder", placeholderText);
            }

            // Create the default option tag
            var defaultOptionTag = new TagBuilder("option");
            defaultOptionTag.Attributes.Add("value", "");
            defaultOptionTag.InnerHtml += placeholderText;
            selectTag.InnerHtml += defaultOptionTag;

            // The option tags
            foreach (var item in items)
            {
                var optionTag = new TagBuilder("option");
                optionTag.Attributes.Add("value", item.Value);

                if (value != null)
                {
                    if (item.Value == value.ToString())
                    {
                        optionTag.Attributes.Add("selected", "selected");
                    }
                }

                optionTag.InnerHtml += item.Text;
                selectTag.InnerHtml += optionTag;
            }

            // Control group div to hold the main control
            var controlGroupTag = new TagBuilder("div");
            controlGroupTag.AddCssClass("control-group");
            controlGroupTag.InnerHtml += selectTag;

            // Inner div to hold the main control
            var mainControlDivTag = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Short);
            mainControlDivTag.InnerHtml += controlGroupTag;
            formGroupDivTag.InnerHtml += mainControlDivTag;

            return MvcHtmlString.Create(formGroupDivTag.ToString());
        }

        public static MvcHtmlString BtDropDownWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            SelectList items,
            string placeholderText = "",
            string displayName = "")
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            displayName = string.IsNullOrEmpty(displayName) ? metadata.DisplayName : displayName;
            return BtDropDownWithLabel(name,
                displayName,
                items,
                metadata.Model,
                metadata.IsRequired,
                metadata.IsReadOnly,
                placeholderText);
        }

        // ReSharper disable once UnusedParameter.Global
        public static MvcHtmlString BtMaintenanceFormErrorPane(this HtmlHelper htmlHelper)
        {
            var divTag = new TagBuilder("div");
            divTag.Attributes.Add("id", "errorPane");
            divTag.Attributes.Add("class", "alert alert-danger");
            divTag.Attributes.Add("style", "display: none;");
            return MvcHtmlString.Create(divTag.ToString());
        }

        //public static MvcHtmlString BtDropDownWithLabelAndButton<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
        //    Expression<Func<TModel, TProperty>> expression,
        //    SelectList items,
        //    MaintenanceButtonRow maintenanceButtonRow,
        //    string placeholderText = "",
        //    string displayName = "")
        //{
        //    var name = ExpressionHelper.GetExpressionText(expression);
        //    var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    displayName = string.IsNullOrEmpty(displayName) ? metadata.DisplayName : displayName;
        //    return BtDropDownWithLabelAndButton(name,
        //        displayName,
        //        items,
        //        metadata.Model,
        //        metadata.IsRequired,
        //        metadata.IsReadOnly,
        //        maintenanceButtonRow,
        //        placeholderText);
        //}

        //private static MvcHtmlString BtRadioButtonListWithLabel(string name,
        //    string displayName,
        //    RadioButtonList radioButtonList,
        //    object value,
        //    bool isReadOnly)
        //{
        //    // Create the outer div
        //    var formGroupDivTag = GetFormGroupDivTag();

        //    // And then the label
        //    var labelTag = GetLabelTag(name, displayName, false);
        //    formGroupDivTag.InnerHtml += labelTag;

        //    // Div control to hold the radio list
        //    var radioDivTag = new TagBuilder("div");
        //    radioDivTag.AddCssClass("col-md-6");

        //    var index = 0;
        //    foreach (var radioItem in radioButtonList.Items)
        //    {
        //        var controlId = $"rb{name}{index++}";

        //        var labelControl = new TagBuilder("label");
        //        labelControl.Attributes.Add("for", controlId);

        //        var radioControlTag = new TagBuilder("input");
        //        radioControlTag.Attributes.Add("id", controlId);
        //        radioControlTag.Attributes.Add("type", "radio");
        //        radioControlTag.Attributes.Add("name", name);
        //        radioControlTag.Attributes.Add("value", radioItem.Value);

        //        if (value == null)
        //        {
        //            if (radioItem.IsDefault)
        //            {
        //                radioControlTag.Attributes.Add("checked", "checked");
        //            }
        //        }
        //        else if (radioItem.Value == value.ToString())
        //        {
        //            radioControlTag.Attributes.Add("checked", "checked");
        //        }

        //        if (isReadOnly)
        //        {
        //            radioControlTag.Attributes.Add("disabled", "disabled");
        //        }

        //        if (radioButtonList.Layout == UiControlEnum.ControlLayoutType.Vertical)
        //        {
        //            labelControl.AddCssClass("radio-inline");
        //            labelControl.InnerHtml += radioControlTag;
        //            labelControl.InnerHtml += radioItem.Label;
        //        }
        //        else
        //        {
        //            labelControl.InnerHtml += radioControlTag;
        //            labelControl.InnerHtml += radioItem.Label;
        //        }

        //        radioDivTag.InnerHtml += labelControl;
        //    }

        //    formGroupDivTag.InnerHtml += radioDivTag;

        //    return MvcHtmlString.Create(formGroupDivTag.ToString());
        //}

        //public static MvcHtmlString BtRadioButtonListWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
        //    Expression<Func<TModel, TProperty>> expression,
        //    RadioButtonList radioButtonList)
        //{
        //    var name = ExpressionHelper.GetExpressionText(expression);
        //    var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    return BtRadioButtonListWithLabel(name,
        //        metadata.DisplayName,
        //        radioButtonList,
        //        metadata.Model,
        //        metadata.IsReadOnly);
        //}

        private static MvcHtmlString BtMaintenanceButtonRowWithLabel(MaintenanceButtonRow maintenanceButtonRow)
        {
            // Create the outer div
            var formGroupDivTag = GetFormGroupDivTag();
            formGroupDivTag.AddCssClass("maintenanceButtonRow");

            var offsetDiv = new TagBuilder("div");
            offsetDiv.AddCssClass("col-sm-offset-2 col-sm-10");

            var controlGroupDiv = new TagBuilder("div");
            controlGroupDiv.AddCssClass("control-group");

            foreach (var genericButton in maintenanceButtonRow.Items)
            {
                var buttonTag = new TagBuilder("button");
                buttonTag.Attributes.Add("type", genericButton.Type);
                buttonTag.AddCssClass(genericButton.IsPrimary ? "btn btn-primary" : "btn btn-default");
                buttonTag.Attributes.Add("id", genericButton.Id);
                buttonTag.InnerHtml += genericButton.Label;

                controlGroupDiv.InnerHtml += buttonTag;
            }

            offsetDiv.InnerHtml += controlGroupDiv;
            formGroupDivTag.InnerHtml += offsetDiv;

            return MvcHtmlString.Create(formGroupDivTag.ToString());
        }

        public static MvcHtmlString BtMaintenanceButtonRowWithLabel<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return BtMaintenanceButtonRowWithLabel(metadata.Model as MaintenanceButtonRow);
        }

        //public static MvcHtmlString BtDatePickerWithLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
        //    Expression<Func<TModel, TProperty>> expression)
        //{
        //    var name = ExpressionHelper.GetExpressionText(expression);
        //    var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    return BtDatePickerWithLabel(htmlHelper,
        //        name,
        //        metadata.DisplayName,
        //        string.Format("{0:" + CoreConstant.Setting.DefaultShortDateFormat + "}", metadata.Model),
        //        metadata.IsRequired);
        //}

        //public static MvcHtmlString BtDatePickerWithLabel(this HtmlHelper htmlHelper,
        //    string name,
        //    string displayName,
        //    string value,
        //    bool isRequired)
        //{
        //    // Create the outer div
        //    var formGroupDivTag = GetFormGroupDivTag();

        //    // And then the label
        //    var labelTag = GetLabelTag(name, displayName, isRequired);
        //    formGroupDivTag.InnerHtml += labelTag;

        //    // Then the main control itself
        //    var mainControlTag = new TagBuilder("input");

        //    mainControlTag.Attributes.Add("id", name);
        //    mainControlTag.Attributes.Add("name", name);
        //    mainControlTag.Attributes.Add("value", value);

        //    mainControlTag.AddCssClass("form-control datepicker");

        //    if (isRequired)
        //    {
        //        mainControlTag.Attributes.Add("required", "required");
        //        mainControlTag.Attributes.Add("placeholder", "This field is required.");
        //    }

        //    mainControlTag.Attributes.Add("type", "text");
        //    mainControlTag.Attributes.Add("readonly", "readonly");
        //    mainControlTag.Attributes.Add("data-date-format", CoreConstant.Setting.DefaultShortDatePickerFormat);

        //    // Inner div to hold the main control
        //    var mainControlDivTag = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Short);
        //    mainControlDivTag.InnerHtml += mainControlTag;

        //    formGroupDivTag.InnerHtml += mainControlDivTag;

        //    return MvcHtmlString.Create(formGroupDivTag.ToString());
        //}

        //#endregion

        //#region (public) E2L Widget Methods

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2Image(this HtmlHelper htmlHelper, string pictureName)
        //{
        //    var imgTag = new TagBuilder("img");

        //    imgTag.Attributes.Add("class", "img-responsive center-block");
        //    imgTag.Attributes.Add("src", ConfigurationManager.AppSettings["cdnImageUrl"] + pictureName);

        //    return MvcHtmlString.Create(imgTag.ToString());
        //}

        //public static MvcHtmlString E2VideoPlayer(this HtmlHelper htmlHelper, string videoType, string videoUrl)
        //{
        //    if (videoType == CoreConstant.PreparationActivityTypeCode.AzureVideo)
        //    {
        //        return E2WistiaVideoPlayer(htmlHelper, videoUrl);
        //    }

        //    if (videoType == CoreConstant.PreparationActivityTypeCode.YoutubeVideo)
        //    {
        //        return E2YoutubePlayer(videoUrl);
        //    }

        //    return MvcHtmlString.Create(string.Empty);
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2WistiaVideoPlayer(this HtmlHelper htmlHelper, string videoUrl)
        //{
        //    var scriptWistiaEmbedTag = new TagBuilder("script");
        //    scriptWistiaEmbedTag.Attributes.Add("async", null);
        //    scriptWistiaEmbedTag.Attributes.Add("src",
        //        string.Format(CoreConstant.WistiaCodeConstant.JavascriptPattern, videoUrl));

        //    var scriptWistiaCoreTag = new TagBuilder("script");
        //    scriptWistiaCoreTag.Attributes.Add("async", null);
        //    scriptWistiaCoreTag.Attributes.Add("src", CoreConstant.WistiaCodeConstant.JavascriptCoreSource);

        //    var wistiaVideoEmbbedDiv = new TagBuilder("div");
        //    wistiaVideoEmbbedDiv.Attributes.Add("style", "height:100%;width:100%");
        //    wistiaVideoEmbbedDiv.Attributes.Add("class",
        //        string.Format(CoreConstant.WistiaCodeConstant.SectionClassPattern, videoUrl));

        //    var wistiaWrapperDiv = new TagBuilder("div");
        //    wistiaWrapperDiv.Attributes.Add("style", "height:100%;left:0;position:absolute;top:0;width:100%;");
        //    wistiaWrapperDiv.Attributes.Add("class", "wistia_responsive_wrapper");
        //    wistiaWrapperDiv.InnerHtml += wistiaVideoEmbbedDiv.ToString();

        //    var wistiaPaddingDiv = new TagBuilder("div");
        //    wistiaPaddingDiv.Attributes.Add("style", "padding:59.5% 0 0 0;position:relative;");
        //    wistiaPaddingDiv.Attributes.Add("class", "wistia_responsive_padding");
        //    wistiaPaddingDiv.InnerHtml += wistiaWrapperDiv.ToString();

        //    var videoDiv = new TagBuilder("div");
        //    videoDiv.InnerHtml += scriptWistiaEmbedTag.ToString();
        //    videoDiv.InnerHtml += scriptWistiaCoreTag.ToString();
        //    videoDiv.InnerHtml += wistiaPaddingDiv.ToString();

        //    if (string.IsNullOrEmpty(videoUrl))
        //    {
        //        videoDiv = new TagBuilder("div");
        //        videoDiv.InnerHtml += Resource.Video_VideoNotExistMessage;
        //    }

        //    return MvcHtmlString.Create(videoDiv.ToString());
        //}


        //public static MvcHtmlString E2MultipleRecorderWidget(this HtmlHelper htmlHelper,
        //    string content,
        //    ICollection<AudioAnswerItem> audioCollection)
        //{
        //    if (string.IsNullOrEmpty(content))
        //    {
        //        return MvcHtmlString.Empty;
        //    }

        //    var result = content;
        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);
        //    foreach (var regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString()
        //            .TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);
        //        if (stringOperation[0].Contains("Audio"))
        //        {
        //            var audioUri = AudioManager.GetCdnAudioUri(stringOperation[1]);

        //            var audioPlayerTag = new TagBuilder("audio");
        //            audioPlayerTag.Attributes.Add("controls", "controls");
        //            audioPlayerTag.Attributes.Add("src", audioUri);
        //            audioPlayerTag.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //            var divTag = new TagBuilder("div");
        //            divTag.Attributes.Add("id", "audioPlayerControl");
        //            divTag.InnerHtml += audioPlayerTag;


        //            result = result.Replace(regexString.ToString(), divTag.ToString());
        //            continue;
        //        }

        //        if (stringOperation[0].Contains("ModelAnswer"))
        //        {
        //            var divTag = GetModelAnswerHtml(stringOperation[1], stringOperation[2]);
        //            result = result.Replace(regexString.ToString(), divTag);
        //            continue;
        //        }

        //        if (stringOperation[0].Contains("Transcript"))
        //        {
        //            var divTag = GetModelAnswerHtml("Text", stringOperation[1]);
        //            result = result.Replace(regexString.ToString(), divTag);
        //            continue;
        //        }

        //        if (stringOperation[0].Contains("IMG"))
        //        {
        //            var imageUri = ConfigurationManager.AppSettings["cdnImageUrl"] + stringOperation[1];

        //            var imageTag = new TagBuilder("img");
        //            imageTag.Attributes.Add("class", "img-responsive center-block");
        //            imageTag.Attributes.Add("src", imageUri);

        //            var divTag = new TagBuilder("div");
        //            divTag.Attributes.Add("class", "zero-left-right");
        //            divTag.InnerHtml += imageTag;

        //            result = result.Replace(regexString.ToString(), divTag.ToString());
        //            continue;
        //        }

        //        var panelTag = new TagBuilder("div");
        //        panelTag.Attributes.Add("class", "panel panel-default");

        //        var titleTag = new TagBuilder("h3");
        //        titleTag.Attributes.Add("class", "panel-title");
        //        titleTag.InnerHtml += Resource.Speaking_Recorder;

        //        var panelHeadingTag = new TagBuilder("div");
        //        panelHeadingTag.Attributes.Add("class", "panel-heading");
        //        panelHeadingTag.InnerHtml += titleTag;
        //        panelTag.InnerHtml += panelHeadingTag;

        //        var recordingData = audioCollection.SingleOrDefault(item => item.SectionName == stringOperation[1]);
        //        var speakingWidget = recordingData != null
        //            ? GenerateSpeakingWidget(htmlHelper,
        //                recordingData.SectionName,
        //                recordingData.AudioUrl,
        //                recordingData.AudioName)
        //            : GenerateSpeakingWidget(htmlHelper, stringOperation[1], string.Empty, string.Empty);

        //        var panelBodyTag = new TagBuilder("div");
        //        panelBodyTag.Attributes.Add("class", "panel-body");

        //        panelBodyTag.InnerHtml += speakingWidget;
        //        panelTag.InnerHtml += panelBodyTag;

        //        result = result.Replace(regexString.ToString(), panelTag.ToString());
        //    }

        //    return MvcHtmlString.Create(result);
        //}

        //public static string GetModelAnswerHtml(string type, string source)
        //{
        //    var panelTag = new TagBuilder("div");
        //    panelTag.Attributes.Add("class", "panel panel-default");

        //    var newIdPanel = Guid.NewGuid();

        //    var panelBodyTag = new TagBuilder("div");
        //    panelBodyTag.Attributes.Add("class", "panel-body");

        //    var collapsedLinkTag = new TagBuilder("a");
        //    collapsedLinkTag.Attributes.Add("role", "button");
        //    collapsedLinkTag.Attributes.Add("data-toggle", "collapse");
        //    collapsedLinkTag.Attributes.Add("href", "#" + newIdPanel);

        //    // generate content for body html
        //    if (type.Contains("Audio"))
        //    {
        //        var audioUri = AudioManager.GetCdnAudioUri(source);

        //        var audioTag = new TagBuilder("audio");
        //        audioTag.Attributes.Add("controls", "controls");
        //        audioTag.Attributes.Add("src", audioUri);
        //        audioTag.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //        panelBodyTag.InnerHtml += audioTag;
        //        collapsedLinkTag.InnerHtml += Resource.Speaking_ModelAnswerAudio;
        //    }

        //    if (type.Contains("Text"))
        //    {
        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.InnerHtml += source;
        //        panelBodyTag.InnerHtml += paragraphTag;
        //        collapsedLinkTag.InnerHtml += Resource.Speaking_ModelAnswerTranscript;
        //    }

        //    var titleTag = new TagBuilder("h3");
        //    titleTag.Attributes.Add("class", "panel-title");
        //    titleTag.InnerHtml += collapsedLinkTag;

        //    var panelHeadingTag = new TagBuilder("div");
        //    panelHeadingTag.Attributes.Add("class", "panel-heading");
        //    panelHeadingTag.Attributes.Add("role", "tab");
        //    panelHeadingTag.InnerHtml += titleTag;
        //    panelTag.InnerHtml += panelHeadingTag;

        //    var collapsePanelTag = new TagBuilder("div");
        //    collapsePanelTag.Attributes.Add("class", "panel-collapse collapse");
        //    collapsePanelTag.Attributes.Add("role", "tabpanel");
        //    collapsePanelTag.Attributes.Add("id", newIdPanel.ToString());
        //    collapsePanelTag.InnerHtml += panelBodyTag;

        //    panelTag.InnerHtml += collapsePanelTag;

        //    return panelTag.ToString();
        //}

        //public static MvcHtmlString E2RecorderWidget(this HtmlHelper htmlHelper,
        //    string additionalCssClass,
        //    string sectionName = "")
        //{
        //    var recordIcon = new TagBuilder("i");
        //    recordIcon.AddCssClass("fa fa-microphone fa-fw");

        //    var recordButton = new TagBuilder("button");
        //    recordButton.Attributes.Add("id", "recordButton");
        //    recordButton.AddCssClass("btn btn-danger");
        //    recordButton.AddCssClass("multipleRecorderButton");
        //    if (!string.IsNullOrEmpty(sectionName))
        //    {
        //        recordButton.Attributes.Add("data-section", sectionName);
        //    }

        //    recordButton.InnerHtml += recordIcon;
        //    recordButton.InnerHtml += Resource.General_Record;

        //    var loadingIcon = new TagBuilder("i");
        //    loadingIcon.AddCssClass("fa fa-spinner fa-pulse fa-2x fa-fw");
        //    loadingIcon.Attributes.Add("id",
        //        string.IsNullOrEmpty(sectionName) ? "spinnerImage" : "spinnerImage-" + sectionName);
        //    loadingIcon.Attributes.Add("style", "display: none");

        //    var recordingLog = new TagBuilder("div");
        //    recordingLog.AddCssClass("col-xs-6 pull-right text-right");
        //    recordingLog.Attributes.Add("id",
        //        string.IsNullOrEmpty(sectionName) ? "recordingLog" : "recordingLog-" + sectionName);

        //    var recordingPanel = new TagBuilder("div");
        //    recordingPanel.AddCssClass("margin-bottom-20 zero-left-right " + additionalCssClass);
        //    recordingPanel.Attributes.Add("id", "recorderControlPanel");
        //    recordingPanel.InnerHtml += recordButton;
        //    recordingPanel.InnerHtml += loadingIcon;
        //    recordingPanel.InnerHtml += recordingLog;

        //    return MvcHtmlString.Create(recordingPanel.ToString());
        //}

        //public static MvcHtmlString E2UploadAudioWidget(this HtmlHelper htmlHelper,
        //    string additionalCssClass,
        //    string sectionName = "")
        //{
        //    var informationPanel = new TagBuilder("p");
        //    informationPanel.AddCssClass("help-block");
        //    informationPanel.InnerHtml += Resource.General_RecorderNotSupportedMessage;

        //    var fileTag = new TagBuilder("input");
        //    fileTag.Attributes.Add("id",
        //        string.IsNullOrEmpty(sectionName) ? "FileAudioRecorder" : "FileAudioRecorder-" + sectionName);
        //    fileTag.Attributes.Add("type", "file");
        //    fileTag.Attributes.Add("name",
        //        string.IsNullOrEmpty(sectionName) ? "FileAudioRecorder" : "FileAudioRecorder-" + sectionName);
        //    fileTag.Attributes.Add("accept", "audio/mpeg");

        //    var filePanel = new TagBuilder("div");
        //    filePanel.AddCssClass("form-group");
        //    filePanel.InnerHtml += fileTag;

        //    var uploadButton = new TagBuilder("button");
        //    uploadButton.AddCssClass("btn btn-primary");
        //    uploadButton.Attributes.Add("id", "uploadAudioButton");
        //    uploadButton.AddCssClass("multipleUploadButton");
        //    uploadButton.InnerHtml += Resource.General_Upload;
        //    if (!string.IsNullOrEmpty(sectionName))
        //    {
        //        uploadButton.Attributes.Add("data-section", sectionName);
        //    }

        //    var formPanel = new TagBuilder("form");
        //    formPanel.AddCssClass("form-inline");
        //    formPanel.InnerHtml += filePanel;
        //    formPanel.InnerHtml += uploadButton;

        //    var mainPanel = new TagBuilder("div");
        //    mainPanel.AddCssClass("margin-bottom-20 zero-left-right " + additionalCssClass);
        //    mainPanel.Attributes.Add("id", "uploadControlPanel");
        //    mainPanel.Attributes.Add("style", "display: none");
        //    mainPanel.InnerHtml += informationPanel;
        //    mainPanel.InnerHtml += formPanel;

        //    return MvcHtmlString.Create(mainPanel.ToString());
        //}

        //public static MvcHtmlString E2AudioPanel(this HtmlHelper htmlHelper,
        //    string audioUri,
        //    string audioName,
        //    string additionalCssClass)
        //{
        //    var audioPanel = new TagBuilder("audio");
        //    audioPanel.Attributes.Add("src", audioUri);
        //    audioPanel.Attributes.Add("type", "audio/mpeg");
        //    audioPanel.Attributes.Add("controls", "controls");
        //    audioPanel.Attributes.Add("id", "audio-player");
        //    audioPanel.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //    var audioUriLink = new TagBuilder("a");
        //    audioUriLink.Attributes.Add("id", "record-url");
        //    audioUriLink.Attributes.Add("href", audioUri);
        //    audioUriLink.InnerHtml += audioName;

        //    var audioUriPanel = new TagBuilder("div");
        //    audioUriPanel.InnerHtml += audioUriLink;

        //    var mainPanel = new TagBuilder("div");
        //    mainPanel.AddCssClass(additionalCssClass);
        //    mainPanel.Attributes.Add("id", "record-result");
        //    mainPanel.InnerHtml += audioPanel;
        //    mainPanel.InnerHtml += audioUriPanel;

        //    return MvcHtmlString.Create(mainPanel.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2MultipleChoiceSingleAnswerQuestion(this HtmlHelper htmlHelper,
        //    IEnumerable<AnswerDropDownItem> answerList,
        //    string questionText)
        //{
        //    var answerDropDownList = new List<object>();
        //    foreach (var answerItem in answerList)
        //    {
        //        var dropDownItem = E2MultipleChoiceSingleAnswerQuestionDropdownList(answerItem.Id,
        //            answerItem.AnswerList,
        //            answerItem.AnswerValue,
        //            answerItem.IsRequired,
        //            answerItem.IsDisabled,
        //            answerItem.AdditionalCssClass);
        //        answerDropDownList.Add(dropDownItem);
        //    }

        //    var mainDivControl = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Full, "no-padding-left-right");
        //    mainDivControl.AddCssClass("multiple-choice-single-answer");
        //    questionText = GetObjectSection(questionText);
        //    mainDivControl.InnerHtml += string.Format(questionText, answerDropDownList.ToArray());

        //    return MvcHtmlString.Create(mainDivControl.ToString());
        //}

        //public static MvcHtmlString E2MultipleChoiceDropDownList(this HtmlHelper htmlHelper,
        //    IEnumerable<AnswerDropDownItem> answerList,
        //    string questionText)
        //{
        //    var answerDropDownList = new List<object>();
        //    foreach (var answerItem in answerList)
        //    {
        //        var dropDownItem = E2MultipleChoiceSingleAnswerQuestionDropdownList(answerItem.Id,
        //            answerItem.AnswerList,
        //            answerItem.AnswerValue,
        //            answerItem.IsRequired,
        //            answerItem.IsDisabled,
        //            answerItem.AdditionalCssClass);
        //        answerDropDownList.Add(dropDownItem);
        //    }

        //    var mainDivControl = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Full, "no-padding-left-right");
        //    mainDivControl.AddCssClass("multiple-choice-single-answer");
        //    mainDivControl.InnerHtml += string.Format(questionText, answerDropDownList.ToArray());

        //    return MvcHtmlString.Create(mainDivControl.ToString());
        //}


        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2GeneralEnglishLink(this HtmlHelper htmlHelper,
        //    string lessonName,
        //    int lessonId,
        //    string lessonType)
        //{
        //    var linkTag = new TagBuilder("a");

        //    switch (lessonType)
        //    {
        //        case CoreConstant.LessonType.Grammar:
        //            linkTag.Attributes.Add("href", "#");
        //            linkTag.Attributes.Add("onclick", "showGrammarPopUp(" + lessonId + ")");
        //            break;

        //        case CoreConstant.LessonType.Listening:
        //            linkTag.Attributes.Add("href", "#");
        //            linkTag.Attributes.Add("onclick", "showListeningPopUp(" + lessonId + ")");
        //            break;

        //        case CoreConstant.LessonType.Speaking:
        //            linkTag.Attributes.Add("href", "#");
        //            linkTag.Attributes.Add("onclick", "showSpeakingPopUp(" + lessonId + ")");
        //            break;

        //        case CoreConstant.LessonType.Reading:
        //            linkTag.Attributes.Add("href", "#");
        //            linkTag.Attributes.Add("onclick", "showReadingPopUp(" + lessonId + ")");
        //            break;

        //        case CoreConstant.LessonType.AutomaticWriting:
        //            linkTag.Attributes.Add("href", "#");
        //            linkTag.Attributes.Add("onclick", "showWritingPopUp(" + lessonId + ")");
        //            break;
        //    }

        //    linkTag.InnerHtml += lessonName;
        //    linkTag.Attributes.Add("id", "lesson-" + lessonId);
        //    return MvcHtmlString.Create(linkTag.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationPanelBody(this HtmlHelper htmlHelper,
        //    ActivityDto activityDto,
        //    bool isFreeStudentPackage,
        //    string timeZoneId)
        //{
        //    var cardBodyContent = E2ActivityDescription(
        //        activityDto.ActionType,
        //        activityDto.Id,
        //        activityDto.IsAlreadyTaken,
        //        activityDto.LastTakenDateTime,
        //        timeZoneId,
        //        activityDto.FirstTakenDateTime,
        //        activityDto.ActivityPoint,
        //        activityDto.ActivityScore,
        //        activityDto.IsFreeLesson,
        //        isFreeStudentPackage,
        //        activityDto.GroupName);

        //    var outerDivTag = new TagBuilder("div");

        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("panel-body");
        //    divTag.InnerHtml += cardBodyContent;

        //    outerDivTag.InnerHtml += divTag;

        //    return MvcHtmlString.Create(outerDivTag.ToString());
        //}


        //public static MvcHtmlString E2ExamPreparationPanelHeader(this HtmlHelper htmlHelper,
        //    ActivityDto activityDto,
        //    bool isFreeStudentPackage,
        //    string packageCode,
        //    string description)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("panel-heading");

        //    var headerTag = new TagBuilder("h4");
        //    headerTag.AddCssClass("panel-title");

        //    var badgeDivTag = new TagBuilder("div");
        //    badgeDivTag.AddCssClass("badge");
        //    badgeDivTag.Attributes.Add("id", "badge-" + activityDto.Id);

        //    var isOnPaidThreshold = IsOnPaidThreshold(packageCode, activityDto.PaidThresholdCode);

        //    if ((isFreeStudentPackage && !activityDto.IsFreeLesson) || !isOnPaidThreshold)
        //    {
        //        var headerLinkTag = new TagBuilder("a");
        //        headerLinkTag.Attributes.Add("href", "#");
        //        headerLinkTag.AddCssClass("collapsed");

        //        // TODO: we need to fix this logic and merge it with some logic below.
        //        badgeDivTag.InnerHtml += "Upgrade";
        //        badgeDivTag.AddCssClass("badge--upgrade");

        //        headerLinkTag.InnerHtml += badgeDivTag;
        //        headerLinkTag.InnerHtml += activityDto.Name;
        //        headerTag.InnerHtml += headerLinkTag;

        //        divTag.InnerHtml += headerTag.ToString();

        //        return MvcHtmlString.Create(divTag.ToString());
        //    }

        //    var linkTag = new TagBuilder("a");
        //    linkTag.Attributes.Add("data-activity-id", activityDto.Id.ToString(CultureInfo.InvariantCulture));

        //    switch (activityDto.ActionType)
        //    {
        //        case CoreConstant.PreparationActivityTypeCode.AzureVideo:
        //        case CoreConstant.PreparationActivityTypeCode.YoutubeVideo:
        //            linkTag.Attributes.Add("id", "videoLink");
        //            linkTag.Attributes.Add("class", "video-link");
        //            linkTag.Attributes.Add("data-assosiated-action-Id",
        //                activityDto.AssosiatedActionId.ToString(CultureInfo.InvariantCulture));
        //            linkTag.Attributes.Add("data-action-type", activityDto.ActionType);
        //            linkTag.Attributes.Add("data-activity-name", activityDto.Name);
        //            linkTag.Attributes.Add("data-url", GenerateUrl("Index", "VideoLesson"));
        //            linkTag.Attributes.Add("data-callback-url", GenerateUrl("MarkAsViewed", "VideoLesson"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DownloadE2Pronounce:
        //            linkTag.Attributes.Add("class", "pronounce-download");
        //            linkTag.Attributes.Add("data-callback-url", GenerateUrl("MarkAsClicked", "ExamPreparation"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.RepeatSentence:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "RepeatSentence"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.MultipleChoiceSingleAnswer:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "MultipleChoiceSingleAnswer"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DownloadPdf:
        //            linkTag.Attributes.Add("class", "pdf-download");
        //            linkTag.Attributes.Add("href",
        //                GenerateUrl("Index", "DocumentLesson", new {documentId = activityDto.AssosiatedActionId}));
        //            linkTag.Attributes.Add("target", "_blank");
        //            linkTag.Attributes.Add("data-callback-url", GenerateUrl("MarkAsDownloaded", "DocumentLesson"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.SummarizeWritingAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.SummarizeWriting:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "SummarizeWriting"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.WriteEssayAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.WriteEssay:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "WriteEssay"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DescribeImageAssessment:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "DescribeImage"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadAloudAssessment:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "ReadAloud"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadAloudPractice:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "ReadAloudPractice"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.AnswerShortQuestion:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "AnswerShortQuestion"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReTellLectureAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.ReTellLecture:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "ReTellLecture"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.Pronunciation:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("Index", "Pronunciation"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.FillInTheBlank:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "FillInTheBlank"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.WriteFromDictation:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "FillInTheBlank"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.MultipleChoiceMultipleAnswer:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "MultipleChoiceMultipleAnswer"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.GrammarLesson:
        //            linkTag.Attributes["data-activity-id"] =
        //                activityDto.AssosiatedActionId.ToString(CultureInfo.InvariantCulture);
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-param", "lessonId");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("View", "GrammarExercise"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.OrderParagraph:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "OrderParagraph"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.HighlightIncorrect:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "HighlightIncorrect"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadingWritingFillInTheBlank:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "ReadingWritingFillInTheBlank"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.VocabularyBuilder:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "VocabularyBuilder"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.HighlightCorrectSummary:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "MultipleChoiceSingleAnswer"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.TargetLink:
        //            linkTag.Attributes.Add("class", "target-link");
        //            linkTag.Attributes.Add("href", description);
        //            linkTag.Attributes.Add("target", "_blank");
        //            linkTag.Attributes.Add("data-callback-url", GenerateUrl("MarkAsClicked", "ExamPreparation"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.FillInTheBox:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "FillInTheBox"));
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DescribeImagePractice:
        //            linkTag.Attributes.Add("class", "activity-link");
        //            linkTag.Attributes.Add("data-url", GenerateUrl("StartPage", "DescribeImagePractice"));
        //            break;
        //    }

        //    // TODO: we need to fix this logic and merge it with some logic aboe.
        //    if (activityDto.IsAlreadyTaken)
        //    {
        //        badgeDivTag.AddCssClass("badge--success");
        //        badgeDivTag.InnerHtml += "Done";
        //    }
        //    else
        //    {
        //        if (isFreeStudentPackage)
        //        {
        //            if (activityDto.IsFreeLesson)
        //            {
        //                badgeDivTag.AddCssClass("badge--free");
        //                badgeDivTag.InnerHtml += "Free";
        //            }
        //            else
        //            {
        //                badgeDivTag.AddCssClass("badge--upgrade");
        //                badgeDivTag.InnerHtml += "Upgrade";
        //            }
        //        }
        //        else
        //        {
        //            badgeDivTag.InnerHtml += "Not Done";
        //        }
        //    }

        //    linkTag.InnerHtml += badgeDivTag.ToString();
        //    linkTag.InnerHtml += activityDto.Name;

        //    linkTag.AddCssClass("collapsed");
        //    headerTag.InnerHtml += linkTag.ToString();

        //    divTag.InnerHtml += headerTag.ToString();

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationTabPanel(this HtmlHelper htmlHelper,
        //    IEnumerable<ModuleDto> moduleDtos,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode)
        //{
        //    var result = new StringBuilder();
        //    var isFirst = true;
        //    foreach (var moduleDto in moduleDtos)
        //    {
        //        var currentPanelId = string.Format(ModuleDivIdPattern, moduleDto.Id);
        //        var currentPanelCssClass = DefaultTabPanelClass;
        //        if (isFirst && !moduleDto.IsCompleted)
        //        {
        //            currentPanelCssClass = ActiveTabPanelClass;
        //            isFirst = false;
        //        }

        //        var tabPanelHtml = E2ExamPreparationTabPanel(htmlHelper,
        //            currentPanelId,
        //            currentPanelCssClass,
        //            moduleDto,
        //            isFreeStudentPackage,
        //            timeZoneId,
        //            packageCode);
        //        result.AppendLine(tabPanelHtml.ToString());
        //    }

        //    return MvcHtmlString.Create(result.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationTabPanel(this HtmlHelper htmlHelper,
        //    string mainPanelId,
        //    string mainPanelCssClass,
        //    ModuleDto moduleDto,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode)
        //{
        //    var panelGroupDiv = new TagBuilder("div");
        //    panelGroupDiv.Attributes.Add("id", string.Format(AccordionDivIdPattern, moduleDto.Id));
        //    panelGroupDiv.AddCssClass("panel-group");

        //    foreach (var subModuleDto in moduleDto.SubModuleDtos)
        //    {
        //        var subTabPanel = E2ExamPreparationTabSubPanel(htmlHelper,
        //            moduleDto,
        //            subModuleDto,
        //            isFreeStudentPackage,
        //            timeZoneId,
        //            packageCode);
        //        panelGroupDiv.InnerHtml += subTabPanel;
        //    }

        //    var tabPanelDiv = new TagBuilder("div");
        //    tabPanelDiv.Attributes.Add("id", mainPanelId);
        //    tabPanelDiv.AddCssClass(mainPanelCssClass);
        //    tabPanelDiv.InnerHtml += panelGroupDiv;

        //    return MvcHtmlString.Create(tabPanelDiv.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationTabSubPanel(this HtmlHelper htmlHelper,
        //    ModuleDto moduleDto,
        //    SubModuleDto subModuleDto,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode)
        //{
        //    var primaryPanelDiv = new TagBuilder("div");
        //    primaryPanelDiv.Attributes.Add("id", string.Format(SubModulePrimaryDivIdPattern, subModuleDto.Id));
        //    primaryPanelDiv.AddCssClass("panel panel-accordion");

        //    if (subModuleDto.IsCompleted)
        //    {
        //        primaryPanelDiv.AddCssClass(CardPanelSuccessClass);
        //    }

        //    var panelGroupLink = string.Format("#" + AccordionDivIdPattern, moduleDto.Id);
        //    var collapsePanelLink = string.Format("#" + SubModuleDivIdPattern, subModuleDto.Id);
        //    var collapsePanelId = string.Format(SubModuleDivIdPattern, subModuleDto.Id);
        //    var checkedIconSubModuleId = string.Format(CheckedModuleIconIdPattern, subModuleDto.Id);

        //    // create panel header
        //    var linkHeader = new TagBuilder("a");
        //    linkHeader.Attributes.Add("data-toggle", "collapse");
        //    linkHeader.Attributes.Add("data-parent", panelGroupLink);
        //    linkHeader.Attributes.Add("href", collapsePanelLink);
        //    linkHeader.InnerHtml += "<span class=\"fa fa-angle-right\"></span>";
        //    linkHeader.InnerHtml += subModuleDto.Name;

        //    var checkBoxTag = new TagBuilder("i");
        //    checkBoxTag.Attributes.Add("id", checkedIconSubModuleId);

        //    if (subModuleDto.IsCompleted)
        //    {
        //        checkBoxTag.AddCssClass("fa fa-check-circle");
        //    }

        //    linkHeader.InnerHtml += checkBoxTag.ToString();

        //    var titleHeader = new TagBuilder("h4");
        //    titleHeader.AddCssClass("panel-title");
        //    titleHeader.InnerHtml += linkHeader;

        //    var headerPanelDiv = new TagBuilder("div");
        //    headerPanelDiv.AddCssClass("panel-heading");
        //    headerPanelDiv.InnerHtml += titleHeader;

        //    primaryPanelDiv.InnerHtml += headerPanelDiv;

        //    // create panel body
        //    var panelBodyDiv = new TagBuilder("div");
        //    panelBodyDiv.AddCssClass("panel-body");

        //    foreach (var groupActivityDto in subModuleDto.GroupActivityDtos.OrderBy(item => item.SortingIndex))
        //    {
        //        if (!subModuleDto.IsHaveGroup)
        //        {
        //            var cardPanel = E2ExamPreparationGenerateCardPanel(htmlHelper,
        //                groupActivityDto.ActivityDtos,
        //                isFreeStudentPackage,
        //                timeZoneId,
        //                packageCode);
        //            panelBodyDiv.InnerHtml += cardPanel;
        //            continue;
        //        }

        //        var groupActivityPanel = E2ExamPreparationGroupCardPanel(htmlHelper,
        //            groupActivityDto,
        //            isFreeStudentPackage,
        //            timeZoneId,
        //            packageCode);
        //        panelBodyDiv.InnerHtml += groupActivityPanel;
        //    }

        //    var panelCollapseDiv = new TagBuilder("div");
        //    panelCollapseDiv.Attributes.Add("id", collapsePanelId);
        //    panelCollapseDiv.AddCssClass("panel-collapse collapse");
        //    panelCollapseDiv.InnerHtml += panelBodyDiv;

        //    primaryPanelDiv.InnerHtml += panelCollapseDiv;

        //    return MvcHtmlString.Create(primaryPanelDiv.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationGroupCardPanel(this HtmlHelper htmlHelper,
        //    GroupActivityDto groupActivityDto,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode)
        //{
        //    var primaryPanelDiv = new TagBuilder("div");
        //    primaryPanelDiv.Attributes.Add("id", string.Format(GroupActivityIdPattern, groupActivityDto.Id));
        //    primaryPanelDiv.AddCssClass("panel panel-card");

        //    if (groupActivityDto.IsCompleted)
        //    {
        //        primaryPanelDiv.AddCssClass(CardPanelSuccessClass);
        //    }
        //    else
        //    {
        //        primaryPanelDiv.AddCssClass(DefaultCardPanelClass);
        //    }

        //    var titleHeader = new TagBuilder("h4");
        //    titleHeader.AddCssClass("panel-title");

        //    var linkTag = new TagBuilder("a");
        //    linkTag.Attributes.Add("href", "#");
        //    linkTag.AddCssClass("collapsed");
        //    linkTag.InnerHtml += GeneratePteGroupName(groupActivityDto.Name);

        //    var checkBoxTag = new TagBuilder("i");
        //    checkBoxTag.Attributes.Add("id", string.Format(CheckedGroupIconIdPattern, groupActivityDto.Id));

        //    if (groupActivityDto.IsCompleted)
        //    {
        //        checkBoxTag.AddCssClass("fa fa-check-circle");
        //    }

        //    linkTag.InnerHtml += checkBoxTag.ToString();

        //    titleHeader.InnerHtml += linkTag.ToString();

        //    var headerPanelDiv = new TagBuilder("div");
        //    headerPanelDiv.AddCssClass("panel-heading");
        //    headerPanelDiv.InnerHtml += titleHeader;

        //    primaryPanelDiv.InnerHtml += headerPanelDiv;

        //    // create panel body
        //    var panelBodyDiv = E2ExamPreparationGenerateCardPanel(htmlHelper,
        //        groupActivityDto.ActivityDtos,
        //        isFreeStudentPackage,
        //        timeZoneId,
        //        packageCode);
        //    primaryPanelDiv.InnerHtml += panelBodyDiv;

        //    return MvcHtmlString.Create(primaryPanelDiv.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationGenerateCardPanel(this HtmlHelper htmlHelper,
        //    ICollection<ActivityDto> activityDtos,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode)
        //{
        //    var panelBodyDiv = new TagBuilder("div");
        //    panelBodyDiv.AddCssClass("panel-body");

        //    var cardGroupDiv = new TagBuilder("div");
        //    cardGroupDiv.AddCssClass("row");

        //    var index = 1;
        //    var totalActivities = activityDtos.Count;
        //    var sortedActivities = activityDtos.OrderBy(item => item.SortingIndex).ToList();
        //    foreach (var activityDto in sortedActivities)
        //    {
        //        var cardPanel = E2ExamPreparationCardPanel(htmlHelper,
        //            activityDto,
        //            isFreeStudentPackage,
        //            timeZoneId,
        //            packageCode,
        //            activityDto.Description);
        //        cardGroupDiv.InnerHtml += cardPanel;

        //        if (index%3 == 0 || index == totalActivities)
        //        {
        //            panelBodyDiv.InnerHtml += cardGroupDiv;
        //            cardGroupDiv = new TagBuilder("div");
        //            cardGroupDiv.AddCssClass("row");
        //        }
        //        index++;
        //    }

        //    return MvcHtmlString.Create(panelBodyDiv.ToString());
        //}

        //public static MvcHtmlString E2ExamPreparationCardPanel(this HtmlHelper htmlHelper,
        //    ActivityDto activityDto,
        //    bool isFreeStudentPackage,
        //    string timeZoneId,
        //    string packageCode,
        //    string description)
        //{
        //    var cardHeaderPanelDiv = E2ExamPreparationPanelHeader(htmlHelper,
        //        activityDto,
        //        isFreeStudentPackage,
        //        packageCode,
        //        description);
        //    var cardBodyPanelDiv = E2ExamPreparationPanelBody(htmlHelper, activityDto, isFreeStudentPackage, timeZoneId);

        //    var mainPanelId = string.Format(CardPanelDivIdPattern, activityDto.Id);

        //    var mainPanelDiv = new TagBuilder("div");
        //    mainPanelDiv.Attributes.Add("id", mainPanelId);
        //    mainPanelDiv.AddCssClass("panel panel-card");
        //    mainPanelDiv.AddCssClass(activityDto.IsAlreadyTaken ? CardPanelSuccessClass : DefaultCardPanelClass);
        //    mainPanelDiv.InnerHtml += cardHeaderPanelDiv;
        //    if (activityDto.ActionType != CoreConstant.PreparationActivityTypeCode.TargetLink)
        //    {
        //        mainPanelDiv.InnerHtml += cardBodyPanelDiv;
        //    }

        //    var mainWrapperDiv = new TagBuilder("div");
        //    mainWrapperDiv.AddCssClass("col-md-4");
        //    mainWrapperDiv.InnerHtml += mainPanelDiv;

        //    return MvcHtmlString.Create(mainWrapperDiv.ToString());
        //}

        //public static MvcHtmlString E2InstructionPanel(this HtmlHelper htmlHelper,
        //    string panelTitle,
        //    string panelInstruction)
        //{
        //    if (string.IsNullOrEmpty(panelTitle) || string.IsNullOrEmpty(panelInstruction))
        //    {
        //        return MvcHtmlString.Create(string.Empty);
        //    }

        //    var panelDivTag = new TagBuilder("div");
        //    panelDivTag.Attributes.Add("class", "panel panel-primary");

        //    if (!string.IsNullOrEmpty(panelTitle) && !string.IsNullOrEmpty(panelInstruction))
        //    {
        //        var panelHeaderDivTag = new TagBuilder("div");
        //        panelHeaderDivTag.Attributes.Add("class", "panel-heading");
        //        panelHeaderDivTag.InnerHtml += panelTitle;

        //        panelDivTag.InnerHtml += panelHeaderDivTag;

        //        var panelBodyDivTag = new TagBuilder("div");
        //        panelBodyDivTag.Attributes.Add("class", "panel-body line-height-20");
        //        panelBodyDivTag.InnerHtml += htmlHelper.Raw(panelInstruction);
        //        panelDivTag.InnerHtml += panelBodyDivTag;
        //    }

        //    return MvcHtmlString.Create(panelDivTag.ToString());
        //}

        //public static MvcHtmlString E2InstructionPanel(this HtmlHelper htmlHelper,
        //    string panelTitle,
        //    string panelInstruction,
        //    string audioName)
        //{
        //    if (string.IsNullOrEmpty(panelTitle) && string.IsNullOrEmpty(panelInstruction))
        //    {
        //        return MvcHtmlString.Create(string.Empty);
        //    }

        //    if (string.IsNullOrEmpty(audioName))
        //    {
        //        return MvcHtmlString.Create(string.Empty);
        //    }

        //    var panelDivTag = new TagBuilder("div");
        //    panelDivTag.Attributes.Add("class", "panel panel-primary");

        //    if (!string.IsNullOrEmpty(panelTitle))
        //    {
        //        var panelHeaderDivTag = new TagBuilder("div");
        //        panelHeaderDivTag.Attributes.Add("class", "panel-heading");
        //        panelHeaderDivTag.InnerHtml += panelTitle;

        //        panelDivTag.InnerHtml += panelHeaderDivTag;
        //    }

        //    var panelBodyContent = string.Empty;
        //    if (!string.IsNullOrEmpty(panelInstruction))
        //    {
        //        panelBodyContent += htmlHelper.Raw(panelInstruction);
        //    }

        //    if (!string.IsNullOrEmpty(audioName))
        //    {
        //        panelBodyContent += GenerateAudioPlayer(audioName);
        //    }

        //    if (string.IsNullOrEmpty(panelBodyContent))
        //    {
        //        return MvcHtmlString.Create(panelDivTag.ToString());
        //    }

        //    var panelBodyDivTag = new TagBuilder("div");
        //    panelBodyDivTag.Attributes.Add("class", "panel-body line-height-20");
        //    panelDivTag.InnerHtml += panelBodyContent;

        //    panelDivTag.InnerHtml += panelBodyDivTag.ToString();

        //    return MvcHtmlString.Create(panelDivTag.ToString());
        //}

        //public static MvcHtmlString E2AnswerShortQuestionContent(this HtmlHelper htmlHelper,
        //    string content,
        //    ICollection<AnswerShortQuestionAnswerDto> answerCollection,
        //    int answerShortQuestionId)
        //{
        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);

        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("form-group");

        //    foreach (Match regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString();
        //        var stringSections = stringOperation.TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);

        //        if (stringSections[0].Contains("ShowAnswer"))
        //        {
        //            var answerKey = stringSections[1];
        //            var correctAnswers =
        //                answerCollection.Where(item => item.AnswerKey == answerKey)
        //                    .Select(item => item.Answer)
        //                    .ToArray();
        //            var correctAnswerContent = string.Join(" / ", correctAnswers);

        //            var additionalSection = GetAdditionalPanel(stringSections[0], correctAnswerContent);
        //            content = content.Replace(stringOperation, additionalSection);

        //            continue;
        //        }

        //        if (stringOperation.Contains("IMG"))
        //        {
        //            var imageTag = new TagBuilder("img");
        //            imageTag.AddCssClass("img-responsive center-block");
        //            imageTag.Attributes.Add("src", ImageManager.GetCdnImageUri(stringSections[1]));

        //            content = content.Replace(stringOperation, imageTag.ToString());
        //            continue;
        //        }

        //        if (stringSections[0].Contains("Audio"))
        //        {
        //            var audioUri = AudioManager.GetCdnAudioUri(stringSections[1]);

        //            var audioPlayerTag = new TagBuilder("audio");
        //            audioPlayerTag.Attributes.Add("controls", "controls");
        //            audioPlayerTag.Attributes.Add("src", audioUri);
        //            audioPlayerTag.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //            var divAudioTag = new TagBuilder("div");
        //            divAudioTag.Attributes.Add("id", "audioPlayerControl");
        //            divAudioTag.InnerHtml += audioPlayerTag;


        //            content = content.Replace(stringOperation, divAudioTag.ToString());
        //            continue;
        //        }

        //        var key = "ShortAnswer_" + answerShortQuestionId + "_" + stringSections[0];
        //        var textboxTag = new TagBuilder("input");
        //        textboxTag.Attributes.Add("type", "text");
        //        textboxTag.Attributes.Add("name", key);
        //        textboxTag.Attributes.Add("id", key);
        //        textboxTag.AddCssClass("input-normal form-control fill-in-the-blank-textbox");

        //        content = content.Replace(stringOperation, textboxTag.ToString());
        //    }

        //    divTag.InnerHtml += content;

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //public static MvcHtmlString E2RepeatSentenceContent(this HtmlHelper htmlHelper,
        //    string content,
        //    int repeatSentenceId)
        //{
        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);

        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("form-group");

        //    foreach (Match regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString();
        //        var stringSections = stringOperation.TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);

        //        if (stringSections[0].Contains("ShowHint"))
        //        {
        //            var additionalSection = GetAdditionalPanel(stringSections[0], stringSections[1]);
        //            content = content.Replace(stringOperation, additionalSection);

        //            continue;
        //        }

        //        if (stringSections[0].Contains("Audio"))
        //        {
        //            var audioUri = AudioManager.GetCdnAudioUri(stringSections[1]);

        //            var audioPlayerTag = new TagBuilder("audio");
        //            audioPlayerTag.Attributes.Add("controls", "controls");
        //            audioPlayerTag.Attributes.Add("src", audioUri);
        //            audioPlayerTag.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //            var divAudioTag = new TagBuilder("div");
        //            divAudioTag.Attributes.Add("id", "audioPlayerControl");
        //            divAudioTag.InnerHtml += audioPlayerTag;


        //            content = content.Replace(stringOperation, divAudioTag.ToString());
        //            continue;
        //        }

        //        var key = "RepeatSentenceAnswer_" + repeatSentenceId + "_" + stringSections[0];
        //        var textboxTag = new TagBuilder("input");
        //        textboxTag.Attributes.Add("type", "text");
        //        textboxTag.Attributes.Add("name", key);
        //        textboxTag.Attributes.Add("id", key);
        //        textboxTag.AddCssClass("form-control");

        //        content = content.Replace(stringOperation, textboxTag.ToString());
        //    }

        //    divTag.InnerHtml += content;

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2FillInTheBlankContent(this HtmlHelper htmlHelper,
        //    string content,
        //    ICollection<FillInTheBlankAnswerDto> fillInTheBlankAnswersDtos,
        //    int fillInTheBlankId,
        //    string type)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("form-group");

        //    foreach (var fillInTheBlankAnswer in fillInTheBlankAnswersDtos)
        //    {
        //        var key = "FillTheBlankAnswer_" + fillInTheBlankId + "_" + fillInTheBlankAnswer.AnswerKey;
        //        var replacementInput = new TagBuilder("input");
        //        replacementInput.Attributes.Add("type", "text");
        //        replacementInput.Attributes.Add("name", key);
        //        replacementInput.Attributes.Add("id", key);
        //        if (type == CoreConstant.PreparationActivityTypeCode.FillInTheBlank)
        //        {
        //            replacementInput.AddCssClass("input-normal form-control fill-in-the-blank-textbox");
        //        }
        //        else if (type == CoreConstant.PreparationActivityTypeCode.WriteFromDictation)
        //        {
        //            replacementInput.AddCssClass("form-control");
        //        }

        //        content = content.Replace("{" + fillInTheBlankAnswer.AnswerKey + "}", replacementInput.ToString());
        //    }

        //    content = GetObjectSection(content);

        //    divTag.InnerHtml += content;

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //public static MvcHtmlString E2FillInTheBoxContent(this HtmlHelper htmlHelper, string content)
        //{
        //    var spanTag = new TagBuilder("span");

        //    content = GetFillInTheBoxContent(content);

        //    spanTag.InnerHtml += content;

        //    return MvcHtmlString.Create(spanTag.ToString());
        //}

        //public static MvcHtmlString E2FillInTheBoxAnswerSheet(this HtmlHelper htmlHelper,
        //    ICollection<FillInTheBoxAnswerDto> answerBoxItem)
        //{
        //    var divFormGroupTag = new TagBuilder("div");
        //    divFormGroupTag.AddCssClass("form-group");

        //    var count = 1;
        //    foreach (var fillInTheBoxAnswerDto in answerBoxItem)
        //    {
        //        var divDefaultPanelTag = new TagBuilder("div");
        //        divDefaultPanelTag.AddCssClass("panel panel-default panel-answer ");

        //        var divBodyPanelTag = new TagBuilder("div");
        //        divBodyPanelTag.AddCssClass("panel-body");

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("exercise-text");

        //        var inputTag = new TagBuilder("input");
        //        inputTag.AddCssClass("input-normal form-control");
        //        inputTag.Attributes.Add("id",
        //            "FillTheBoxAnswer_" + fillInTheBoxAnswerDto.FillInTheBoxId + "_" + fillInTheBoxAnswerDto.AnswerKey);
        //        inputTag.Attributes.Add("name",
        //            "FillTheBoxAnswer_" + fillInTheBoxAnswerDto.FillInTheBoxId + "_" + fillInTheBoxAnswerDto.AnswerKey);

        //        paragraphTag.InnerHtml += count + ") ";
        //        paragraphTag.InnerHtml += inputTag.ToString();

        //        divBodyPanelTag.InnerHtml += paragraphTag.ToString();
        //        divDefaultPanelTag.InnerHtml += divBodyPanelTag.ToString();
        //        divFormGroupTag.InnerHtml += divDefaultPanelTag.ToString();

        //        count++;
        //    }

        //    return MvcHtmlString.Create(divFormGroupTag.ToString());
        //}

        //public static string GetAdditionalPanel(string type, string source)
        //{
        //    var panelTag = new TagBuilder("div");
        //    panelTag.Attributes.Add("class", "panel panel-default panel-widget");

        //    var newIdPanel = Guid.NewGuid();

        //    var panelBodyTag = new TagBuilder("div");
        //    panelBodyTag.Attributes.Add("class", "panel-body");

        //    var collapsedLinkTag = new TagBuilder("a");
        //    collapsedLinkTag.Attributes.Add("role", "button");
        //    collapsedLinkTag.Attributes.Add("data-toggle", "collapse");
        //    collapsedLinkTag.Attributes.Add("href", "#" + newIdPanel);

        //    // generate content for body html
        //    if (type.Contains("Transcript"))
        //    {
        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.InnerHtml += source;
        //        panelBodyTag.InnerHtml += paragraphTag;
        //        collapsedLinkTag.InnerHtml += Resource.Pte_ShowTranscript;
        //    }

        //    if (type.Contains("ShowHint"))
        //    {
        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.InnerHtml += source;
        //        panelBodyTag.InnerHtml += paragraphTag;
        //        collapsedLinkTag.InnerHtml += Resource.Pte_ShowHint;
        //    }

        //    if (type.Contains("ShowAnswer"))
        //    {
        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.InnerHtml += source;
        //        panelBodyTag.InnerHtml += paragraphTag;
        //        collapsedLinkTag.InnerHtml += Resource.Pte_ShowAnswer;
        //    }

        //    var titleTag = new TagBuilder("h3");
        //    titleTag.Attributes.Add("class", "panel-title");
        //    titleTag.InnerHtml += collapsedLinkTag;

        //    var panelHeadingTag = new TagBuilder("div");
        //    panelHeadingTag.Attributes.Add("class", "panel-heading");
        //    panelHeadingTag.Attributes.Add("role", "tab");
        //    panelHeadingTag.InnerHtml += titleTag;
        //    panelTag.InnerHtml += panelHeadingTag;

        //    var collapsePanelTag = new TagBuilder("div");
        //    collapsePanelTag.Attributes.Add("class", "panel-collapse collapse");
        //    collapsePanelTag.Attributes.Add("role", "tabpanel");
        //    collapsePanelTag.Attributes.Add("id", newIdPanel.ToString());
        //    collapsePanelTag.InnerHtml += panelBodyTag;

        //    panelTag.InnerHtml += collapsePanelTag;

        //    return panelTag.ToString();
        //}

        //public static MvcHtmlString E2DurationText(this HtmlHelper htmlHelper, int durations, string idName)
        //{
        //    var minutes = durations/60;
        //    var secondsLeft = durations%60;
        //    var timeSpan = new TimeSpan(0, 0, minutes, secondsLeft);

        //    var spanTag = new TagBuilder("span");
        //    spanTag.Attributes.Add("id", idName);
        //    spanTag.InnerHtml += timeSpan.ToString(@"mm\:ss");

        //    return MvcHtmlString.Create(spanTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2GenerateMultipleAnswerContent(this HtmlHelper htmlHelper,
        //    IEnumerable<AnswerComboBoxItem> answerList,
        //    string questionText)
        //{
        //    var comboboxList = new List<object>();

        //    foreach (var answerComboBoxItem in answerList)
        //    {
        //        var comboBoxItem = E2ComboBoxList(answerComboBoxItem.Id, answerComboBoxItem.AnswerList);
        //        comboboxList.Add(comboBoxItem);
        //    }

        //    var mainDivControl = GetMainControlDivTag(UiControlEnum.ControlWidthOption.Full, "no-padding-left-right");

        //    questionText = GetObjectSection(questionText);

        //    mainDivControl.InnerHtml += string.Format(questionText, comboboxList.ToArray());
        //    return MvcHtmlString.Create(mainDivControl.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2GenerateGrammarLessonTestContent(this HtmlHelper htmlHelper,
        //    ICollection<GrammarTestQuestionDto> grammarTestQuestion)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("row");

        //    var countNumber = 1;

        //    foreach (var grammarTestQuestionDto in grammarTestQuestion)
        //    {
        //        var comboBoxTextList = new List<object>();
        //        var divTag2 = new TagBuilder("div");
        //        divTag2.AddCssClass("panel panel-default");

        //        var divTag3 = new TagBuilder("div");
        //        divTag3.AddCssClass("panel-body");

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("test-paragraph");

        //        var groupIds = grammarTestQuestionDto.GrammarAnswerDtos.Select(item => item.GroupId).Distinct().ToList();

        //        foreach (var groupId in groupIds)
        //        {
        //            var key = "grammarTest_" + grammarTestQuestionDto.Id + "_" + groupId;

        //            var replacementSelect = new TagBuilder("select");
        //            replacementSelect.Attributes.Add("name", key);
        //            replacementSelect.Attributes.Add("id", key);
        //            replacementSelect.AddCssClass("input-normal test-form form-control");
        //            replacementSelect.Attributes.Add("required", "required");
        //            replacementSelect.Attributes.Add("placeholder", "This field is required");

        //            foreach (
        //                var grammarTestAnswerDto in
        //                    grammarTestQuestionDto.GrammarAnswerDtos.Where(item => item.GroupId == groupId))
        //            {
        //                var selectTag = new TagBuilder("option");
        //                selectTag.Attributes.Add("value", grammarTestAnswerDto.Id.ToString());
        //                selectTag.InnerHtml = grammarTestAnswerDto.AnswerText;
        //                replacementSelect.InnerHtml += selectTag.ToString();
        //            }
        //            comboBoxTextList.Add(replacementSelect.ToString());
        //        }

        //        grammarTestQuestionDto.QuestionText = string.Format(grammarTestQuestionDto.QuestionText,
        //            comboBoxTextList.ToArray());

        //        var numberLabelTag = new TagBuilder("label");
        //        numberLabelTag.AddCssClass("label label-default m-r-20");
        //        numberLabelTag.InnerHtml = countNumber.ToString();

        //        paragraphTag.InnerHtml += numberLabelTag + grammarTestQuestionDto.QuestionText;
        //        divTag3.InnerHtml += paragraphTag.ToString();
        //        divTag2.InnerHtml += divTag3.ToString();
        //        divTag.InnerHtml += divTag2.ToString();
        //        countNumber++;
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2GenerateListeningLessonTestContent(this HtmlHelper htmlHelper,
        //    ICollection<ListeningTestQuestionDto> listeningTestQuestion)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("row");

        //    var countNumber = 1;

        //    foreach (var listeningTestQuestionDto in listeningTestQuestion)
        //    {
        //        var comboBoxTextList = new List<object>();
        //        var divTag2 = new TagBuilder("div");
        //        divTag2.AddCssClass("panel panel-default");

        //        var divTag3 = new TagBuilder("div");
        //        divTag3.AddCssClass("panel-body");

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("test-paragraph");

        //        var groupIds =
        //            listeningTestQuestionDto.ListeningTestAnswerDtos.Select(item => item.GroupId).Distinct().ToList();

        //        foreach (var groupId in groupIds)
        //        {
        //            var key = "listeningTest_" + listeningTestQuestionDto.Id + "_" + groupId;

        //            var replacementSelect = new TagBuilder("select");
        //            replacementSelect.Attributes.Add("name", key);
        //            replacementSelect.Attributes.Add("id", key);
        //            replacementSelect.AddCssClass("input-normal test-form form-control");
        //            replacementSelect.Attributes.Add("required", "required");
        //            replacementSelect.Attributes.Add("placeholder", "This field is required");

        //            foreach (var listeningTestAnswerDto in listeningTestQuestionDto.ListeningTestAnswerDtos)
        //            {
        //                var selectTag = new TagBuilder("option");
        //                selectTag.Attributes.Add("value", listeningTestAnswerDto.Id.ToString());
        //                selectTag.InnerHtml = listeningTestAnswerDto.AnswerText;
        //                replacementSelect.InnerHtml += selectTag.ToString();
        //            }
        //            comboBoxTextList.Add(replacementSelect.ToString());
        //        }

        //        listeningTestQuestionDto.QuestionText = string.Format(listeningTestQuestionDto.QuestionText,
        //            comboBoxTextList.ToArray());

        //        var numberLabelTag = new TagBuilder("label");
        //        numberLabelTag.AddCssClass("label label-default m-r-20");
        //        numberLabelTag.InnerHtml = countNumber.ToString();

        //        paragraphTag.InnerHtml += numberLabelTag + listeningTestQuestionDto.QuestionText;
        //        divTag3.InnerHtml += paragraphTag.ToString();
        //        divTag2.InnerHtml += divTag3.ToString();
        //        divTag.InnerHtml += divTag2.ToString();
        //        countNumber++;
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2VocabularyBuilderContent(this HtmlHelper htmlHelper,
        //    ICollection<VocabularyBuilderQuestionDto> questions)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("form-group");

        //    var countNumber = 1;

        //    foreach (var vocabularyBuilderQuestionDto in questions)
        //    {
        //        var comboBoxTextList = new List<object>();
        //        var divTag2 = new TagBuilder("div");
        //        divTag2.AddCssClass("panel panel-default");

        //        var divTag3 = new TagBuilder("div");
        //        divTag3.AddCssClass("panel-body");

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("exercise-text");

        //        var groupIds =
        //            vocabularyBuilderQuestionDto.VocabularyBuilderIdAnswerDtos.OrderBy(item => item.GroupId).Select(
        //                item => item.GroupId).Distinct().ToList();
        //        foreach (var groupId in groupIds)
        //        {
        //            var key = "vocabularyBuilder_" + vocabularyBuilderQuestionDto.Id + "_" + groupId;

        //            var replacementSelect = new TagBuilder("select");
        //            replacementSelect.Attributes.Add("name", key);
        //            replacementSelect.Attributes.Add("id", key);
        //            replacementSelect.AddCssClass("input-normal test-form form-control");
        //            replacementSelect.Attributes.Add("required", "required");
        //            replacementSelect.Attributes.Add("placeholder", "This field is required");
        //            var selectedVocabularyBuilderAnswers =
        //                vocabularyBuilderQuestionDto.VocabularyBuilderIdAnswerDtos.Where(item => item.GroupId == groupId)
        //                    .ToList();
        //            foreach (var vocabularyBuilderAnswerDto in selectedVocabularyBuilderAnswers)
        //            {
        //                var selectTag = new TagBuilder("option");
        //                selectTag.Attributes.Add("value", vocabularyBuilderAnswerDto.Id.ToString());
        //                selectTag.InnerHtml = vocabularyBuilderAnswerDto.AnswerText;
        //                replacementSelect.InnerHtml += selectTag.ToString();
        //            }
        //            comboBoxTextList.Add(replacementSelect.ToString());
        //        }

        //        vocabularyBuilderQuestionDto.QuestionText = string.Format(vocabularyBuilderQuestionDto.QuestionText,
        //            comboBoxTextList.ToArray());
        //        paragraphTag.InnerHtml += countNumber + ") " + vocabularyBuilderQuestionDto.QuestionText;
        //        divTag3.InnerHtml += paragraphTag.ToString();
        //        divTag2.InnerHtml += divTag3.ToString();
        //        divTag.InnerHtml += divTag2.ToString();

        //        countNumber++;
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2GenerateReadingLessonTestContent(this HtmlHelper htmlHelper,
        //    ICollection<ReadingTestQuestionDto> readingTestQuestion)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("row");

        //    var countNumber = 1;

        //    foreach (var readingTestQuestionDto in readingTestQuestion)
        //    {
        //        var comboBoxTextList = new List<object>();
        //        var divTag2 = new TagBuilder("div");
        //        divTag2.AddCssClass("panel panel-default");

        //        var divTag3 = new TagBuilder("div");
        //        divTag3.AddCssClass("panel-body");

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("test-paragraph");

        //        var groupIds = readingTestQuestionDto.ReadingAnswerDtos.Select(item => item.GroupId).Distinct().ToList();

        //        foreach (var groupId in groupIds)
        //        {
        //            var key = "readingTest_" + readingTestQuestionDto.Id + "_" + groupId;

        //            var replacementSelect = new TagBuilder("select");
        //            replacementSelect.Attributes.Add("name", key);
        //            replacementSelect.Attributes.Add("id", key);
        //            replacementSelect.AddCssClass("input-normal form-control");
        //            replacementSelect.Attributes.Add("required", "required");
        //            replacementSelect.Attributes.Add("placeholder", "This field is required");

        //            foreach (var grammarTestAnswerDto in readingTestQuestionDto.ReadingAnswerDtos)
        //            {
        //                var selectTag = new TagBuilder("option");
        //                selectTag.Attributes.Add("value", grammarTestAnswerDto.Id.ToString());
        //                selectTag.InnerHtml = grammarTestAnswerDto.AnswerText;
        //                replacementSelect.InnerHtml += selectTag.ToString();
        //            }
        //            comboBoxTextList.Add(replacementSelect.ToString());
        //        }

        //        readingTestQuestionDto.QuestionText = string.Format(readingTestQuestionDto.QuestionText,
        //            comboBoxTextList.ToArray());

        //        var numberLabelTag = new TagBuilder("label");
        //        numberLabelTag.AddCssClass("label label-default m-r-20");
        //        numberLabelTag.InnerHtml = countNumber.ToString();

        //        paragraphTag.InnerHtml += numberLabelTag + readingTestQuestionDto.QuestionText;
        //        divTag3.InnerHtml += paragraphTag.ToString();
        //        divTag2.InnerHtml += divTag3.ToString();
        //        divTag.InnerHtml += divTag2.ToString();
        //        countNumber++;
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2ReadingWritingFillInTheBlankContent(this HtmlHelper htmlHelper,
        //    ICollection<ReadingWritingFillInTheBlankQuestionDto> questions)
        //{
        //    var mainDivTag = new TagBuilder("div");
        //    mainDivTag.AddCssClass("form-group");

        //    var questionNumber = 1;

        //    foreach (var readingWritingFillInTheBlankQuestionDto in questions)
        //    {
        //        var groupIds =
        //            readingWritingFillInTheBlankQuestionDto.ReadingWritingFillInTheBlankAnswerDtos.Select(
        //                item => item.GroupId).Distinct().ToList();
        //        var questionComboBox = GetQuestionComboBoxList(groupIds,
        //            readingWritingFillInTheBlankQuestionDto.ReadingWritingFillInTheBlankAnswerDtos,
        //            readingWritingFillInTheBlankQuestionDto.Id);
        //        readingWritingFillInTheBlankQuestionDto.QuestionText =
        //            string.Format(readingWritingFillInTheBlankQuestionDto.QuestionText, questionComboBox);

        //        var paragraphTag = new TagBuilder("p");
        //        paragraphTag.AddCssClass("exercise-text");
        //        paragraphTag.InnerHtml += questionNumber + ") " + readingWritingFillInTheBlankQuestionDto.QuestionText;

        //        var panelBodyTag = new TagBuilder("div");
        //        panelBodyTag.AddCssClass("panel-body");
        //        panelBodyTag.InnerHtml += paragraphTag.ToString();

        //        var panelDefaultTag = new TagBuilder("div");
        //        panelDefaultTag.AddCssClass("panel panel-default");
        //        panelDefaultTag.InnerHtml += panelBodyTag.ToString();

        //        mainDivTag.InnerHtml += panelDefaultTag.ToString();

        //        questionNumber++;
        //    }

        //    return MvcHtmlString.Create(mainDivTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2HighlightIncorrectContent(this HtmlHelper htmlHelper, string questionText)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.Attributes.Add("id", "highlighted-words");

        //    var splitWords = questionText.Split(' ');

        //    var htmlString = new StringBuilder();

        //    var index = 0;
        //    var paragraphTag = new TagBuilder("p");

        //    foreach (var splitWord in splitWords)
        //    {
        //        var spanTag = new TagBuilder("span");
        //        spanTag.AddCssClass("highlight-word");
        //        spanTag.Attributes.Add("data-index", index.ToString());
        //        spanTag.Attributes.Add("id", "word_" + index);
        //        spanTag.InnerHtml += splitWord;
        //        htmlString.Append(spanTag);
        //        index++;
        //    }
        //    paragraphTag.InnerHtml += htmlString.ToString();
        //    divTag.InnerHtml += paragraphTag.ToString();
        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2OrderParagraphContent(this HtmlHelper htmlHelper,
        //    ICollection<AnswerItem> answerListItems,
        //    string content)
        //{
        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);
        //    foreach (Match regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString();
        //        var stringSections = stringOperation.TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);

        //        if (stringOperation.Contains("Droppable"))
        //        {
        //            var dropParagraphSection = GenerateDroppableSection(stringSections[0]);
        //            content = content.Replace(stringOperation, dropParagraphSection.ToString());
        //            continue;
        //        }

        //        if (stringOperation.Contains("Draggable"))
        //        {
        //            var dragParagraphSection = GenerateDraggableSection(answerListItems, stringSections[0]);
        //            content = content.Replace(stringOperation, dragParagraphSection.ToString());
        //            continue;
        //        }

        //        var generalOrderParagraphResult = GenerateOrderParagraphGrid(answerListItems, stringSections[0]);
        //        content = content.Replace(stringOperation, generalOrderParagraphResult.ToString());
        //    }

        //    return MvcHtmlString.Create(content);
        //}

        //public static MvcHtmlString E2ExamPreparationTabHeader(this HtmlHelper htmlHelper,
        //    IEnumerable<ModuleDto> moduleDtos)
        //{
        //    var result = new StringBuilder();
        //    var isFirst = true;
        //    foreach (var moduleDto in moduleDtos)
        //    {
        //        var tabHrefId = string.Format("#" + ModuleDivIdPattern, moduleDto.Id);
        //        var selectedClass = string.Empty;
        //        if (isFirst && !moduleDto.IsCompleted)
        //        {
        //            selectedClass = ActiveTabHeaderClass;
        //            isFirst = false;
        //        }

        //        var tabHeaderPanelHtml = E2ExamPreparationTabHeader(
        //            moduleDto.Name,
        //            tabHrefId,
        //            selectedClass);
        //        result.AppendLine(tabHeaderPanelHtml.ToString());
        //    }

        //    return MvcHtmlString.Create(result.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2SwitchPageLink(this HtmlHelper htmlHelper,
        //    string roleName,
        //    HttpSessionStateBase session)
        //{
        //    var roleNames = SessionManager.Instance.GetRoleName(session).Split(',');
        //    if (roleNames.Length == 1)
        //    {
        //        return MvcHtmlString.Create(string.Empty);
        //    }

        //    var iconTag = new TagBuilder("i");
        //    iconTag.AddCssClass("fa fa-exchange");

        //    var linkTag = new TagBuilder("a");
        //    if (roleName.Equals(CoreConstant.Role.E2Teacher))
        //    {
        //        linkTag.Attributes.Add("href", GenerateUrl("Index", "Home", new {area = "DutyTutor"}));
        //        linkTag.InnerHtml += iconTag + "&nbsp;" + Resource.Teacher_SwitchToDutyTutor;
        //    }

        //    if (roleName.Equals(CoreConstant.Role.E2DutyTutor))
        //    {
        //        linkTag.Attributes.Add("href", GenerateUrl("Index", "Home", new {area = "Teacher"}));
        //        linkTag.InnerHtml += iconTag + "&nbsp;" + Resource.DutyTutor_SwitchToTeacher;
        //    }

        //    var listTag = new TagBuilder("li");
        //    listTag.InnerHtml += linkTag.ToString();

        //    return MvcHtmlString.Create(listTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString E2CheckBoxListWithLabel(this HtmlHelper htmlHelper,
        //    string name,
        //    string displayName,
        //    CheckBoxList checkBoxList,
        //    bool isReadOnly)
        //{
        //    // Create the outer div
        //    var formGroupDivTag = GetFormGroupDivTag();

        //    // And then the label
        //    var labelTag = GetLabelTag(name, displayName, false);
        //    formGroupDivTag.InnerHtml += labelTag;

        //    // Div control to hold the checkbox list
        //    var checkboxDivTag = new TagBuilder("div");
        //    checkboxDivTag.AddCssClass("col-md-10");

        //    // Then the main control itself
        //    var checkBoxInnerDivTag = new TagBuilder("div");
        //    checkBoxInnerDivTag.AddCssClass("checkbox");

        //    var index = 0;
        //    foreach (var checkboxItem in checkBoxList.Items)
        //    {
        //        var controlId = $"cb{name}{index}";

        //        var mainControlTag = new TagBuilder("input");
        //        mainControlTag.Attributes.Add("id", controlId);
        //        mainControlTag.Attributes.Add("type", "checkbox");
        //        mainControlTag.Attributes.Add("name", $"{name}[{index}]");
        //        mainControlTag.Attributes.Add("value", checkboxItem.Value);

        //        if (checkboxItem.IsChecked)
        //        {
        //            mainControlTag.Attributes.Add("checked", "checked");
        //        }

        //        if (isReadOnly)
        //        {
        //            mainControlTag.Attributes.Add("disabled", "disabled");
        //        }

        //        var spanLabelTag = new TagBuilder("span");
        //        spanLabelTag.InnerHtml += checkboxItem.Label;

        //        var labelControl = new TagBuilder("label");
        //        labelControl.InnerHtml += mainControlTag;
        //        labelControl.InnerHtml += spanLabelTag;

        //        var hiddenControlTag = new TagBuilder("input");
        //        hiddenControlTag.Attributes.Add("type", "hidden");
        //        hiddenControlTag.Attributes.Add("name", $"{name}[{index}]");
        //        hiddenControlTag.Attributes.Add("value", "0");

        //        if (checkBoxList.Layout == UiControlEnum.ControlLayoutType.Vertical)
        //        {
        //            checkBoxInnerDivTag.InnerHtml += labelControl;
        //            checkboxDivTag.InnerHtml += checkBoxInnerDivTag;
        //            checkboxDivTag.InnerHtml += hiddenControlTag;

        //            checkBoxInnerDivTag = new TagBuilder("div");
        //            checkBoxInnerDivTag.AddCssClass("checkbox");
        //        }
        //        else
        //        {
        //            checkBoxInnerDivTag.InnerHtml += labelControl;
        //            checkboxDivTag.InnerHtml += hiddenControlTag;
        //        }

        //        index++;
        //    }

        //    if (!string.IsNullOrEmpty(checkBoxInnerDivTag.InnerHtml))
        //    {
        //        checkboxDivTag.InnerHtml += checkBoxInnerDivTag;
        //    }

        //    formGroupDivTag.InnerHtml += checkboxDivTag;

        //    return MvcHtmlString.Create(formGroupDivTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString GenerateWritingOptionRadio(this HtmlHelper htmlHelper,
        //    string controlName,
        //    ICollection<WritingAssessmentOptionDto> writingAssessmentOptionDtos,
        //    int selectedId,
        //    bool isReadOnly)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("control");

        //    foreach (var writingAssessmentOptionDto in writingAssessmentOptionDtos)
        //    {
        //        var divRadioTag = new TagBuilder("div");
        //        divRadioTag.AddCssClass("radio");

        //        var labelTag = new TagBuilder("label");

        //        var inputTag = new TagBuilder("input");
        //        inputTag.Attributes.Add("type", "radio");
        //        inputTag.Attributes.Add("name", controlName);
        //        inputTag.Attributes.Add("class", controlName);
        //        inputTag.Attributes.Add("value", writingAssessmentOptionDto.Id.ToString());
        //        inputTag.Attributes.Add("data-score", writingAssessmentOptionDto.Score.ToString());

        //        if (isReadOnly)
        //        {
        //            inputTag.Attributes.Add("readonly", "readonly");
        //            inputTag.Attributes.Add("disabled", "disabled");
        //        }

        //        if (selectedId > 0)
        //        {
        //            if (selectedId == writingAssessmentOptionDto.Id)
        //            {
        //                inputTag.Attributes.Add("checked", "checked");
        //            }
        //        }

        //        var spanTag = new TagBuilder("span");
        //        spanTag.InnerHtml += writingAssessmentOptionDto.Name;

        //        labelTag.InnerHtml += inputTag.ToString();
        //        labelTag.InnerHtml += spanTag.ToString();
        //        divRadioTag.InnerHtml = labelTag.ToString();

        //        divTag.InnerHtml += divRadioTag.ToString();
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //// ReSharper disable once UnusedParameter.Global
        //public static MvcHtmlString GenerateSpeakingOptionRadio(this HtmlHelper htmlHelper,
        //    string controlName,
        //    ICollection<SpeakingAssessmentOptionDto> speakingAssessmentOptionDtos,
        //    int selectedId,
        //    bool isReadOnly)
        //{
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("control");

        //    foreach (var speakingAssessmentOptionDto in speakingAssessmentOptionDtos)
        //    {
        //        var divRadioTag = new TagBuilder("div");
        //        divRadioTag.AddCssClass("radio");

        //        var labelTag = new TagBuilder("label");

        //        var inputTag = new TagBuilder("input");
        //        inputTag.Attributes.Add("type", "radio");
        //        inputTag.Attributes.Add("name", controlName);
        //        inputTag.Attributes.Add("class", controlName);
        //        inputTag.Attributes.Add("value", speakingAssessmentOptionDto.Id.ToString());
        //        inputTag.Attributes.Add("data-score", speakingAssessmentOptionDto.Score.ToString());

        //        if (isReadOnly)
        //        {
        //            inputTag.Attributes.Add("readonly", "readonly");
        //            inputTag.Attributes.Add("disabled", "disabled");
        //        }

        //        if (selectedId > 0)
        //        {
        //            if (selectedId == speakingAssessmentOptionDto.Id)
        //            {
        //                inputTag.Attributes.Add("checked", "checked");
        //            }
        //        }

        //        var spanTag = new TagBuilder("span");
        //        spanTag.InnerHtml += speakingAssessmentOptionDto.Name;

        //        labelTag.InnerHtml += inputTag.ToString();
        //        labelTag.InnerHtml += spanTag.ToString();
        //        divRadioTag.InnerHtml = labelTag.ToString();

        //        divTag.InnerHtml += divRadioTag.ToString();
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //public static MvcHtmlString GenerateWhiteboardMessagePanel(WhiteboardMessageItem whiteboardMessageItem)
        //{
        //    var chatPanelCssClass = "message-out";
        //    var chatHeaderNameCssClass = "";
        //    var messageFrom = Resource.Whiteboard_StudentLabel;
        //    if (whiteboardMessageItem.FromType.Equals(CoreConstant.MessageFromType.TeacherFromType) ||
        //        whiteboardMessageItem.FromType.Equals(CoreConstant.MessageFromType.TeacherTopicFromType))
        //    {
        //        chatPanelCssClass = "message-in";
        //        chatHeaderNameCssClass = "is-teacher";
        //        messageFrom = Resource.Whiteboard_TeacherLabel;
        //    }

        //    var chatPanel = new TagBuilder("div");
        //    chatPanel.AddCssClass("chat-message " + chatPanelCssClass);

        //    // generate header panel
        //    var chatHeaderLabel = new TagBuilder("span");
        //    chatHeaderLabel.AddCssClass("chat-message__name");
        //    chatHeaderLabel.InnerHtml += whiteboardMessageItem.FromName;

        //    var chatHeaderNameLabel = new TagBuilder("span");
        //    chatHeaderNameLabel.AddCssClass("chat-message__status " + chatHeaderNameCssClass);
        //    chatHeaderNameLabel.InnerHtml += messageFrom;

        //    var chatHeaderNamePanel = new TagBuilder("div");
        //    chatHeaderNamePanel.AddCssClass("chat-message__meta");
        //    chatHeaderNamePanel.InnerHtml += chatHeaderNameLabel;

        //    var chatHeaderPanel = new TagBuilder("div");
        //    chatHeaderPanel.AddCssClass("chat-message__header");
        //    chatHeaderPanel.InnerHtml += chatHeaderLabel;
        //    chatHeaderPanel.InnerHtml += chatHeaderNamePanel;

        //    chatPanel.InnerHtml += chatHeaderPanel;

        //    // generate body panel
        //    var chatBodyPanel = new TagBuilder("div");
        //    chatBodyPanel.AddCssClass("chat-message__body");

        //    var timeIcon = new TagBuilder("i");
        //    timeIcon.AddCssClass("fa fa-clock-o fa-fw");

        //    var chatTimePanel = new TagBuilder("span");
        //    chatTimePanel.AddCssClass("chat-message__time-stamp");
        //    chatTimePanel.InnerHtml += timeIcon;
        //    chatTimePanel.InnerHtml +=
        //        whiteboardMessageItem.SentDate.ToString(CoreConstant.Setting.DefaultLongDateFormat);

        //    chatBodyPanel.InnerHtml += chatTimePanel;

        //    var chatMessagePanel = new TagBuilder("p");
        //    chatMessagePanel.InnerHtml += whiteboardMessageItem.Message;

        //    var chatBodyContentPanel = new TagBuilder("div");
        //    chatBodyContentPanel.AddCssClass("chat-message__content");
        //    chatBodyContentPanel.InnerHtml += chatMessagePanel;

        //    chatBodyPanel.InnerHtml += chatBodyContentPanel;
        //    chatPanel.InnerHtml += chatBodyPanel;

        //    if (!string.IsNullOrEmpty(whiteboardMessageItem.AttachmentUri))
        //    {
        //        var attachmentIcon = new TagBuilder("i");
        //        attachmentIcon.AddCssClass("fa fa-file-word-o fa-fw");

        //        var attachmentLink = new TagBuilder("a");
        //        attachmentLink.Attributes.Add("href", whiteboardMessageItem.AttachmentUri);
        //        attachmentLink.InnerHtml += attachmentIcon;
        //        attachmentLink.InnerHtml += whiteboardMessageItem.Attachment;

        //        var chatAttachmentLabel = new TagBuilder("span");
        //        chatAttachmentLabel.AddCssClass("label label-attachment");
        //        chatAttachmentLabel.InnerHtml += attachmentLink;

        //        var chatAttachmentPanel = new TagBuilder("div");
        //        chatAttachmentPanel.AddCssClass("chat-message__footer");
        //        chatAttachmentPanel.InnerHtml += chatAttachmentLabel;

        //        chatPanel.InnerHtml += chatAttachmentPanel;
        //    }

        //    var listPanel = new TagBuilder("li");
        //    listPanel.AddCssClass("chat-item clearfix");
        //    listPanel.InnerHtml += chatPanel;

        //    return MvcHtmlString.Create(listPanel.ToString());
        //}

        //#endregion

        //#region (private) Methods

        //private static MvcHtmlString E2ActivityDescription(string activityType,
        //    int activityId,
        //    bool isAlreadyTaken,
        //    DateTime lastTakenDateTime,
        //    string timeZoneId,
        //    DateTime firstTakenDateTime,
        //    int point,
        //    int score,
        //    bool isFreeActivity,
        //    bool isFreeStudentPackage,
        //    string groupName)
        //{
        //    var paragraphTag = new TagBuilder("p");
        //    paragraphTag.Attributes.Add("id", "descriptionPanel-" + activityId);
        //    paragraphTag.InnerHtml =
        //        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat";

        //    string lastTaken;

        //    if (isFreeStudentPackage && !isFreeActivity)
        //    {
        //        paragraphTag.InnerHtml = Resource.ExamPreparation_DefaultPaidActivityDescription;
        //        return MvcHtmlString.Create(paragraphTag.ToString());
        //    }

        //    switch (activityType)
        //    {
        //        case CoreConstant.PreparationActivityTypeCode.AzureVideo:
        //        case CoreConstant.PreparationActivityTypeCode.YoutubeVideo:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.Video_ViewedDescription, lastTaken)
        //                : Resource.Video_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DownloadPdf:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.Document_DownloadedDescription, lastTaken)
        //                : Resource.Document_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DownloadE2Pronounce:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.E2Pronounce_ClickedDescription, lastTaken)
        //                : Resource.E2Pronounce_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.Pronunciation:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.Pronounciation_ClickedDescription, lastTaken)
        //                : Resource.Pronounciation_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.SummarizeWritingAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.SummarizeWriting:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = score != 0
        //                ? string.Format(Resource.WritingAssessment_AssessmentInfo, score)
        //                : isAlreadyTaken
        //                    ? groupName == "Practice"
        //                        ? string.Format(Resource.SummarizeWriting_SubmittedPracticeDescription, lastTaken)
        //                        : string.Format(Resource.SummarizeWriting_SubmittedDescription, lastTaken)
        //                    : Resource.SummarizeWriting_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.WriteEssayAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.WriteEssay:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = score != 0
        //                ? string.Format(Resource.WritingAssessment_AssessmentInfo, score)
        //                : isAlreadyTaken
        //                    ? groupName == "Practice"
        //                        ? string.Format(Resource.WriteEssay_SubmittedPracticeDescription, lastTaken)
        //                        : string.Format(Resource.WriteEssay_SubmittedDescription, lastTaken)
        //                    : Resource.SummarizeWriting_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadAloudAssessment:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = score != 0
        //                ? string.Format(Resource.SpeakingAssessment_AssessmentInfo, score)
        //                : isAlreadyTaken
        //                    ? string.Format(Resource.ReadAloud_SubmittedDescription, lastTaken)
        //                    : Resource.ReadAloud_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadAloudPractice:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.ReadAloudPractice_SubmittedDescription, lastTaken)
        //                : Resource.ReadAloudPractice_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DescribeImagePractice:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.DescribeImagePractice_SubmittedDescription, lastTaken)
        //                : Resource.DescribeImagePractice_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReTellLectureAssessment:
        //        case CoreConstant.PreparationActivityTypeCode.ReTellLecture:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = score != 0
        //                ? string.Format(Resource.SpeakingAssessment_AssessmentInfo, score)
        //                : isAlreadyTaken
        //                    ? groupName == "Practice"
        //                        ? string.Format(Resource.ReTellLecture_SubmittedPracticeDescription, lastTaken)
        //                        : string.Format(Resource.ReTellLecture_SubmittedDescription, lastTaken)
        //                    : Resource.ReTellLecture_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.DescribeImageAssessment:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = score != 0
        //                ? string.Format(Resource.SpeakingAssessment_AssessmentInfo, score)
        //                : isAlreadyTaken
        //                    ? string.Format(Resource.DescribeImage_SubmittedDescription, lastTaken)
        //                    : Resource.DescribeImage_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.GrammarLesson:
        //            lastTaken =
        //                DateTimeManager.UtcToLocalDateTime(timeZoneId, lastTakenDateTime)
        //                    .ToString(CoreConstant.Setting.DefaultLongDateFormat);
        //            paragraphTag.InnerHtml = isAlreadyTaken
        //                ? string.Format(Resource.Grammar_SubmittedDateMessage, lastTaken, point, score)
        //                : Resource.Grammar_DefaultDescription;
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.AnswerShortQuestion:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.AnswerShortQuestion_DescriptionIsAnswered,
        //                Resource.AnswerShortQuestion_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.RepeatSentence:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.RepeatSentence_DescriptionIsAnswered,
        //                Resource.RepeatSentence_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.MultipleChoiceSingleAnswer:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.MultipleChoiceSingleAnswer_DescriptionIsAnswered,
        //                Resource.MultipleChoiceSingleAnswer_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.HighlightCorrectSummary:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.HighlightCorrectSummary_DefaultDescription,
        //                Resource.HighlightCorrectSummary_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.FillInTheBlank:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.FillInTheBlank_DescriptionIsAnswered,
        //                Resource.FillInTheBlank_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.WriteFromDictation:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.WriteFromDictation_DescriptionIsAnswered,
        //                Resource.WriteFromDictation_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.MultipleChoiceMultipleAnswer:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.MultipleChoiceMultipleAnswer_DescriptionIsAnswered,
        //                Resource.MultipleChoiceMultipleAnswer_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.OrderParagraph:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.OrderParagraph_DescriptionIsAnswered,
        //                Resource.OrderParagraph_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.ReadingWritingFillInTheBlank:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.ReadingWritingFillInTheBlank_DescriptionIsAnswered,
        //                Resource.ReadingWritingFillInTheBlank_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.VocabularyBuilder:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.VocabularyBuilder_DescriptionIsAnswered,
        //                Resource.VocabularyBuilder_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.HighlightIncorrect:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.HighlightIncorrect_DescriptionIfAnswered,
        //                Resource.HighlightIncorrect_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;

        //        case CoreConstant.PreparationActivityTypeCode.FillInTheBox:
        //            DescriptionForTest(paragraphTag,
        //                timeZoneId,
        //                firstTakenDateTime,
        //                lastTakenDateTime,
        //                point,
        //                score,
        //                Resource.FillInTheBox_DescriptionIsAnswered,
        //                Resource.FillInTheBox_DefaultDescription,
        //                isAlreadyTaken
        //                );
        //            break;
        //    }

        //    return MvcHtmlString.Create(paragraphTag.ToString());
        //}

        //private static string GetObjectSection(string content)
        //{
        //    var result = content;
        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);
        //    foreach (var regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString();
        //        var stringSections = stringOperation.TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);

        //        if (stringOperation.Contains("[PDF]"))
        //        {
        //            var pdfHtml = new StringBuilder();

        //            pdfHtml.AppendLine("<a id=\"pdfLink\" target=\"_blank\" href=\"" +
        //                               HttpContext.Current.Request.Url.Scheme + "://" +
        //                               HttpContext.Current.Request.Url.Authority + "/DownloadDocument/" +
        //                               stringSections[1] + "\">");
        //            pdfHtml.AppendLine("here");
        //            pdfHtml.AppendLine("</a>");
        //            result = result.Replace(stringOperation, pdfHtml.ToString());
        //            continue;
        //        }

        //        if (stringOperation.Contains("Transcript") || stringOperation.Contains("ShowHint") ||
        //            stringOperation.Contains("ShowAnswer"))
        //        {
        //            // prepare speaking file
        //            var additionalSection = GetAdditionalPanel(stringSections[0], stringSections[1]);
        //            result = result.Replace(stringOperation, additionalSection);
        //        }
        //    }

        //    return result;
        //}

        //private static MvcHtmlString E2ExamPreparationTabHeader(string moduleName,
        //    string tabHrefId,
        //    string selectedClass)
        //{
        //    var liTag = new TagBuilder("li");
        //    liTag.AddCssClass(selectedClass);

        //    var linkTag = new TagBuilder("a");
        //    linkTag.Attributes.Add("href", tabHrefId);
        //    linkTag.Attributes.Add("data-toggle", "tab");

        //    var iconTag = new TagBuilder("i");

        //    switch (moduleName)
        //    {
        //        case CoreConstant.PreparationModuleTypeCode.Speaking:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--speaking");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Writing:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--writing");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Reading:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--reading");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Listening:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--listening");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Grammar:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--grammar");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Vocabulary:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--vocabulary");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.EssayWriting:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--essay-writing");
        //            break;

        //        case CoreConstant.PreparationModuleTypeCode.Webinars:
        //            iconTag.AddCssClass("tabs-icon tabs-icon--webinars");
        //            break;
        //    }

        //    linkTag.InnerHtml += iconTag.ToString();
        //    linkTag.InnerHtml += moduleName;

        //    liTag.InnerHtml += linkTag;

        //    return MvcHtmlString.Create(liTag.ToString());
        //}

        //private static string GeneratePteGroupName(string groupActivityName)
        //{
        //    var result = groupActivityName;

        //    var bracketMatches = Regex.Matches(groupActivityName, BracketRegexPrefix);
        //    if (bracketMatches.Count == 0)
        //    {
        //        return groupActivityName;
        //    }

        //    foreach (var bracketMatch in bracketMatches)
        //    {
        //        result = result.Replace(bracketMatch.ToString(), string.Empty);
        //    }

        //    return result;
        //}

        //private static object[] GetQuestionComboBoxList(IEnumerable<int> groupIds,
        //    ICollection<ReadingWritingFillInTheBlankAnswerDto> answerDtos,
        //    int questionId)
        //{
        //    var comboBoxTextList = new List<object>();
        //    foreach (var groupId in groupIds)
        //    {
        //        var key = "readingWritingFillInTheBlank_" + questionId + "_" + groupId;
        //        var optionList = GenerateAnswerOptionList(answerDtos, groupId);

        //        var replacementSelect = new TagBuilder("select");
        //        replacementSelect.Attributes.Add("name", key);
        //        replacementSelect.Attributes.Add("id", key);
        //        replacementSelect.AddCssClass("input-normal form-control");
        //        replacementSelect.Attributes.Add("required", "required");
        //        replacementSelect.Attributes.Add("placeholder", "This field is required");
        //        replacementSelect.InnerHtml += optionList;

        //        comboBoxTextList.Add(replacementSelect.ToString());
        //    }

        //    return comboBoxTextList.ToArray();
        //}

        //private static string GenerateAnswerOptionList(ICollection<ReadingWritingFillInTheBlankAnswerDto> answerDtos,
        //    int groupId)
        //{
        //    var result = string.Empty;
        //    var selectedWritingFillInTheBlankAnswers = answerDtos.Where(item => item.GroupId == groupId).ToList();
        //    foreach (var readingWritingFillInTheBlankAnswerDto in selectedWritingFillInTheBlankAnswers)
        //    {
        //        var selectTag = new TagBuilder("option");
        //        selectTag.Attributes.Add("value", readingWritingFillInTheBlankAnswerDto.Id.ToString());
        //        selectTag.InnerHtml = readingWritingFillInTheBlankAnswerDto.AnswerText;
        //        result += selectTag.ToString();
        //    }

        //    return result;
        //}

        //private static MvcHtmlString E2ComboBoxList(string name, SelectList checkboxList)
        //{
        //    var stringBuilder = new StringBuilder();
        //    var divTag = new TagBuilder("div");
        //    divTag.AddCssClass("m-t-20");

        //    foreach (var checkbox in checkboxList)
        //    {
        //        var key = name + "_" + checkbox.Value;
        //        var checkboxDivTag = new TagBuilder("div");
        //        checkboxDivTag.Attributes.Add("id", "paragraph-" + checkbox.Value);
        //        checkboxDivTag.AddCssClass("checkbox");

        //        var labelTag = new TagBuilder("label");
        //        labelTag.Attributes.Add("for", key);
        //        labelTag.InnerHtml += checkbox.Text;

        //        var checkboxTag = new TagBuilder("input");
        //        checkboxTag.Attributes.Add("type", "checkbox");
        //        checkboxTag.Attributes.Add("id", key);
        //        checkboxTag.Attributes.Add("name", key);
        //        checkboxTag.Attributes.Add("value", checkbox.Value);

        //        var spanDiv = new TagBuilder("span")
        //        {
        //            InnerHtml = checkbox.Text
        //        };

        //        labelTag.InnerHtml = checkboxTag + spanDiv.ToString();
        //        checkboxDivTag.InnerHtml = labelTag.ToString();
        //        stringBuilder.Append(checkboxDivTag);
        //    }
        //    divTag.InnerHtml += stringBuilder.ToString();
        //    return MvcHtmlString.Create(divTag.ToString());
        //}

        //private static void DescriptionForTest(TagBuilder paragraphTag,
        //    string timeZoneId,
        //    DateTime firstTakenDateTime,
        //    DateTime lastTakenDateTime,
        //    int point,
        //    int score,
        //    string takenDescription,
        //    string defaultDescription,
        //    bool isAlreadyTaken)
        //{
        //    var firstAnsweredMessage = ScoreMessageManager.GetFirstTimeAnsweredMessage(firstTakenDateTime,
        //        point,
        //        timeZoneId);
        //    var lastAnsweredMessage = ScoreMessageManager.GetLastTimeAnsweredMessage(lastTakenDateTime,
        //        score,
        //        timeZoneId);

        //    var breakTag = new TagBuilder("br");
        //    if (isAlreadyTaken)
        //    {
        //        paragraphTag.InnerHtml = takenDescription;
        //        paragraphTag.InnerHtml += breakTag;
        //        paragraphTag.InnerHtml += firstAnsweredMessage;
        //        paragraphTag.InnerHtml += breakTag;
        //        paragraphTag.InnerHtml += lastAnsweredMessage;
        //    }
        //    else
        //    {
        //        paragraphTag.InnerHtml = defaultDescription;
        //    }
        //}

        private static TagBuilder GetFormGroupDivTag()
        {
            var tag = new TagBuilder("div");
            tag.AddCssClass("form-group");
            return tag;
        }

        //private static string GenerateUrl(string function, string controller, object routeValues = null)
        //{
        //    var requestContext = HttpContext.Current.Request.RequestContext;
        //    return new UrlHelper(requestContext).Action(function, controller, routeValues);
        //}

        private static TagBuilder GetLabelTag(string name, string displayName, bool isRequired)
        {
            var tag = new TagBuilder("label");
            tag.AddCssClass("col-sm-2 control-label");
            tag.Attributes.Add("for", name);

            if (isRequired)
            {
                tag.InnerHtml += displayName + " *";
            }
            else
            {
                tag.InnerHtml += displayName;
            }

            return tag;
        }

        private static TagBuilder GetMainControlDivTag(UiControlEnum.ControlWidthOption widthOption,
            string additionalCssClass = "")
        {
            var mainControlDivTag = new TagBuilder("div");

            switch (widthOption)
            {
                case UiControlEnum.ControlWidthOption.ExtraShort:
                    mainControlDivTag.AddCssClass("col-sm-2");
                    break;

                case UiControlEnum.ControlWidthOption.Short:
                    mainControlDivTag.AddCssClass("col-sm-4");
                    break;

                case UiControlEnum.ControlWidthOption.Medium:
                    mainControlDivTag.AddCssClass("col-sm-6");
                    break;

                case UiControlEnum.ControlWidthOption.ExtraLong:
                    mainControlDivTag.AddCssClass("col-sm-10");
                    break;

                case UiControlEnum.ControlWidthOption.Full:
                    mainControlDivTag.AddCssClass("col-sm-12");
                    break;

                default:
                    mainControlDivTag.AddCssClass("col-sm-8");
                    break;
            }

            if (!string.IsNullOrEmpty(additionalCssClass))
            {
                mainControlDivTag.AddCssClass(additionalCssClass);
            }

            return mainControlDivTag;
        }

        //private static string GetFillInTheBoxContent(string content)
        //{
        //    var result = content;

        //    var regexStrings = Regex.Matches(content, BracketRegexPrefix);
        //    foreach (var regexString in regexStrings)
        //    {
        //        var stringOperation = regexString.ToString();
        //        var stringSections = stringOperation.TrimStart('{')
        //            .TrimEnd('}')
        //            .Split(new[] {"__"}, StringSplitOptions.RemoveEmptyEntries);

        //        if (stringOperation.Contains("IMG"))
        //        {
        //            var imageTagHtml = new StringBuilder();

        //            imageTagHtml.AppendLine("<img class='img-responsive' src='" +
        //                                    ConfigurationManager.AppSettings["cdnImageUrl"] + stringSections[1] + "'/>");
        //            result = result.Replace(stringOperation, imageTagHtml.ToString());
        //        }
        //    }

        //    return result;
        //}

        //private static MvcHtmlString E2YoutubePlayer(string youtubeVideoId)
        //{
        //    var youtubeIframeTag = new TagBuilder("iframe");
        //    youtubeIframeTag.Attributes.Add("id", "youtube-player");
        //    youtubeIframeTag.Attributes.Add("type", "text/html");
        //    youtubeIframeTag.Attributes.Add("width", "640");
        //    youtubeIframeTag.Attributes.Add("height", "390");
        //    youtubeIframeTag.Attributes.Add("frameborder", "0");
        //    youtubeIframeTag.Attributes.Add("allowfullscreen", "true");

        //    var videoSource = string.Format(YoutubeEmbedUrlFormat, youtubeVideoId);
        //    youtubeIframeTag.Attributes.Add("src", videoSource);

        //    var youtubeDivTag = new TagBuilder("div");
        //    youtubeDivTag.Attributes.Add("class", "video-container");
        //    youtubeDivTag.InnerHtml += youtubeIframeTag;

        //    return MvcHtmlString.Create(youtubeDivTag.ToString());
        //}

        //private static MvcHtmlString GenerateSpeakingWidget(this HtmlHelper htmlHelper,
        //    string sectionName,
        //    string audioUri,
        //    string audioName)
        //{
        //    var result = string.Empty;

        //    result += E2RecorderWidget(htmlHelper, string.Empty, sectionName);
        //    result += E2UploadAudioWidget(htmlHelper, string.Empty, sectionName);
        //    if (!string.IsNullOrEmpty(audioName) && !string.IsNullOrEmpty(audioUri))
        //    {
        //        result += E2AudioPanel(htmlHelper, audioUri, audioName, string.Empty);
        //    }

        //    return MvcHtmlString.Create(result);
        //}

        //private static MvcHtmlString E2MultipleChoiceSingleAnswerQuestionDropdownList(string name,
        //    SelectList optionList,
        //    object value,
        //    bool isRequired,
        //    bool isReadOnly,
        //    string additionalCssClass)
        //{
        //    // The select tag
        //    var selectTag = new TagBuilder("select");
        //    selectTag.Attributes.Add("id", name);
        //    selectTag.Attributes.Add("name", name);
        //    selectTag.Attributes.Add("class", additionalCssClass);

        //    if (isRequired)
        //    {
        //        selectTag.Attributes.Add("required", "required");
        //    }

        //    if (isReadOnly)
        //    {
        //        selectTag.Attributes.Add("disabled", "disabled");
        //    }

        //    // The option tags
        //    foreach (var item in optionList)
        //    {
        //        var optionTag = new TagBuilder("option");
        //        optionTag.Attributes.Add("value", item.Value);

        //        if (value != null)
        //        {
        //            if (item.Value == value.ToString())
        //            {
        //                optionTag.Attributes.Add("selected", "selected");
        //            }
        //        }

        //        optionTag.InnerHtml += item.Text;
        //        selectTag.InnerHtml += optionTag;
        //    }

        //    return MvcHtmlString.Create(selectTag.ToString());
        //}

        //private static bool IsOnPaidThreshold(string packageCode, int activityPaidThreshold)
        //{
        //    var codeSection = packageCode.Split('_');
        //    if (codeSection.Length == 2)
        //    {
        //        return false;
        //    }

        //    int currentPaidPackageLevel;
        //    if (!int.TryParse(codeSection[2], out currentPaidPackageLevel))
        //    {
        //        return false;
        //    }

        //    return currentPaidPackageLevel >= activityPaidThreshold;
        //}

        //private static MvcHtmlString GenerateDroppableSection(string answerKey)
        //{
        //    var mainDivControl = new TagBuilder("div");
        //    mainDivControl.AddCssClass("dragdrop dragdrop-section droppable");
        //    mainDivControl.Attributes.Add("data-sentenceKey", answerKey);
        //    mainDivControl.Attributes.Add("data-answerId", "");

        //    return MvcHtmlString.Create(mainDivControl.ToString());
        //}

        //private static MvcHtmlString GenerateDraggableSection(IEnumerable<AnswerItem> paragraphs, string answerKey)
        //{
        //    var htmlString = new StringBuilder();
        //    var selectedParagraphs =
        //        paragraphs.Where(item => item.AnswerKey == answerKey).OrderBy(item => item.SortingIndex);
        //    foreach (var paragraph in selectedParagraphs)
        //    {
        //        var mainDivControl = new TagBuilder("div");
        //        mainDivControl.AddCssClass("dragdrop dragdrop-section draggable");
        //        mainDivControl.Attributes.Add("data-answerId", paragraph.Id.ToString(CultureInfo.InvariantCulture));
        //        mainDivControl.InnerHtml += paragraph.AnswerText;

        //        htmlString.AppendLine(mainDivControl.ToString());
        //    }

        //    return MvcHtmlString.Create(htmlString.ToString());
        //}

        //private static MvcHtmlString GenerateOrderParagraphGrid(
        //    IEnumerable<AnswerItem> answerList,
        //    string answerKey)
        //{
        //    var mainDivControl = new TagBuilder("div");
        //    mainDivControl.AddCssClass("gridster");

        //    var ulTag = new TagBuilder("ul");
        //    var selectedAnswerList =
        //        answerList.Where(item => item.AnswerKey == answerKey).OrderBy(item => item.SortingIndex);
        //    foreach (var answerItem in selectedAnswerList)
        //    {
        //        var liTag = new TagBuilder("li");
        //        liTag.Attributes.Add("data-row", answerItem.SortingIndex.ToString());
        //        liTag.Attributes.Add("data-sizex", "1");
        //        liTag.Attributes.Add("data-sizey", "1");
        //        liTag.Attributes.Add("data-col", "1");
        //        liTag.Attributes.Add("data-answerid", answerItem.Id.ToString());
        //        liTag.AddCssClass("gs-w");
        //        liTag.Attributes.Add("id", "paragraph_" + answerItem.Id);
        //        liTag.InnerHtml = answerItem.AnswerText;

        //        ulTag.InnerHtml += liTag.ToString();
        //    }

        //    mainDivControl.InnerHtml += ulTag.ToString();

        //    return MvcHtmlString.Create(mainDivControl.ToString());
        //}

        //private static string GenerateAudioPlayer(string audioName)
        //{
        //    var audioUri = AudioManager.GetCdnAudioUri(audioName);

        //    var audioPlayerTag = new TagBuilder("audio");
        //    audioPlayerTag.Attributes.Add("controls", "controls");
        //    audioPlayerTag.Attributes.Add("src", audioUri);
        //    audioPlayerTag.InnerHtml += Resource.General_AudioNotSupportedMessage;

        //    var divAudioTag = new TagBuilder("div");
        //    divAudioTag.Attributes.Add("id", "audioPlayerControl");
        //    divAudioTag.AddCssClass("col-xs-6");
        //    divAudioTag.InnerHtml += audioPlayerTag;

        //    return divAudioTag.ToString();
        //}

        //#endregion
    }
}