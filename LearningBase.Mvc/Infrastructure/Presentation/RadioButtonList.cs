﻿using System.Collections.Generic;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class RadioButtonList
    {
        #region Fields

        private List<RadioButtonItem> _items;

        private UiControlEnum.ControlLayoutType _layout = UiControlEnum.ControlLayoutType.Horizontal;

        #endregion


        #region Properties

        public UiControlEnum.ControlLayoutType Layout
        {
            set { this._layout = value; }
            get { return this._layout; }
        }

        public ICollection<RadioButtonItem> Items
        {
            get { return this._items ?? (this._items = new List<RadioButtonItem>()); }
        }

        #endregion
    }
}