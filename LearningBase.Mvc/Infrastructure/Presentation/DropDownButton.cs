﻿using System.Collections.Generic;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class DropDownButton
    {
        #region Fields

        private List<DropDownButtonItem> _items;

        #endregion


        #region Properties

        public ICollection<DropDownButtonItem> Items
        {
            get { return this._items ?? (this._items = new List<DropDownButtonItem>()); }
        }

        #endregion
    }
}