﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class DropDownButtonItem
    {
        #region Properties

        public string Id { get; set; }

        public string Label { get; set; }

        public bool IsSelected { get; set; }

        #endregion
    }
}