﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class CheckBoxItem
    {
        #region Properties

        public string Label { get; set; }

        public string Value { get; set; }

        public bool IsChecked { get; set; }

        #endregion
    }
}