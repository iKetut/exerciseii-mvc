﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class RadioButtonWithCheckBoxItem : RadioButtonItem
    {
        #region Properties

        public CheckBoxList CheckBoxList { get; set; }

        #endregion
    }
}