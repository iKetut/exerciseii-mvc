﻿using System.Web.Mvc;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class AnswerDropDownItem
    {
        #region Properties

        public SelectList AnswerList { get; set; }

        public string Id { get; set; }

        public string AdditionalCssClass { get; set; }

        public object AnswerValue { get; set; }

        public bool IsRequired { get; set; }

        public bool IsDisabled { get; set; }

        #endregion
    }
}