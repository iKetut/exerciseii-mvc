﻿using System.Collections.Generic;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class RadioButtonWithCheckBoxList
    {
        #region Fields

        private List<RadioButtonWithCheckBoxItem> _items;

        #endregion


        #region Properties

        public ICollection<RadioButtonWithCheckBoxItem> Items
        {
            get { return this._items ?? (this._items = new List<RadioButtonWithCheckBoxItem>()); }
        }

        #endregion
    }
}