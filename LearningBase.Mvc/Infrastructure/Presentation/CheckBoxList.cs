﻿using System.Collections.Generic;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class CheckBoxList
    {
        #region Fields

        private List<CheckBoxItem> _items;

        private UiControlEnum.ControlLayoutType _layout = UiControlEnum.ControlLayoutType.Horizontal;

        #endregion


        #region Properties

        public UiControlEnum.ControlLayoutType Layout
        {
            set { this._layout = value; }
            get { return this._layout; }
        }

        public ICollection<CheckBoxItem> Items
        {
            get { return this._items ?? (this._items = new List<CheckBoxItem>()); }
        }

        #endregion
    }
}