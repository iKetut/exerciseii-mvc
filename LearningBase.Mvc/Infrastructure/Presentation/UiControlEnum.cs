﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public static class UiControlEnum
    {
        #region ControlLayoutType enum

        public enum ControlLayoutType
        {
            Horizontal,
            Vertical
        }

        #endregion

        #region ControlSizeOption enum

        public enum ControlSizeOption
        {
            Small,
            Medium,
            Large
        }

        #endregion

        #region ControlWidthOption enum

        public enum ControlWidthOption
        {
            ExtraShort,
            Short,
            Medium,
            Long,
            ExtraLong,
            Full
        }

        #endregion
    }
}