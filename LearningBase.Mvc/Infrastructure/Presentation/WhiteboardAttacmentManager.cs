﻿using System.Configuration;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class WhiteboardAttacmentManager
    {
        #region Fields

        private static WhiteboardAttacmentManager _instance;

        #endregion

        #region Properties

        public static WhiteboardAttacmentManager Instance
        {
            get { return _instance ?? (_instance = new WhiteboardAttacmentManager()); }
        }

        #endregion

        #region Public Methods

        public string GetCdnWhiteboardUri(string audioName)
        {
            var cdnAudioRootUri = ConfigurationManager.AppSettings["cdnWhiteboardAttachmentUrl"];
            return cdnAudioRootUri + audioName;
        }

        #endregion
    }
}