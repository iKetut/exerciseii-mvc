﻿using System.Configuration;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public static class AudioManager
    {
        #region Public Methods

        public static string GetCdnAudioUri(string audioName)
        {
            var cdnAudioRootUri = ConfigurationManager.AppSettings["cdnAudioUrl"];
            if (audioName.Contains(".mp3"))
            {
                return cdnAudioRootUri + audioName;
            }

            return cdnAudioRootUri + audioName + ".mp3";
        }

        public static  string GetCdnRecordUri(string audioName)
        {
            var cdnAudioRootUri = ConfigurationManager.AppSettings["cdnRecordingUrl"];
            return cdnAudioRootUri + audioName;
        }

        #endregion
    }
}