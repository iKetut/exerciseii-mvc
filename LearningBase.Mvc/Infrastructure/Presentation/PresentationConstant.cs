﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public static class PresentationConstant
    {
        #region Nested type: ButtonType

        public static class ButtonType
        {
            #region Constants

            public const string Submit = "submit";
            public const string Button = "button";

            #endregion
        }

        #endregion
    }
}