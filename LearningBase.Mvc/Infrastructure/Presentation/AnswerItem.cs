﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class AnswerItem
    {
        #region Properties

        public int Id { get; set; }

        public string AnswerText { get; set; }

        public string AnswerKey { get; set; }

        public int SortingIndex { get; set; }
        
        #endregion
    }
}