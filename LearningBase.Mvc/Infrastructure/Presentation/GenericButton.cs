﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class GenericButton
    {
        #region Properties

        public string Id { get; set; }

        public string Label { get; set; }

        public string Type { get; set; }

        public bool IsPrimary { get; set; }

        #endregion
    }
}