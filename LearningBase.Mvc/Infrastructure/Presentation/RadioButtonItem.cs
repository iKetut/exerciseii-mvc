﻿namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class RadioButtonItem
    {
        #region Properties

        public string Label { get; set; }

        public string Value { get; set; }

        public bool IsDefault { get; set; }

        public string LabelCssClass { get; set; }

        #endregion
    }
}