﻿using System.Configuration;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class ImageManager
    {
        #region Public Methods

        public static string GetCdnImageUri(string imageName)
        {
            var cdnImageRootUri = ConfigurationManager.AppSettings["cdnImageUrl"];
            return cdnImageRootUri + imageName;
        }

        #endregion
    }
}