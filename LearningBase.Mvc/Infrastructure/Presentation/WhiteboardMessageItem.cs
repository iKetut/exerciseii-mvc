﻿using System;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class WhiteboardMessageItem
    {
        #region Properties

        public string Message { get; set; }

        public string FromName { get; set; }

        public string FromType { get; set; }

        public DateTime SentDate { get; set; }

        public string Attachment { get; set; }

        public string AttachmentUri { get; set; }
        
        #endregion
    }
}