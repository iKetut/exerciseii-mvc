﻿using System.Collections.Generic;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class MaintenanceButtonRow
    {
        #region Preset enum

        public enum Preset
        {
            CreateCancel,
            SaveDeleteCancel,
            SubmitClose,
            Save,
            SaveCancel,
            SaveClose,
            Cancel,
            Close,
            SignUpGoToPaymentCancel
        }

        #endregion

        #region Fields

        private List<GenericButton> _items;

        #endregion

        #region Constructors

        public MaintenanceButtonRow(string label)
        {
            this.Items.Add(GetCustomLabelButton(label));
        }

        public MaintenanceButtonRow(Preset preset)
        {
            switch (preset)
            {
                case Preset.CreateCancel:
                    this.Items.Add(GetCreateButton());
                    this.Items.Add(GetCancelButton());
                    break;

                case Preset.SaveDeleteCancel:
                    this.Items.Add(GetSaveButton());
                    this.Items.Add(GetDeleteButton());
                    this.Items.Add(GetCancelButton());
                    break;

                case Preset.SubmitClose:
                    this.Items.Add(GetSubmitButton());
                    this.Items.Add(GetCloseButton());
                    break;

                case Preset.SaveCancel:
                    this.Items.Add(GetSaveButton());
                    this.Items.Add(GetCancelButton());
                    break;

                case Preset.SaveClose:
                    this.Items.Add(GetSaveButton());
                    this.Items.Add(GetCloseButton());
                    break;

                case Preset.Save:
                    this.Items.Add(GetSaveButton());
                    break;

                case Preset.Cancel:
                    this.Items.Add(GetCancelButton());
                    break;

                case Preset.Close:
                    this.Items.Add(GetCloseButton());
                    break;

                case Preset.SignUpGoToPaymentCancel:
                    this.Items.Add(GetSignUpButton());
                    this.Items.Add(GetGoToPaymentButton());
                    this.Items.Add(GetCancelButton());
                    break;
            }
        }

        #endregion

        #region Properties

        public ICollection<GenericButton> Items
        {
            get { return this._items ?? (this._items = new List<GenericButton>()); }
        }

        #endregion

        #region (private) Methods

        private static GenericButton GetCreateButton()
        {
            return new GenericButton
            {
                Id = "createButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Create,
                IsPrimary = true
            };
        }

        private static GenericButton GetSaveButton()
        {
            return new GenericButton
            {
                Id = "saveButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Save,
                IsPrimary = true
            };
        }

        private static GenericButton GetSubmitButton()
        {
            return new GenericButton
            {
                Id = "submitButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Submit,
                IsPrimary = true
            };
        }

        private static GenericButton GetDeleteButton()
        {
            return new GenericButton
            {
                Id = "deleteButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Delete,
                IsPrimary = true
            };
        }

        private static GenericButton GetCancelButton()
        {
            return new GenericButton
            {
                Id = "cancelButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Cancel
            };
        }

        private static GenericButton GetCloseButton()
        {
            return new GenericButton
            {
                Id = "closeButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_Close
            };
        }

        private static GenericButton GetSignUpButton()
        {
            return new GenericButton
            {
                Id = "signUpButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_SignUp
            };
        }

        private static GenericButton GetGoToPaymentButton()
        {
            return new GenericButton
            {
                Id = "goToPaymentButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = Resource.General_GoToPayment
            };
        }

        private static GenericButton GetCustomLabelButton(string label)
        {
            return new GenericButton
            {
                Id = "customButton",
                Type = PresentationConstant.ButtonType.Button,
                Label = label
            };
        }

        #endregion
    }
}