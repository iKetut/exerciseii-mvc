﻿using System.Web.Mvc;

namespace LearningBase.Mvc.Infrastructure.Presentation
{
    public class AnswerComboBoxItem
    {
        #region Properties

        public SelectList AnswerList { get; set; }

        public string Id { get; set; }

        public string AdditionalCssClass { get; set; }

        public object AnswerValue { get; set; }

        public bool IsSelected { get; set; }

        public bool IsRequired { get; set; }

        #endregion
    }
}