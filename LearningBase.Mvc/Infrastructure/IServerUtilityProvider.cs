﻿namespace LearningBase.Mvc.Infrastructure
{
    public interface IServerUtilityProvider
    {
        #region (public) Methods

        string MapPath(string path);

        #endregion
    }
}