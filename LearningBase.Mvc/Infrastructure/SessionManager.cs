﻿using System;
using System.Web;
using System.Web.Mvc;
using LearningBase.ServiceContract.Core;

namespace LearningBase.Mvc.Infrastructure
{
    public class SessionManager
    {
        #region Constants

        private const string SystemConfigurationSessionName = "SystemConfiguration";

        #endregion

        #region Fields

        private static readonly Lazy<SessionManager> Lazy =
            new Lazy<SessionManager>(() => new SessionManager());

        private IAccountService _accountService;

        #endregion

        #region Properties

        public static SessionManager Instance
        {
            get { return Lazy.Value; }
        }

        #endregion

        #region Public Methods

        public void Initialize()
        {
            this._accountService = DependencyResolver.Current.GetService<IAccountService>();
        }

        public int GetUserId(HttpSessionStateBase session)
        {
            this.PrepareSession(session);
            return ((ISystemConfiguration) session[SystemConfigurationSessionName]).UserId;
        }

        public string GetUserName(HttpSessionStateBase session)
        {
            this.PrepareSession(session);
            return ((ISystemConfiguration) session[SystemConfigurationSessionName]).UserName;
        }

        public int GetEntityId(HttpSessionStateBase session)
        {
            this.PrepareSession(session);
            return ((ISystemConfiguration)session[SystemConfigurationSessionName]).EntityId;
        }

        public void Clear(HttpSessionStateBase session)
        {
            session[SystemConfigurationSessionName] = null;
        }

        public void Refresh(HttpSessionStateBase session)
        {
            this.Initialize();
            ((ISystemConfiguration) session[SystemConfigurationSessionName])?.Refresh(this._accountService);
        }

        #endregion

        #region Private Methods

        private void PrepareSession(HttpSessionStateBase session)
        {
            if (session[SystemConfigurationSessionName] != null)
            {
                return;
            }

            var systemConfiguration = new SystemConfiguration(this._accountService);
            systemConfiguration.Refresh();
            session[SystemConfigurationSessionName] = systemConfiguration;
        }

        #endregion
    }
}