﻿using LearningBase.ServiceContract.Core;
using WebMatrix.WebData;

namespace LearningBase.Mvc.Infrastructure
{
    public class SystemConfiguration : ISystemConfiguration
    {
        #region Fields

        private IAccountService _accountService;

        private int _userId;
        private string _userName;
        private int _entityId;

        #endregion

        #region Constructors

        public SystemConfiguration(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        #endregion

        #region ISystemConfiguration Members

        public int UserId
        {
            get
            {
                if (WebSecurity.IsAuthenticated && this._userId == 0)
                {
                    this.Refresh();
                }

                return this._userId;
            }

            set { this._userId = value; }
        }

        public string UserName
        {
            get
            {
                if (WebSecurity.IsAuthenticated && string.IsNullOrEmpty(this._userName))
                {
                    this.Refresh();
                }

                return this._userName;
            }

            set { this._userName = value; }
        }

        public int EntityId
        {
            get
            {
                if (WebSecurity.IsAuthenticated && this._entityId == 0)
                {
                    this.Refresh();
                }

                return this._entityId;
            }

            set { this._entityId = value; }
        }

        public void Refresh(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        #endregion

        #region Public Methods

        public void Refresh()
        {
            // Populate the system configuration
            var currentLoginInfoResponse = this._accountService.GetCurrentLoginInformation();

            this.UserId = currentLoginInfoResponse.UserId;
            this.UserName = currentLoginInfoResponse.UserName;
            this.EntityId = currentLoginInfoResponse.EntityId;
        }

        #endregion
    }
}