﻿using System.Web;

namespace LearningBase.Mvc.Infrastructure
{
    public class ServerUtilityProvider : IServerUtilityProvider
    {
        #region IServerUtilityProvider Members

        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }

        #endregion
    }
}