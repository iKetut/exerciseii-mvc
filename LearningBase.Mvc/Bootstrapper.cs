﻿using System.Web.Mvc;
using LearningBase.Mvc.Infrastructure;
using LearningBase.Repository;
using LearningBase.Repository.Common;
using LearningBase.Repository.Course;
using LearningBase.Repository.Utility;
using LearningBase.RepositoryContract;
using LearningBase.RepositoryContract.Common;
using LearningBase.RepositoryContract.Course;
using LearningBase.RepositoryContract.Utility;
using LearningBase.Service;
using LearningBase.Service.Common;
using LearningBase.Service.Core;
using LearningBase.Service.Course;
using LearningBase.ServiceContract.Common;
using LearningBase.ServiceContract.Core;
using LearningBase.ServiceContract.Course;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using ISecurityProvider = LearningBase.ServiceContract.ISecurityProvider;

namespace LearningBase.Mvc
{
    public static class Bootstrapper
    {
        #region Public Methods

        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
        }

        #endregion

        #region Private Methods

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //---------------------------------------------------------------------
            // REPOSITORY
            //---------------------------------------------------------------------
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IUserProfileRepository, UserProfileRepository>();
            container.RegisterType<IUserLoginActivityRepository, UserLoginActivityRepository>();
            container.RegisterType<IStudentRepository, StudentRepository>();
            container.RegisterType<ICourseRepository, CourseRepository>();
            container.RegisterType<IModuleRepository, ModuleRepository>();

            //---------------------------------------------------------------------
            // SERVICE
            //---------------------------------------------------------------------
            container.RegisterType<ISecurityProvider, SecurityProvider>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IStudentService, StudentService>();
            container.RegisterType<ICourseService, CourseService>();
            container.RegisterType<IModuleService, ModuleService>();

            container.RegisterType<IServerUtilityProvider, ServerUtilityProvider>();

            RegisterTypes(container);

            return container;
        }

        #endregion
    }
}