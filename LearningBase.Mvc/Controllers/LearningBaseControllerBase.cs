﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using LearningBase.ServiceContract.Core.Request;
using LearningBase.ServiceContract.Core.Response;
using LearningBase.Dto;
using LearningBase.Mvc.Infrastructure;
using LearningBase.Mvc.ViewModel;

namespace LearningBase.Mvc.Controllers
{
    public class LearningBaseControllerBase : Controller
    {
        #region Fields

        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        protected LearningBaseControllerBase(IServerUtilityProvider serverUtilityProvider)
        {
            this.ServerUtilityProvider = serverUtilityProvider;

            var config =
                new MapperConfiguration(cfg => cfg.CreateMap<AuditableDto, AuditableViewModelBase>().ReverseMap());
            this._mapper = config.CreateMapper();
        }

        #endregion

        #region Properties

        protected IServerUtilityProvider ServerUtilityProvider { get; }

        #endregion

        #region Protected Methods

        protected IEnumerable<string> GetModelStateError()
        {
            return this.ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage).ToList();
        }

        protected JsonResult GetBasicSuccessJson()
        {
            return this.Json(new {IsSuccess = true});
        }

        protected JsonResult GetBasicSuccessJson(BasicResponse response, object value)
        {
            return this.Json(new
            {
                IsSuccess = true,
                MessageInfoTextArray = response.GetMessageInfoTextArray(),
                Value = value
            });
        }

        protected JsonResult GetErrorJson(string[] messages)
        {
            return this.Json(new
            {
                IsSuccess = false,
                MessageTextArray = messages
            });
        }

        protected JsonResult GetErrorJson(BasicResponse response)
        {
            return this.Json(new
            {
                IsSuccess = false,
                MessageTextArray = response.GetMessageTextArray(),
                MessageErrorTextArray = response.GetMessageErrorTextArray(),
                MessageInfoTextArray = response.GetMessageInfoTextArray()
            });
        }

        protected void PopulateAuditFieldValue(AuditableDto dto, AuditableViewModelBase model)
        {
            this._mapper.Map(dto, model);
        }

        protected void PopulateAuditFieldValue(AuditableViewModelBase model, AuditableDto dto)
        {
            this._mapper.Map(model, dto);
        }

        protected ActionResult GetPagedSearchGridJson<TDto>(FormCollection formCollection,
            Func<PagedSearchRequest, GenericPagedSearchResponse<TDto>> searchMethod,
            Func<ICollection<TDto>, List<object>> dtoCollectionToRowJsonMethod)
        {
            int pageSize;
            string sortColumnName;
            string sortOrder;
            string keyword;
            int pageIndex;

            this.ReadPageSearchParameter(formCollection,
                out pageSize,
                out sortColumnName,
                out sortOrder,
                out keyword,
                out pageIndex);

            var request = GetPagedSearchRequest(pageIndex, pageSize, sortColumnName, sortOrder, keyword);
            var response = searchMethod(request);
            var rowJsonData = dtoCollectionToRowJsonMethod(response.DtoCollection);

            return this.GetPagedSearchGridJson(pageIndex, pageSize, rowJsonData, response);
        }

        protected void ReadPageSearchParameter(FormCollection formCollection,
            out int pageSize,
            out string sortColumnName,
            out string sortOrder,
            out string keyword,
            out int pageIndex)
        {
            pageIndex = 0;
            if (formCollection.AllKeys.Contains("current"))
            {
                int.TryParse(formCollection["current"], out pageIndex);
            }

            pageSize = 0;
            if (formCollection.AllKeys.Contains("rowCount"))
            {
                int.TryParse(formCollection["rowCount"], out pageSize);
            }

            var formKey = formCollection.AllKeys.SingleOrDefault(item => item.Contains("sort"));
            if (formKey != null)
            {
                sortColumnName = formKey.Replace("sort", "").Replace("[", "").Replace("]", "");
                sortOrder = formCollection[formKey];
            }
            else
            {
                SetDefaultSorting(out sortColumnName, out sortOrder);
            }

            keyword = string.Empty;
            if (formCollection.AllKeys.Contains("searchPhrase"))
            {
                keyword = formCollection["searchPhrase"];
            }
        }

        protected static PagedSearchRequest GetPagedSearchRequest(int pageIndex,
            int pageSize,
            string sortColumnName,
            string sortOrder,
            string keyword)
        {
            var request = new PagedSearchRequest
            {
                PageIndex = pageIndex - 1,
                PageSize = pageSize,
                OrderByFieldName = sortColumnName,
                SortOrder = sortOrder,
                Keyword = keyword
            };

            return request;
        }

        protected ActionResult GetPagedSearchGridJson<TDto>(int pageIndex,
            int pageSize,
            List<object> rowJsonData,
            GenericPagedSearchResponse<TDto> response)
        {
            var jsonData = new
            {
                current = pageIndex,
                rowCount = pageSize,
                rows = rowJsonData,
                total = response.TotalCount
            };

            return this.Json(jsonData);
        }

        protected string GetEmailTemplateFilePath()
        {
            return this.ServerUtilityProvider.MapPath(ConfigurationManager.AppSettings["EmailTemplateFolder"]);
        }

        protected string GetSpeakingAssessmentTemplateFilePath()
        {
            return this.ServerUtilityProvider.MapPath(ConfigurationManager.AppSettings["DocumentTemplateFolder"]);
        }

        #endregion

        #region Private Methods

        private static void SetDefaultSorting(out string sortColumnName, out string sortOrder)
        {
            sortColumnName = string.Empty;
            sortOrder = string.Empty;
        }

        private static Exception GetDataExceptionMessage(Exception dataException)
        {
            var currentException = dataException;
            while (true)
            {
                if (currentException.InnerException == null)
                {
                    break;
                }

                currentException = currentException.InnerException;
            }

            return currentException;
        }

        #endregion
    }
}