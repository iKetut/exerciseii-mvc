﻿using System;
using System.Linq;
using System.Web.Mvc;
using LearningBase.Core;
using LearningBase.Mvc.Infrastructure;
using LearningBase.Mvc.ViewModel.Account;
using LearningBase.ServiceContract.Core;
using LearningBase.ServiceContract.Core.Request;

namespace LearningBase.Mvc.Controllers
{
    public class AccountController : LearningBaseControllerBase
    {
        #region Fields

        private readonly IAccountService _accountService;

        #endregion

        #region Constructors

        public AccountController(IServerUtilityProvider serverUtilityProvider, IAccountService accountService)
            : base(serverUtilityProvider)
        {
            this._accountService = accountService;
        }

        #endregion

        #region Public Methods

        public ActionResult Login(string returnUrl)
        {
            var response = this._accountService.GetCurrentLoginInformation();
            if (response.IsAuthenticated)
            {
                if (response.Roles.Contains(CoreConstant.Role.Administrator))
                {
                    return this.RedirectToAction("Index", "Home", new { area = "Administrator" });
                }

                if (response.Roles.Contains(CoreConstant.Role.Student))
                {
                    return this.RedirectToAction("Index", "Home", new { area = "Student" });
                }

                throw new NotImplementedException();
            }

            var model = new LoginVm();
            if (!string.IsNullOrEmpty(returnUrl))
            {
                model.ReturnUrl = returnUrl;
            }

            return this.View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginVm model)
        {
            if (this.ModelState.IsValid)
            {
                var request = new LoginRequest
                {
                    UserName = model.UserName,
                    Password = model.Password,
                    IpAddress = this.HttpContext.Request.UserHostAddress
                };

                var response = this._accountService.Login(request);

                if (response.IsError())
                {
                    return this.GetErrorJson(response.GetMessageErrorTextArray());
                }

                // Check if this is admin
                var isAdminResponse = this._accountService.IsAdministrator(request.UserName);
                if (isAdminResponse.IsError())
                {
                    return this.GetErrorJson(response.GetMessageErrorTextArray());
                }

                if (isAdminResponse.Result)
                {
                    var redirectUrl = string.IsNullOrEmpty(model.ReturnUrl)
                        ? this.Url.Action("Index", "Home", new {area = "Administrator"})
                        : model.ReturnUrl;

                    return
                        this.Json(
                            new
                            {
                                IsSuccess = true,
                                RedirectUrl = redirectUrl
                            });
                }

                // Check if this is student
                var isStudent = this._accountService.IsStudent(request.UserName);

                if (isStudent.IsError())
                {
                    return this.GetErrorJson(response.GetMessageErrorTextArray());
                }
                if (isStudent.Result)
                {
                    var redirectUrl = string.IsNullOrEmpty(model.ReturnUrl)
                        ? this.Url.Action("Index", "Home", new {area = "Student"})
                        : model.ReturnUrl;

                    return
                        this.Json(
                            new
                            {
                                IsSuccess = true,
                                RedirectUrl = redirectUrl
                            });
                }
            }

            return this.GetErrorJson(this.GetModelStateError().ToArray());
        }

        public ActionResult Logout()
        {
            this._accountService.Logout();
            SessionManager.Instance.Clear(this.Session);
            return this.RedirectToAction("Login");
        }

        #endregion
    }
}