﻿using System;
using System.Web.Mvc;
using LearningBase.Core;
using LearningBase.Mvc.Infrastructure;
using LearningBase.ServiceContract.Core;

namespace LearningBase.Mvc.Controllers
{
    public class HomeController : LearningBaseControllerBase
    {
        #region Fields

        private readonly IAccountService _accountService;

        #endregion

        #region Constructors

        public HomeController(IServerUtilityProvider serverUtilityProvider, IAccountService accountService)
            : base(serverUtilityProvider)
        {
            this._accountService = accountService;
        }

        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            var response = this._accountService.GetCurrentLoginInformation();
            if (response.IsAuthenticated)
            {
                if (response.Roles.Contains(CoreConstant.Role.Administrator))
                {
                    return this.RedirectToAction("Index", "Home", new { area = "Administrator" });
                }

                if (response.Roles.Contains(CoreConstant.Role.Student))
                {
                    return this.RedirectToAction("Index", "Home", new { area = "Student" });
                }

                throw new NotImplementedException();
            }

            return this.RedirectToAction("Login", "Account");
        }

        #endregion
    }
}