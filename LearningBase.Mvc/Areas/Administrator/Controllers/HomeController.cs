﻿using System.Web.Mvc;
using LearningBase.Core;
using LearningBase.Mvc.Controllers;
using LearningBase.Mvc.Infrastructure;

namespace LearningBase.Mvc.Areas.Administrator.Controllers
{
    [Authorize(Roles = CoreConstant.Role.Administrator)]
    public class HomeController : LearningBaseControllerBase
    {
        #region Constructors

        public HomeController(
            IServerUtilityProvider serverUtilityProvider)
            : base(serverUtilityProvider)
        {
        }

        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            return this.View();
        }

        #endregion
    }
}