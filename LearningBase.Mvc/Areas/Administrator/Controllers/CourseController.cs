﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LearningBase.Core;
using LearningBase.Dto.Course;
using LearningBase.Mvc.Areas.Administrator.Models.Course;
using LearningBase.Mvc.Controllers;
using LearningBase.Mvc.Infrastructure;
using LearningBase.Mvc.Infrastructure.Presentation;
using LearningBase.ServiceContract.Course;
using LearningBase.ServiceContract.Course.Request;

namespace LearningBase.Mvc.Areas.Administrator.Controllers
{
    [Authorize(Roles = CoreConstant.Role.Administrator)]
    public class CourseController : LearningBaseControllerBase
    {
        #region Fields

        private readonly ICourseService _courseService;
        private readonly IModuleService _moduleService;

        #endregion

        #region Constructors

        public CourseController(IServerUtilityProvider serverUtilityProvider,
            ICourseService courseService,
            IModuleService moduleService)
            : base(serverUtilityProvider)
        {
            this._courseService = courseService;
            this._moduleService = moduleService;
        }

        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            return this.PartialView("_Index");
        }

        [HttpPost]
        public ActionResult GetCourseGridJson(FormCollection formCollection)
        {
            return this.GetPagedSearchGridJson(formCollection,
                this._courseService.PagedSearch,
                delegate(ICollection<CourseDto> entities)
                {
                    var rowJsonData = new List<object>();
                    foreach (var entity in entities)
                    {
                        rowJsonData.Add(new
                        {
                            entity.Id,
                            entity.Code,
                            entity.Name,
                            entity.Description
                        });
                    }

                    return rowJsonData;
                });
        }

        public ActionResult Create()
        {
            var model = new CreatePartialVm
            {
                MaintenanceButtonRow = new MaintenanceButtonRow(MaintenanceButtonRow.Preset.CreateCancel)
            };

            return this.PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(CreatePartialVm model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorJson(this.GetModelStateError().ToArray());
            }

            var request = new SaveCourseRequest
            {
                CourseDto = new CourseDto
                {
                    Code = model.Code,
                    Name = model.Name,
                    Description = model.Description
                }
            };

            var response = this._courseService.Create(request);
            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson(response, response.DtoId);
        }

        [HttpGet]
        public ActionResult Edit(int courseId)
        {
            var response = this._courseService.Read(courseId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            var model = new EditPartialVm
            {
                Id = response.Dto.Id,
                Code = response.Dto.Code,
                Name = response.Dto.Name,
                Description = response.Dto.Description,
                MaintenanceButtonRow = new MaintenanceButtonRow(MaintenanceButtonRow.Preset.SaveDeleteCancel)
            };

            this.PopulateAuditFieldValue(response.Dto, model);

            return this.PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(EditPartialVm model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorJson(this.GetModelStateError().ToArray());
            }

            var request = new SaveCourseRequest
            {
                CourseDto = new CourseDto
                {
                    Id = model.Id,
                    Code = model.Code,
                    Name = model.Name,
                    Description = model.Description
                }
            };

            this.PopulateAuditFieldValue(model, request.CourseDto);

            var response = this._courseService.Edit(request);
            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson(response, response.DtoId);
        }

        [HttpPost]
        public ActionResult Delete(int courseId)
        {
            var response = this._courseService.Delete(courseId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson();
        }

        [HttpGet]
        public ActionResult CourseEditor(int courseId)
        {
            var response = this._courseService.Read(courseId);
            var searchModulesReponse = this._moduleService.Search(courseId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            var model = new CourseEditorPartialVm
            {
                CourseId = response.Dto.Id,
                Code = response.Dto.Code,
                Name = response.Dto.Name
            };

            var orderedList = searchModulesReponse.DtoCollection.OrderBy(item => item.SortingIndex);
            foreach (var moduleDto in orderedList)
            {
                model.ModuleDtos.Add(moduleDto);
            }

            return this.PartialView("_CourseEditor", model);
        }

        [HttpPost]
        public ActionResult AddModule(int courseId, string moduleName)
        {
            var request = new SaveModuleRequest
            {
                ModuleDto = new ModuleDto
                {
                    CourseId = courseId,
                    Name = moduleName
                }
            };

            var response = this._moduleService.Create(request);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.Json(new {IsSuccess = true, ModuleId = response.DtoId});
        }

        [HttpPost]
        public ActionResult EditModuleName(int courseId, int moduleId, string moduleName)
        {
            var response = this._moduleService.EditName(moduleId, courseId, moduleName);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson();
        }

        [HttpPost]
        public ActionResult DeleteModule(int moduleId)
        {
            var response = this._moduleService.Delete(moduleId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson();
        }

        [HttpPost]
        public ActionResult UpdateIndex(int moduleId, int sortingIndex)
        {
            var response = this._moduleService.UpdateIndex(moduleId, sortingIndex);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson();
        }

        #endregion
    }
}