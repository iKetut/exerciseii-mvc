﻿using System.Collections.Generic;
using System.Linq;
using LearningBase.Core;
using LearningBase.Dto.Common;
using LearningBase.Mvc.Areas.Administrator.Models.Student;
using LearningBase.Mvc.Controllers;
using LearningBase.Mvc.Infrastructure;
using LearningBase.Mvc.Infrastructure.Presentation;
using LearningBase.ServiceContract.Common;
using LearningBase.ServiceContract.Common.Request;
using LearningBase.ServiceContract.Course;
using System.Web.Mvc;

namespace LearningBase.Mvc.Areas.Administrator.Controllers
{
    [Authorize(Roles = CoreConstant.Role.Administrator)]
    public class StudentController : LearningBaseControllerBase
    {
        #region Fields

        private readonly IStudentService _studentService;
        private readonly ICourseService _courseService;

        #endregion

        #region Constructors

        public StudentController(IServerUtilityProvider serverUtilityProvider,
            IStudentService studentService,
            ICourseService courseService)
            : base(serverUtilityProvider)
        {
            this._studentService = studentService;
            this._courseService = courseService;
        }

        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            return this.PartialView("_Index");
        }

        [HttpPost]
        public ActionResult GetStudentGridJson(FormCollection formCollection)
        {
            return this.GetPagedSearchGridJson(formCollection,
                this._studentService.PagedSearch,
                delegate(ICollection<StudentDto> entities)
                {
                    var rowJsonData = new List<object>();
                    foreach (var entity in entities)
                    {
                        rowJsonData.Add(new
                        {
                            entity.Id,
                            entity.UserName,
                            entity.EmailAddress,
                            CreatedDateTime =
                                DateTimeManager.UtcToLocalDateTime(CoreConstant.Setting.DefaultLocalTimeZoneId,
                                    entity.CreatedDateTime).ToString(CoreConstant.Setting.DefaultLongDateFormat)
                        });
                    }

                    return rowJsonData;
                });
        }

        public ActionResult Create()
        {
            var model = new CreatePartialVm
            {
                CourseSelectList = this.GetCourseSelectList(),
                MaintenanceButtonRow = new MaintenanceButtonRow(MaintenanceButtonRow.Preset.CreateCancel)
            };

            return this.PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(CreatePartialVm model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorJson(this.GetModelStateError().ToArray());
            }

            var request = new CreateStudentRequest
            {
                StudentDto = new StudentDto
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    UserName = model.UserName,
                    CourseId = model.CourseId
                },
                PrimaryUserPassword = model.Password
            };

            var response = this._studentService.Create(request);
            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson(response, response.DtoId);
        }

        [HttpGet]
        public ActionResult Edit(int studentId)
        {
            var response = this._studentService.Read(studentId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            var model = new EditPartialVm
            {
                Id = response.Dto.Id,
                FirstName = response.Dto.FirstName,
                LastName = response.Dto.LastName,
                EmailAddress = response.Dto.EmailAddress,
                UserId = response.Dto.UserId,
                UserName = response.Dto.UserName,
                CourseSelectList = this.GetCourseSelectList(),
                CourseId = response.Dto.CourseId,
                MaintenanceButtonRow = new MaintenanceButtonRow(MaintenanceButtonRow.Preset.SaveDeleteCancel)
            };

            this.PopulateAuditFieldValue(response.Dto, model);

            return this.PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(EditPartialVm model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.GetErrorJson(this.GetModelStateError().ToArray());
            }

            var request = new EditStudentRequest
            {
                StudentDto = new StudentDto
                {
                    Id = model.Id,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAddress = model.EmailAddress,
                    UserId = model.UserId,
                    CourseId = model.CourseId
                }
            };

            this.PopulateAuditFieldValue(model, request.StudentDto);

            var response = this._studentService.Edit(request);
            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson(response, response.DtoId);
        }

        [HttpPost]
        public ActionResult Delete(int studentId)
        {
            var response = this._studentService.Delete(studentId);

            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson();
        }

        #endregion

        #region Private Methods

        private SelectList GetCourseSelectList()
        {
            var response = this._courseService.GetAll();

            var studentPackages =
                response.DtoCollection.Select(
                    item => new {item.Id, item.Name});
            return new SelectList(studentPackages, "Id", "Name");
        }

        #endregion
    }
}