﻿using System.Web.Mvc;

namespace LearningBase.Mvc.Areas.Administrator
{
    public class AdministratorAreaRegistration : AreaRegistration
    {
        #region Properties

        public override string AreaName
        {
            get { return "Administrator"; }
        }

        #endregion

        #region Public Methods

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Administrator_default",
                "Administrator/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional},
                new[] {"LearningBase.Mvc.Areas.Administrator.Controllers"}
                );
        }

        #endregion
    }
}