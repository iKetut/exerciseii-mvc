﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using LearningBase.Dto.Course;

namespace LearningBase.Mvc.Areas.Administrator.Models.Course
{
    public class CourseEditorPartialVm
    {
        #region Fields

        private ICollection<ModuleDto> _moduleDtos;

        #endregion

        #region Properties

        public int CourseId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public ICollection<ModuleDto> ModuleDtos
        {
            get { return this._moduleDtos ?? (this._moduleDtos = new Collection<ModuleDto>()); }
        }

        #endregion
    }
}