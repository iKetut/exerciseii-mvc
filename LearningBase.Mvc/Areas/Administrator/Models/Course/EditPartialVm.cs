﻿using System.ComponentModel.DataAnnotations;
using LearningBase.Mvc.Infrastructure.Presentation;
using LearningBase.Mvc.ViewModel;

namespace LearningBase.Mvc.Areas.Administrator.Models.Course
{
    public class EditPartialVm : AuditableViewModelBase
    {
        #region Properties

        public int Id { get; set; }

        [Required]
        [Display(Name = "Course_Code", ResourceType = typeof(Resource))]
        public string Code { get; set; }

        [Required]
        [Display(Name = "Course_Name", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Display(Name = "Course_Description", ResourceType = typeof(Resource))]
        public string Description { get; set; }

        public MaintenanceButtonRow MaintenanceButtonRow { get; set; }

        #endregion
    }
}