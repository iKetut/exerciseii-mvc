﻿using System.ComponentModel.DataAnnotations;
using LearningBase.Mvc.Infrastructure.Presentation;

namespace LearningBase.Mvc.Areas.Administrator.Models.Course
{
    public class CreatePartialVm
    {
        #region Properties

        [Required]
        [Display(Name = "Course_Code", ResourceType = typeof(Resource))]
        public string Code { get; set; }

        [Required]
        [Display(Name = "Course_Name", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Display(Name = "Course_Description", ResourceType = typeof(Resource))]
        public string Description { get; set; }

        public MaintenanceButtonRow MaintenanceButtonRow { get; set; }

        #endregion
    }
}