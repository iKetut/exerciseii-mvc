﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using LearningBase.Mvc.Infrastructure.Presentation;

namespace LearningBase.Mvc.Areas.Administrator.Models.Student
{
    public class CreatePartialVm
    {
        #region Properties

        [Required]
        [Display(Name = "Student_FirstName", ResourceType = typeof(Resource))]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Student_LastName", ResourceType = typeof(Resource))]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Student_EmailAddress", ResourceType = typeof(Resource))]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Student_UserName", ResourceType = typeof(Resource))]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Student_Password", ResourceType = typeof(Resource))]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Student_RetypePassword", ResourceType = typeof(Resource))]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resource),
            ErrorMessageResourceName = "Student_PasswordNotMatchMessage")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Student_Course", ResourceType = typeof(Resource))]
        public int CourseId { get; set; }

        public SelectList CourseSelectList { get; set; }

        public MaintenanceButtonRow MaintenanceButtonRow { get; set; }

        #endregion
    }
}