﻿using System.Web.Mvc;
using LearningBase.Core;
using LearningBase.Mvc.Controllers;
using LearningBase.Mvc.Infrastructure;
using LearningBase.ServiceContract.Common;

namespace LearningBase.Mvc.Areas.Student.Controllers
{
    [Authorize(Roles = CoreConstant.Role.Student)]
    public class HomeController : LearningBaseControllerBase
    {
        #region Fields

        private readonly IStudentService _studentService;

        #endregion

        #region Constructors

        public HomeController(IServerUtilityProvider serverUtilityProvider, IStudentService studentService)
            : base(serverUtilityProvider)
        {
            this._studentService = studentService;
        }

        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            SessionManager.Instance.Refresh(this.Session);
            return this.View();
        }

        [HttpPost]
        public ActionResult GetProfile()
        {
            var studentId = SessionManager.Instance.GetEntityId(this.Session);

            var response = this._studentService.Read(studentId);
            if (response.IsError())
            {
                return this.GetErrorJson(response);
            }

            return this.GetBasicSuccessJson(response,
                new
                {
                    response.Dto.StudentName,
                    CourseName = response.Dto.CourseName
                });
        }

        #endregion
    }
}