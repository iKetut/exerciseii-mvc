﻿using System.Web.Mvc;

namespace LearningBase.Mvc.Areas.Student
{
    public class StudentAreaRegistration : AreaRegistration
    {
        #region Properties

        public override string AreaName
        {
            get { return "Student"; }
        }

        #endregion

        #region Public Methods

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Student_default",
                "Student/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional},
                new[] {"LearningBase.Mvc.Areas.Student.Controllers"}
                );
        }

        #endregion
    }
}