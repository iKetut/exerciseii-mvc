﻿var AccountLogin = function (element) {
    this.Element = element;
}

AccountLogin.prototype = {
    constructor: AccountLogin,
    Register: function() {
        var self = this;

        var loginForm = $("#loginForm", self.Element);
        var loginUrl = loginForm.data('login-url');

        $("#loginForm")
            .validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

        $('#btn-login')
            .click(function() {
                self.Login(loginUrl);
            });

        $('#Password')
            .keypress(function(e) {
                if (e.keyCode === 13) {
                    self.Login(loginUrl);
                }
            });
    },
    Login: function (loginUrl) {
        return submitForm('#loginForm',
            loginUrl,
            function(data) {
                window.location.href = data.RedirectUrl;
            });
    }
}