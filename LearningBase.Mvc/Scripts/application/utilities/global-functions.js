﻿// Click element if exists
function clickIfExists(selector) {
    if ($(selector).length > 0) {
        $(selector).click();
    }
    else {
        window.setTimeout(function () { clickIfExists(selector); }, 100);
    }
}