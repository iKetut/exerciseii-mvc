﻿var FormCreate = function (element) {
    this.Element = element;
    this.Caller = null;
    this.Grid = $('#gridMaintenance');
    this.MaintenanceForm = $("#maintenanceForm", element);
}

FormCreate.prototype = {
    constructor: FormCreate,
    Register: function () {
        var self = this;
        var maintenanceForm = self.MaintenanceForm;

        $('#createButton', self.Element)
            .click(function () {
                submitForm('#' + self.MaintenanceForm.attr('id'),
                    maintenanceForm.data('url'),
                    function (data) {
                        showInformation(maintenanceForm.data('saved-message'));
                        console.log(self.Grid);
                        self.Grid.bootgrid('reload');
                        closeMaintenanceWindow();
                        if (self.Caller !== null && (typeof self.Caller.TriggerClickedLink == 'function')) {
                            self.Caller.TriggerClickedLink(data.Value);
                        }
                    });
            });

        $('#cancelButton', self.Element)
            .click(function() {
                $(self.Element).html('');
            });
    },
    SetupCaller: function (caller) {
        this.Caller = caller;
    }
}