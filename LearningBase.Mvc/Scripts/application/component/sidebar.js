﻿var Sidebar = function (element) {
    this.Element = element;
}

Sidebar.prototype = {
    constructor: Sidebar,
    Register: function () {
        var self = this;

        $('.nav a', self.Element)
            .on('click',
                function () {
                    $('.nav a', self.Element)
                        .each(function () {
                            $(this).removeClass('active');
                        });
                });

        $('.nav-ajax-menu', self.Element)
            .on('click',
                function (event) {
                    event.preventDefault();

                    showLoadingDialog();

                    var contentUrl = $(this).data('url');

                    $.get(contentUrl,
                        function (contentData) {
                            $('#mainUserContent').html(contentData);

                            var helper = new Helper();
                            var namespace = helper.CreateNameSpaceFromURL(contentUrl);
                            var apps = new Application($('#mainUserContent'), namespace);
                            apps.Register();

                            hideLoadingDialog();
                        },
                        "html");

                    $(this).addClass("active");
                });
    }
}