﻿var FormView = function (element) {
    this.Element = element;
    this.Caller = null;
}

FormView.prototype = {
    constructor: FormEdit,
    Register: function () {
        var self = this;

        $('#deleteButton', self.Element)
            .click(function () {
                var url = $("#maintenanceForm", self.Element).data('delete-url');
                url += '?id=' + $('#Id').val();

                submitNonForm($("#maintenanceForm", self.Element).data('delete-confirmation-message'),
                    url, function () {
                        showInformation($("#maintenanceForm", self.Element).data('delete-message'));
                        $('#gridMaintenance').bootgrid('reload');
                        closeMaintenanceWindow();
                    });
            });
        
        $('#cancelButton', self.Element)
            .click(function () {
                $(self.Element).html('');
            });
    },
    SetupCaller: function (caller) {
        this.Caller = caller;
    }
}