﻿var FormEdit = function (element) {
    this.Element = element;
    this.Caller = null;
    this.Grid = $('#gridMaintenance');
    this.MaintenanceForm = $("#maintenanceForm", element);
}

FormEdit.prototype = {
    constructor: FormEdit,
    Register: function () {
        var self = this;
        var maintenanceForm = self.MaintenanceForm;

        $('#saveButton', self.Element)
             .click(function () {
                 submitForm('#'+self.MaintenanceForm.attr('id'),
                     maintenanceForm.data('edit-url'),
                     function (data) {
                         showInformation(maintenanceForm.data('saved-message'));
                         self.Grid.bootgrid('reload');
                         closeMaintenanceWindow();
                         if (self.Caller !== null && (typeof self.Caller.TriggerClickedLink == 'function')) {
                             self.Caller.TriggerClickedLink(data.Value);
                         }
                     });
             });

        $('#deleteButton', self.Element)
            .click(function () {
                var url = maintenanceForm.data('delete-url');
                url += '?id=' + $('#Id').val();

                submitNonForm(maintenanceForm.data('delete-confirmation-message'),
                    url, function () {
                        showInformation(maintenanceForm.data('delete-message'));
                        self.Grid.bootgrid('reload');
                        closeMaintenanceWindow();
                    });
            });
        
        $('#cancelButton', self.Element)
            .click(function () {
                $(self.Element).html('');
            });
    },
    SetupCaller: function (caller) {
        this.Caller = caller;
    }
}