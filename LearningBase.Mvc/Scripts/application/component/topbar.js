﻿var Topbar = function (element) {
    this.Element = element;
}

Topbar.prototype = {
    constructor: Topbar,
    Register: function () {
        var self = this;

        $('.nav-ajax-menu', self.Element)
            .on('click',
                function (event) {
                    event.preventDefault();

                    showLoadingDialog();

                    var contentUrl = $(this).data('url');

                    $.get(contentUrl,
                        function (contentData) {
                            $('#mainUserContent').html(contentData);

                            var helper = new Helper();
                            var namespace = helper.CreateNameSpaceFromURL(contentUrl);
                            var apps = new Application($('#mainUserContent'), namespace);
                            apps.Register();

                            hideLoadingDialog();
                        },
                        "html");
                });
    }
}