﻿var Helper = function () { }

Helper.prototype = {
    constructor: Helper,
    CreateNameSpaceFromURL: function (url) {
        var urlWithoutParameters = url.split('?');
        var arrayUrl = urlWithoutParameters[0].split('/');
        arrayUrl.splice(0, 1);
        if (arrayUrl.length === 2) {
            arrayUrl.push('Index');
        }
        var result = arrayUrl.join('');

        return result;
    }
}