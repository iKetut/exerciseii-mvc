﻿var AdministratorStudentIndex = function (element) {
    this.Element = element;
    this.IsTriggerMode = false;
    this.TriggerId = 0;
}

AdministratorStudentIndex.prototype = {
    constructor: AdministratorStudentIndex,
    Register: function() {
        var self = this;

        var grid = $("#gridStudent ", self.Element);

        grid.bootgrid({
                ajax: true,
                post: function() {},
                url: grid.data('url'),
                formatters: {
                    "UserName": function(column, row) {
                        return "<a href=\"#\" class=\"userNameLink\" id=\"" +
                            row.Id +
                            "\">" +
                            row.UserName +
                            "</a>";
                    }
                }
            })
            .on("loaded.rs.jquery.bootgrid",
                function() {
                    $('.command-add', self.Element)
                        .on("click",
                            function() {

                                showProcessingDialog();

                                var maintenanceDiv = $('#maintenancePane');
                                var url = grid.data('create-url');

                                $.get(url,
                                    function(data) {
                                        maintenanceDiv.html(data);

                                        var helper = new Helper();
                                        var namespace = helper.CreateNameSpaceFromURL(url);
                                        var apps = new Application(maintenanceDiv, namespace);
                                        apps.RegisterWithCaller(self);

                                        hideProcessingDialog();
                                        $('html, body')
                                            .animate({
                                                    scrollTop:
                                                        maintenanceDiv.offset().top - 75
                                                },
                                                'slow');
                                    });
                            });

                    $('.userNameLink', self.Element)
                        .on("click",
                            function(e) {
                                e.preventDefault();

                                showProcessingDialog();

                                var maintenanceDiv = $('#maintenancePane');
                                var url = grid.data('edit-url');
                                url += '?studentId=' + $(this).attr('id');

                                $.get(url,
                                    function(data) {
                                        maintenanceDiv.html(data);

                                        var helper = new Helper();
                                        var namespace = helper.CreateNameSpaceFromURL(url);
                                        var apps = new Application(maintenanceDiv, namespace);
                                        apps.RegisterWithCaller(self);

                                        hideProcessingDialog();
                                        $('html, body')
                                            .animate({
                                                    scrollTop:
                                                        maintenanceDiv.offset().top - 75
                                                },
                                                'slow');
                                    });
                            });

                    if (self.IsTriggerMode) {
                        $('#' + self.TriggerId + '', grid).trigger('click');

                        self.IsTriggerMode = false;
                        self.TriggerId = 0;
                    }

                });
    },
    TriggerClickedLink: function(id) {
        this.IsTriggerMode = true;
        this.TriggerId = id;
    }
}