﻿var AdministratorStudentCreate = function (element) {
    this.Element = element;
    this.Caller = null;
}

AdministratorStudentCreate.prototype = {
    constructor: AdministratorStudentCreate,
    Register: function() {
        var self = this;
        var maintenanceForm = $("#maintenanceForm", self.Element);

        var form = new FormCreate(self.Element);
        form.Grid = $('#gridStudent');
        form.SetupCaller(this.Caller);
        form.Register();

        maintenanceForm.validate({
            rules: {
                FirstName: {
                    maxlength: 200
                },
                LastName: {
                    maxlength: 200
                },
                EmailAddress: {
                    maxlength: 500
                },
                UserName: {
                    maxlength: 56
                },
                Password: {
                    maxlength: 128
                },
                ConfirmPassword: {
                    minlength: 5,
                    equalTo: "#Password"
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
    },
    SetupCaller: function(caller) {
        this.Caller = caller;
    }
}