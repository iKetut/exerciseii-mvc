﻿var AdministratorStudentEdit = function (element) {
    this.Element = element;
    this.Caller = null;
}

AdministratorStudentEdit.prototype = {
    constructor: AdministratorStudentEdit,
    Register: function() {
        var self = this;
        var maintenanceForm = $("#maintenanceForm", self.Element);

        var form = new FormEdit(self.Element);
        form.Grid = $('#gridOrganization');
        form.SetupCaller(self.Caller);
        form.Register();

        maintenanceForm.validate({
            rules: {
                FirstName: {
                    maxlength: 200
                },
                LastName: {
                    maxlength: 200
                },
                EmailAddress: {
                    maxlength: 500
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });

        $('#deleteButton')
            .off('click')
            .on('click',
                function() {
                    var url = maintenanceForm.data('delete-url');
                    url += '?studentId=' + $('#Id').val();

                    submitDeleteForm(maintenanceForm.data('delete-confirmation-message'),
                        url,
                        function() {
                            showInformation(maintenanceForm.data('delete-message'));
                            $('#gridStudent').bootgrid('reload');
                            closeMaintenanceWindow();
                        });
                });

    },
    SetupCaller: function(caller) {
        this.Caller = caller;
    }
}