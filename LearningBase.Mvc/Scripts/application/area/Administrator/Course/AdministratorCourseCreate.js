﻿var AdministratorCourseCreate = function (element) {
    this.Element = element;
    this.Caller = null;
}

AdministratorCourseCreate.prototype = {
    constructor: AdministratorCourseCreate,
    Register: function() {
        var self = this;
        var maintenanceForm = $("#maintenanceForm", self.Element);

        var form = new FormCreate(self.Element);
        form.Grid = $('#gridCourse');
        form.SetupCaller(this.Caller);
        form.Register();

        maintenanceForm.validate({
            rules: {
                Code: {
                    maxlength: 10
                },
                Name: {
                    maxlength: 200
                },
                Decription: {
                    maxlength: 500
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
    },
    SetupCaller: function(caller) {
        this.Caller = caller;
    }
}