﻿var AdministratorCourseCourseEditor = function (element) {
    this.Element = element;
    this.Caller = null;
}

AdministratorCourseCourseEditor.prototype = {
    constructor: AdministratorCourseCourseEditor,
    Register: function () {
        var self = this;

        self.CourseId = $("#CourseId").val();

        var editor = $('#editor');
        var addModuleUrl = editor.data("add-module-url");
        var editModuleNameUrl = editor.data("edit-module-name-url");
        var deleteModuleUrl = editor.data("delete-module-url");
        var updateModuleIndexUrl = editor.data("update-module-index-url");
        var savedInfoMessage = editor.data("saved-info-message");

        self.DragDropSourceModuleId = 0;
        self.DragDropTargetModuleId = 0;
        
        $("#editor ul").sortable({
            placeholder: "ui-state-highlight",
            start: function (event, ui) {
                self.DragDropSourceModuleId = $(ui.item).data("module-id");
            },
            stop: function(e, ui) {
                alert(ui.item.index() + 1);
                submitData({ moduleId: self.DragDropSourceModuleId, sortingIndex: ui.item.index() + 1 },
                    updateModuleIndexUrl, function() {

                    });
            }
        });

        $('#btn-add-new-module')
            .click(function() {
                var createData = { courseId: self.CourseId, moduleName: "New Module" };

                submitData(createData,
                    addModuleUrl,
                    function(data) {

                        showInformation(savedInfoMessage);

                        $("#editor ul.module")
                            .append("<li class=\"ui-sortable-handle\">" +
                                "<div class=\"module-panel\" data-module-id=\"" +
                                data.ModuleId +
                                "\" data-module-name=\"New Module\">" +
                                "<a href=\"#\" class=\"module-edit-link\">New Module</a>" +
                                "<span class=\"module-buttons\">" +
                                "<a href=\"#\" class=\"module-delete-link\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Module\" data-original-title=\"Delete Module\"><i class=\"fa fa-trash\"></i></a>" +
                                "<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Add New Topic\" data-original-title=\"Add New Topic\"><i class=\"fa fa-folder\"></i></a>" +
                                "</span></div>" +
                                "</li></div>");
                    });
            });

        $('#editor ul')
            .on("click",
                "li.ui-sortable-handle > div.module-panel > a.module-edit-link",
                function(e) {
                    e.preventDefault();

                    if (self.PreviousEditedElement != null) {
                        self.PreviousEditedElement.html(self.CurrentEditedHtml);
                    }

                    self.PreviousEditedElement = $(this).parent();
                    self.CurrentEditedHtml = $(this).parent().html();
                    
                    var currentValue = $(this).parent().data("module-name");

                    $(this).parent().html("<label class=\"course-editor-label\">Module Name:</label> " +
                        "<input class=\"form-control course-editor-textbox\" value=\"" + currentValue + "\"/>" +
                        "<div class=\"course-editor-buttons\"><button class=\"btn btn-primary btn-save\">Save</button><button class=\"btn btn-primary btn-cancel\">Cancel</button></div>");
                });

        $('#editor ul')
            .on("click",
                "li.ui-sortable-handle > div.module-panel > span.module-buttons > a.module-delete-link",
                function(e) {
                    e.preventDefault();

                    var modulePanel = $(this).parent().parent();
                    var moduleId = modulePanel.data("module-id");
                    var itemToDelete = $(this).parent().parent().parent();

                    showYesNoActionConfirmationDialog('This module will be deleted. Do you want to continue?',
                        function () {
                            var deleteModuleData = { moduleId: moduleId };
                            submitData(deleteModuleData,
                                deleteModuleUrl,
                                function () {
                                    itemToDelete.remove();
                                    showInformation(savedInfoMessage);
                                });
                        },
                        null);
                });

        $('#editor ul')
            .on("click",
                "li.ui-sortable-handle > div.module-panel > div.course-editor-buttons > button.btn-save",
                function (e) {

                    var modulePanel = $(this).parent().parent();
                    
                    var moduleId = modulePanel.data("module-id");
                    var moduleName = modulePanel.find('.course-editor-textbox').val();

                    var editModuleNameData = { courseId: self.CourseId, moduleId: moduleId, moduleName: moduleName };
                    submitData(editModuleNameData,
                        editModuleNameUrl,
                        function() {

                            modulePanel.data("module-name", moduleName);

                            self.PreviousEditedElement.html("<a href=\"#\" class=\"module-edit-link\">" +
                                moduleName +
                                "</a>" +
                                "<span class=\"module-buttons\">" +
                                "<a href=\"#\" class=\"module-delete-link\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Module\" data-original-title=\"Delete Module\"><i class=\"fa fa-trash\"></i></a>" +
                                "<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Add New Topic\" data-original-title=\"Add New Topic\"><i class=\"fa fa-folder\"></i></a>" +
                                "</span></div>" +
                                "</li>");
                            self.CurrentEditedHtml = "";
                            self.PreviousEditedElement = null;

                            showInformation(savedInfoMessage);
                        });
                });

        $('#editor ul')
            .on("click",
                "li.ui-sortable-handle > div.module-panel > div.course-editor-buttons > button.btn-cancel",
                function (e) {
                    $(this).parent().parent().html(self.CurrentEditedHtml);
                    self.CurrentEditedHtml = "";
                    self.PreviousEditedElement = null;
                });
    },
    SetupCaller: function (caller) {
        this.Caller = caller;
    }
}