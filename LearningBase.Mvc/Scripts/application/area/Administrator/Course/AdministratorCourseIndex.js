﻿var AdministratorCourseIndex = function(element) {
    this.Element = element;
    this.IsTriggerMode = false;
    this.TriggerId = 0;
};
AdministratorCourseIndex.prototype = {
    constructor: AdministratorCourseIndex,
    Register: function() {
        var self = this;

        var grid = $("#gridCourse", self.Element);

        grid.bootgrid({
                ajax: true,
                post: function() {},
                url: grid.data("url"),
                formatters: {
                    "Code": function(column, row) {
                        return "<a href=\"#\" class=\"codeLink\" id=\"" +
                            row.Id +
                            "\">" +
                            row.Code +
                            "</a>";
                    },
                    "Action": function(column, row) {
                        return "<a href=\"#\" id=\"" +
                            row.Id +
                            "\" class=\"editContentLink\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit Content\" data-original-title=\"Edit Content\"><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a>";
                    }
                }
            })
            .on("loaded.rs.jquery.bootgrid",
                function() {
                    $(".command-add", self.Element)
                        .on("click",
                            function() {

                                showProcessingDialog();

                                var maintenanceDiv = $("#maintenancePane");
                                var url = grid.data("create-url");

                                $.get(url,
                                    function(data) {
                                        maintenanceDiv.html(data);

                                        var helper = new Helper();
                                        var namespace = helper.CreateNameSpaceFromURL(url);
                                        var apps = new Application(maintenanceDiv, namespace);
                                        apps.RegisterWithCaller(self);

                                        hideProcessingDialog();
                                        $("html, body")
                                            .animate({
                                                    scrollTop:
                                                        maintenanceDiv.offset().top - 75
                                                },
                                                "slow");
                                    });
                            });

                    $(".codeLink", self.Element)
                        .on("click",
                            function(e) {
                                e.preventDefault();

                                showProcessingDialog();

                                var maintenanceDiv = $("#maintenancePane");
                                var url = grid.data("edit-url");
                                url += "?courseId=" + $(this).attr("id");

                                $.get(url,
                                    function(data) {
                                        maintenanceDiv.html(data);

                                        var helper = new Helper();
                                        var namespace = helper.CreateNameSpaceFromURL(url);
                                        var apps = new Application(maintenanceDiv, namespace);
                                        apps.RegisterWithCaller(self);

                                        hideProcessingDialog();
                                        $("html, body")
                                            .animate({
                                                    scrollTop:
                                                        maintenanceDiv.offset().top - 75
                                                },
                                                "slow");
                                    });
                            });

                    $(".editContentLink", self.Element)
                        .on("click",
                            function(e) {
                                e.preventDefault();

                                showProcessingDialog();

                                var maintenanceDiv = $("#maintenancePane");
                                var url = grid.data("edit-content-url");
                                url += "?courseId=" + $(this).attr("id");

                                $.get(url,
                                    function(data) {
                                        maintenanceDiv.html(data);

                                        var helper = new Helper();
                                        var namespace = helper.CreateNameSpaceFromURL(url);
                                        var apps = new Application(maintenanceDiv, namespace);
                                        apps.RegisterWithCaller(self);

                                        hideProcessingDialog();
                                        $("html, body")
                                            .animate({
                                                    scrollTop:
                                                        maintenanceDiv.offset().top - 75
                                                },
                                                "slow");
                                    });
                            });

                    if (self.IsTriggerMode) {
                        $("#" + self.TriggerId + "", grid).trigger("click");

                        self.IsTriggerMode = false;
                        self.TriggerId = 0;
                    }

                });
    },
    TriggerClickedLink: function(id) {
        this.IsTriggerMode = true;
        this.TriggerId = id;
    }
};