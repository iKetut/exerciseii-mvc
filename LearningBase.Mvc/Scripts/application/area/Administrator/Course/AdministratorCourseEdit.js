﻿var AdministratorCourseEdit = function (element) {
    this.Element = element;
    this.Caller = null;
}

AdministratorCourseEdit.prototype = {
    constructor: AdministratorCourseEdit,
    Register: function() {
        var self = this;
        var maintenanceForm = $("#maintenanceForm", self.Element);

        var form = new FormEdit(self.Element);
        form.Grid = $('#gridCourse');
        form.SetupCaller(self.Caller);
        form.Register();

        maintenanceForm.validate({
            rules: {
                Code: {
                    maxlength: 10
                },
                Name: {
                    maxlength: 200
                },
                Decription: {
                    maxlength: 500
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });

        $('#deleteButton')
            .off('click')
            .on('click',
                function() {
                    var url = maintenanceForm.data('delete-url');
                    url += '?courseId=' + $('#Id').val();

                    submitDeleteForm(maintenanceForm.data('delete-confirmation-message'),
                        url,
                        function() {
                            showInformation(maintenanceForm.data('delete-message'));
                            $('#gridCourse').bootgrid('reload');
                            closeMaintenanceWindow();
                        });
                });

    },
    SetupCaller: function(caller) {
        this.Caller = caller;
    }
}