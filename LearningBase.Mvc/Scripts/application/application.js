﻿var Application = function (element, namespace) {
    this.Element = element;
    this.Namespace = namespace;
};

Application.prototype = {
    constructor: Application,
    Register: function () {
        if (this.Namespace === "") {
            console.error("Namespace Empty");
            return;
        }

        if (!window[this.Namespace]) {
            console.error("Javascript file for the loaded page has not been created yet");
            return;
        }

        var currentPage = new window[this.Namespace](this.Element);
        currentPage.Register();
    },
    RegisterWithCaller: function (caller) {
        if (this.Namespace === "") {
            console.error("Namespace Empty");
            return;
        }

        if (!window[this.Namespace]) {
            console.error("Javascript file for the loaded page has not been created yet");
            return;
        }

        var currentPage = new window[this.Namespace](this.Element);
        if (typeof currentPage.Register == 'function') {
            currentPage.SetupCaller(caller);
            currentPage.Register();
        }
    }
}
