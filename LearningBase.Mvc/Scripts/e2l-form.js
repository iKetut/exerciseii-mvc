﻿function submitData(postData, submitUrl, onSuccess) {
    showProcessingDialog();

    $.ajax({
        cache: false,
        type: "POST",
        url: submitUrl,
        data: postData,
        success: function (data) {
            if (data.IsSuccess) {
                hideProcessingDialog();
                onSuccess(data);
            } else {
                processErrorPostResponse(data);

                return false;
            }
        },
        error: function () {
            alert("Error when sending request...");
            hideProcessingDialog();
            return false;
        }
    });

    return false;
}

function submitForm(formName, submitUrl, onSuccess) {
    showProcessingDialog();

    var isValid = $(formName).valid();
    if (!isValid) {
        hideProcessingDialog();
        return false;
    }

    $.ajax({
        cache: false,
        type: "POST",
        url: submitUrl,
        data: $(formName).serialize(),
        success: function(data) {
            if (data.IsSuccess) {
                hideProcessingDialog();
                onSuccess(data);
            } else {
                processErrorPostResponse(data);

                return false;
            }
        },
        error: function() {
            alert("Error when sending request...");
            hideProcessingDialog();
            return false;
        }
    });

    return false;
}

function submitNonForm(confirmationMessage, submitUrl, onSuccess) {
    if (confirmationMessage !== '') {
        showConfirmationDialog(confirmationMessage, function () {
            submitNonFormPost(submitUrl, onSuccess);
        });
    } else {
        submitNonFormPost(submitUrl, onSuccess);
    }
}

function submitNonFormPost(submitUrl, onSuccess) {
    showProcessingDialog();

    setTimeout(function() {
            $.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: submitUrl,
                success: function(data) {
                    if (data.IsSuccess) {
                        hideProcessingDialog();
                        onSuccess(data);
                    } else {
                        processErrorPostResponse(data);
                    }
                },
                error: function() {
                    alert("Error when sending request...");
                    hideProcessingDialog();
                    return false;
                }
            });
        },
        500);
}

function processErrorPostResponse(data) {
    $("#errorPane").hide();
    $("#errorPane").empty();

    var responseErrorlist = $("#errorPane").append("<ul></ul>").find("ul");
    for (var i = 0; i < data.MessageTextArray.length; i++) {
        responseErrorlist.append("<li>" + data.MessageTextArray[i] + "</li>");
    }

    $("#errorPane").show();

    $('html, body')
        .animate({
            scrollTop:
                $('#errorPane').offset().top - 75
        },
            'slow');

    hideProcessingDialog();
}

function submitDeleteForm(confirmationMessage, submitUrl, onSuccess) {
    showConfirmationDialog(confirmationMessage,
        function() {

            showProcessingDialog();

            $.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: submitUrl,
                success: function(data) {
                    if (data.IsSuccess) {
                        onSuccess();
                    } else {
                        processErrorPostResponse(data);
                    }

                    hideProcessingDialog();
                }
            });
        });
}

$('#cancelButton')
    .click(function () {
        closeMaintenanceWindow();
    });

function closeMaintenanceWindow() {
    var maintenanceDiv = $('#maintenancePane');
    maintenanceDiv.empty();
}

function showInformation(message) {
    $.notify(message,
        {
            autoHide: true,
            autoHideDelay: 3000,
            className: 'info',
            position: 'top center',
            showAnimation: 'slideDown',
            showDuration: 400,
            hideAnimation: 'slideUp',
            hideDuration: 200
        });
}

function showPopup(url) {
    var param = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no, width=1000,height=600,left=-1000,top=-1000';
    open(url, '_blank', param);
}