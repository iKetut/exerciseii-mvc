﻿function Timer(timedOutInfo, closeAction) {
    var timer = $('#time-duration').html();

    if (timer == null) {
        return false;
    }

    timer = timer.split(':');
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    seconds -= 1;
    if (minutes < 0) {
        return clearInterval(interval);
    }

    if (seconds < 0 && minutes != 0) {
        minutes -= 1;
        seconds = 59;
    } else if (seconds < 10 && length.seconds !== 2) {
        seconds = '0' + seconds;
    }

    if (minutes < 10 && minutes.length != 2) {
        minutes = '0' + minutes;
    }

    if (minutes == 0 && seconds <= 59 && seconds > 0) {
        $('#time-duration').addClass('text-danger');
        $('#writing-duration').addClass('text-danger');
    }

    $('#time-duration').html(minutes + ':' + seconds);
    $('#writing-duration').html(minutes + ':' + seconds);

    if (minutes == 0 && seconds == 0) {
        clearInterval(interval);
        $('#time-duration').removeClass('text-danger');
        $('#writing-duration').removeClass('text-danger');
        $('#AnswerText').removeAttr("required");
        showInfoDialog(timedOutInfo, function () {
            closeAction();
        });
    }

    return true;
}