﻿function showProcessingDialog() {
    $('#processingDialogPane').modal({ show: true, backdrop: 'static', keyboard: false });
}

function hideProcessingDialog() {
    $('#processingDialogPane').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

function showLoadingDialog() {
    $('#processingDialogPane').modal({ show: true, backdrop: 'static', keyboard: false });
}

function hideLoadingDialog() {
    $('#processingDialogPane').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

function showErrorDialog(message) {
    BootstrapDialog.show({
        title: 'Error',
        message: message,
        cssClass: 'error-dialog',
        draggable: true,
        buttons: [{
            label: 'OK',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
        }]
    });
}

function showInfoDialog(message) {
    BootstrapDialog.show({
        title: 'Information',
        message: message,
        cssClass: 'info-dialog',
        draggable: true,
        buttons: [{
            label: 'OK',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
        }]
    });
}

function showInfoDialog(message, actionToExecute) {
    BootstrapDialog.show({
        title: 'Information',
        message: message,
        cssClass: 'info-dialog',
        draggable: true,
        closable: false,
        buttons: [{
            label: 'OK',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                actionToExecute();
            }
        }]
    });
}

function showConfirmationDialog(message, yesAction, noAction, otherActionToExecute) {
    BootstrapDialog.show({
        title: 'Confirmation',
        message: message,
        cssClass: 'modal--confirmation confirmation-dialog',
        draggable: true,
        buttons: [{
            label: 'Yes',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                yesAction();
            },
            cssClass: 'btn-primary'
        }, {
            label: 'No',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                if (typeof noAction === 'function') noAction();
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                if(typeof otherActionToExecute === 'function') otherActionToExecute();
            }
        }]
    });
}

function showYesNoActionConfirmationDialog(message, yesAction, noAction) {
    BootstrapDialog.show({
        title: 'Confirmation',
        message: message,
        cssClass: 'confirmation-dialog',
        draggable: true,
        buttons: [{
            label: 'Yes',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                yesAction();
            }
        }, {
            label: 'No',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                noAction();
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
        }]
    });
}

function showVideo(url,title, closeAction) {
    BootstrapDialog.show({
        title: title,
        cssClass: 'info-dialog',
        closable: false,
        message: function (dialog) {
            var $message = $('<div></div>');
            $message.load(url, function () {
                var $footerButton = dialog.getButton('btn-1');
                $footerButton.stopSpin();
            });

            return $message;
        },
        onshow: function (dialog) {
            var $footerButton = dialog.getButton('btn-1');
            $footerButton.spin();
        },
        buttons: [{
            id: 'btn-1',
            label: "Close",
            action: function (dialog) {
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                dialog.close();
                closeAction();
            }
        }]
    });
}


function showPopup(url) {
        var param = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no, width=1000,height=600,left=-1000,top=-1000';
        open(url, '_blank', param);
    }