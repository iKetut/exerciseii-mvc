﻿using System;

namespace LearningBase.Mvc.ViewModel
{
    public class AuditableViewModelBase
    {
        #region Properties

        public string CreatedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

        #endregion
    }
}