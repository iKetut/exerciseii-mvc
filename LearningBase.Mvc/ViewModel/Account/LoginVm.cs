﻿using System.ComponentModel.DataAnnotations;

namespace LearningBase.Mvc.ViewModel.Account
{
    public class LoginVm
    {
        #region Properties

        [Required]
        [Display(Name = "Login_UserName", ResourceType = typeof(Resource))]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Login_Password", ResourceType = typeof(Resource))]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        public string ErrorMessage { get; set; }

        #endregion
    }
}