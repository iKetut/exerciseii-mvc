﻿IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'UserProfile'))
BEGIN

	CREATE TABLE [dbo].[UserProfile] (
		[UserId]   INT IDENTITY (1, 1) NOT NULL,
		[UserName] NVARCHAR (56) NOT NULL, 
		[FirstName] NVARCHAR(200) NOT NULL,
		[LastName] NVARCHAR(200) NOT NULL,
		[EmailAddress] NVARCHAR(500) NOT NULL,
		[MaximumLogin] INT NULL,
		[CreatedBy] NVARCHAR(56) NOT NULL, 
		[CreatedDateTime] DATETIME2 NOT NULL, 
		[LastModifiedBy] NVARCHAR(56) NOT NULL, 
		[LastModifiedDateTime] DATETIME2 NOT NULL, 
		PRIMARY KEY CLUSTERED ([UserId] ASC),
		UNIQUE NONCLUSTERED ([UserName] ASC)
	);

END
GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'sys_DatabaseInformation'))
BEGIN
	CREATE TABLE [dbo].[sys_DatabaseInformation]
	(
		[Id] INT NOT NULL PRIMARY KEY,
		[Name] NVARCHAR(100) NOT NULL, 
		[Value] NVARCHAR(100) NOT NULL, 
		[CreatedBy] NVARCHAR(56) NOT NULL, 
		[CreatedDateTime] DATETIME2 NOT NULL, 
		[LastModifiedBy] NVARCHAR(56) NOT NULL,
		[LastModifiedDateTime] DATETIME2 NOT NULL 
	)
END
GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'webpages_Membership'))
BEGIN
	CREATE TABLE [dbo].[webpages_Membership] (
		[UserId]                                  INT            NOT NULL,
		[CreateDate]                              DATETIME       NULL,
		[ConfirmationToken]                       NVARCHAR (128) NULL,
		[IsConfirmed]                             BIT            DEFAULT ((0)) NULL,
		[LastPasswordFailureDate]                 DATETIME       NULL,
		[PasswordFailuresSinceLastSuccess]        INT            DEFAULT ((0)) NOT NULL,
		[Password]                                NVARCHAR (128) NOT NULL,
		[PasswordChangedDate]                     DATETIME       NULL,
		[PasswordSalt]                            NVARCHAR (128) NOT NULL,
		[PasswordVerificationToken]               NVARCHAR (128) NULL,
		[PasswordVerificationTokenExpirationDate] DATETIME       NULL,
		PRIMARY KEY CLUSTERED ([UserId] ASC)
	);
END
GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'webpages_OAuthMembership'))
BEGIN
	CREATE TABLE [dbo].[webpages_OAuthMembership] (
		[Provider]       NVARCHAR (30)  NOT NULL,
		[ProviderUserId] NVARCHAR (100) NOT NULL,
		[UserId]         INT            NOT NULL,
		PRIMARY KEY CLUSTERED ([Provider] ASC, [ProviderUserId] ASC)
	);
END
GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'webpages_Roles'))
BEGIN
	CREATE TABLE [dbo].[webpages_Roles] (
		[RoleId]   INT            IDENTITY (1, 1) NOT NULL,
		[RoleName] NVARCHAR (256) NOT NULL,
		PRIMARY KEY CLUSTERED ([RoleId] ASC),
		UNIQUE NONCLUSTERED ([RoleName] ASC)
	);
END
GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'webpages_UsersInRoles'))
BEGIN

	CREATE TABLE [dbo].[webpages_UsersInRoles] (
		[UserId] INT NOT NULL,
		[RoleId] INT NOT NULL,
		PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
		CONSTRAINT [fk_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[webpages_Roles] ([RoleId]),
		CONSTRAINT [fk_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserProfile] ([UserId])
	);

END
GO

/*
	Insert built in roles
*/

IF NOT EXISTS (SELECT RoleName FROM webpages_Roles)
BEGIN
	INSERT INTO webpages_Roles (RoleName) VALUES ('Administrator');
END

IF NOT EXISTS (SELECT RoleName FROM webpages_Roles WHERE RoleName = 'Student')
BEGIN
	INSERT INTO webpages_Roles (RoleName) VALUES ('Student');
END

GO

-- Create admin
IF NOT EXISTS(SELECT UserId FROM UserProfile WHERE UserName = 'admin')
BEGIN
	INSERT INTO UserProfile (UserName, FirstName, LastName, EmailAddress,CreatedBy,CreatedDateTime,LastModifiedBy,LastModifiedDateTime) 
	VALUES ('admin', 'Admin', 'Administrator', 'admin@learningbase.com','system',GETUTCDATE(),'system',GETUTCDATE());

	INSERT INTO [dbo].[webpages_Membership]
			   ([UserId]
			   ,[CreateDate]
			   ,[ConfirmationToken]
			   ,[IsConfirmed]
			   ,[LastPasswordFailureDate]
			   ,[PasswordFailuresSinceLastSuccess]
			   ,[Password]
			   ,[PasswordChangedDate]
			   ,[PasswordSalt]
			   ,[PasswordVerificationToken]
			   ,[PasswordVerificationTokenExpirationDate])
		 VALUES
				(1,
				'2014-12-07 09:22:05.380',
				NULL,
				1,
				'2014-12-07 10:08:54.530',
				0,
				'ADvx8rK+hYDp84saMeoab1NVnBfuG6J0d5YDpsQTqerxdTe+/GDx2CSv3g5Fxbvj6A==',	
				'2014-12-07 09:22:05.380',
				'',
				NULL,
				NULL);

	INSERT INTO webpages_UsersInRoles (UserId, RoleId) VALUES (1, 1);

END

GO

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'utl_UserLoginActivity'))
BEGIN
	CREATE TABLE [dbo].[utl_UserLoginActivity]
	(
		[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
		[UserName] NVARCHAR(56) NOT NULL,
		[IpAddress] NVARCHAR(30) NOT NULL,
		[FirstLoginDateTime] DateTime2 NOT NULL,
		[LastLoginDateTime] DateTime2 NOT NULL
	)
END
GO