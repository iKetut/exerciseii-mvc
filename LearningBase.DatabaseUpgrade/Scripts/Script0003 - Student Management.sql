﻿IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'com_Student'))
BEGIN
	CREATE TABLE [dbo].[com_Student]
	(
		[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
		[UserId] INT NOT NULL, 
		[TimeZoneId] NVARCHAR(100) NULL,
		[CourseId] INT NOT NULL,
		[CreatedBy] NVARCHAR(56) NOT NULL, 
		[CreatedDateTime] DATETIME2 NOT NULL, 
		[LastModifiedBy] NVARCHAR(56) NOT NULL, 
		[LastModifiedDateTime] DATETIME2 NOT NULL, 
		CONSTRAINT [FK_com_Student_To_UserProfile] FOREIGN KEY (UserId) REFERENCES [UserProfile]([UserId]) On Delete Cascade,
		CONSTRAINT [FK_com_Student_To_crs_Course] FOREIGN KEY (CourseId) REFERENCES [crs_Course]([Id])
	)
END

GO

CREATE PROCEDURE [dbo].[usp_com_DeleteStudent]
	@studentId INT
AS
BEGIN
	
	DECLARE @TempUserId INT

	SELECT @TempUserId = UserId 
	FROM dbo.com_Student WHERE Id = @studentId
	
	DELETE FROM dbo.webpages_UsersInRoles 
		WHERE UserId = @TempUserId
			
	DELETE FROM dbo.webpages_Membership  
		WHERE UserId = @TempUserId
			
	DELETE FROM dbo.UserProfile 
		WHERE UserId = @TempUserId

	DELETE FROM dbo.com_Student WHERE Id = @studentId

END

GO