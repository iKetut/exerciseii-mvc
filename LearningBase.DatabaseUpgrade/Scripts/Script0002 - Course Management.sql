﻿IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'crs_Course'))
BEGIN
	CREATE TABLE [dbo].[crs_Course]
	(
		[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
		[Code] NVARCHAR(10) NOT NULL,
		[Name] NVARCHAR(200) NOT NULL,
		[Description] NVARCHAR(500) NULL,
		[CreatedBy] NVARCHAR(56) NOT NULL, 
		[CreatedDateTime] DATETIME2 NOT NULL, 
		[LastModifiedBy] NVARCHAR(56) NOT NULL, 
		[LastModifiedDateTime] DATETIME2 NOT NULL, 
	)
END

IF (NOT EXISTS (SELECT *  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'crs_Module'))
BEGIN
	CREATE TABLE [dbo].[crs_Module]
	(
		[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
		[CourseId] INT NOT NULL,
		[Name] NVARCHAR(200) NOT NULL,
		[SortingIndex] INT NOT NULL,
		[CreatedBy] NVARCHAR(56) NOT NULL, 
		[CreatedDateTime] DATETIME2 NOT NULL, 
		[LastModifiedBy] NVARCHAR(56) NOT NULL, 
		[LastModifiedDateTime] DATETIME2 NOT NULL, 
		CONSTRAINT [FK_crs_Module_To_crs_Course] FOREIGN KEY (CourseId) REFERENCES [crs_Course]([Id])
	)
END

GO

IF OBJECT_ID('usp_crs_UpdateModuleIndex', 'P') IS NOT NULL
	DROP PROC [dbo].[usp_exp_GetMyActivityProgress]
GO

CREATE PROCEDURE [dbo].[usp_crs_UpdateModuleIndex]
	@moduleId INT,
	@newIndex INT
AS
BEGIN

	DECLARE @currentIndex INT;
	SELECT @currentIndex = SortingIndex FROM crs_Module WHERE Id = @moduleId;
	
	DECLARE @courseId INT;
	SELECT @courseId = CourseId FROM crs_Module WHERE Id = @moduleId;

	IF(@currentIndex > @newIndex)
	BEGIN
		UPDATE crs_Module SET SortingIndex = SortingIndex + 1 WHERE CourseId = @courseId AND SortingIndex >=@newIndex;
	END
	ELSE
	BEGIN
		UPDATE crs_Module SET SortingIndex = SortingIndex - 1 WHERE CourseId = @courseId AND SortingIndex >=@newIndex;
	END

	UPDATE crs_Module SET SortingIndex = @newIndex WHERE Id = @moduleId;
END